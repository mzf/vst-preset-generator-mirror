#!/bin/bash

# -------------------------------------------------------------------------------
#    This file is a part of the VST Preset Generator
#    Copyright (C) 2007  Francois Mazen
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#-------------------------------------------------------------------------------

function check_return_code
{
	if [ $1 -ne 0 ]
	then
		(>&2 echo "BUILD FAILED")
		exit $1
	fi
}

if [ $# -ne 1 ]
then
	(>&2 echo "Missing zip filename as argument.")
	exit 1
fi

ZIP_FILENAME=$1

echo =================
echo generate zip file
echo =================
7z a "${ZIP_FILENAME}" "./../build64/release/vpg64.exe"
check_return_code $?
7z a "${ZIP_FILENAME}" "./../build32/release/vpg32.exe"
check_return_code $?
7z a "${ZIP_FILENAME}" "./../doc/vpg-doc.pdf"
check_return_code $?
7z a "${ZIP_FILENAME}" "./../../../LICENSE"
check_return_code $?
7z a "${ZIP_FILENAME}" "./../../../CHANGELOG"
check_return_code $?

exit 0
