/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
*/

function Component()
{
	// Allow 64bit binaries only on 64bit platform.
	console.log("CPU Architecture: " +  systemInfo.currentCpuArchitecture);

	installer.componentByName("vpg.bin64").setValue("Virtual", "true");

	if ( systemInfo.currentCpuArchitecture === "x86_64") 
	{
		installer.componentByName("vpg.bin64").setValue("Virtual", "false");
		installer.componentByName("vpg.bin64").setValue("Default", "true");
	}

	// Set default folder to program files.
	var programFiles = installer.environmentVariable("ProgramFiles");
	if (programFiles != "")
	{
		installer.setValue("TargetDir", programFiles + "/VST Preset Generator");
	}
}
