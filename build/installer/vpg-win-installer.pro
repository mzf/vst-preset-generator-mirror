TEMPLATE = aux

# Get version from the vpg.pro file.
VPG_VERSION = $$fromfile("../../src/vpg.pro", VERSION)
message(Build version: $$VPG_VERSION)

BUILD_DATE = $$system("date /T")
BUILD_DATE_LIST = $$split(BUILD_DATE, /)
BUILD_DATE = $$member(BUILD_DATE_LIST, 2)-$$member(BUILD_DATE_LIST, 1)-$$member(BUILD_DATE_LIST, 0)
message(Build date: $$BUILD_DATE)

# Replace the version and release date in all xml files
TEMPLATE_FILES = $$files("*.template", true)
for(TEMPLATE_FILE, TEMPLATE_FILES){
	CONTENT = $$cat($${TEMPLATE_FILE}, blob)
	CONTENT = $$replace(CONTENT, "__VPG_VERSION__", $$VPG_VERSION)
	CONTENT = $$replace(CONTENT, "__RELEASE_DATE__", $$BUILD_DATE)
	XML_FILENAME = $$replace(TEMPLATE_FILE, ".template", "")
	write_file($${XML_FILENAME}, CONTENT)
}

# Installer
INSTALLER_FILENAME = vpg-$$VPG_VERSION-win-setup.exe

INPUT = $$PWD/config/config.xml $$PWD/packages
installer_generator.input = INPUT
installer_generator.output = ../$$INSTALLER_FILENAME
installer_generator.commands = binarycreator -c $$PWD/config/config.xml -p $$PWD/packages ${QMAKE_FILE_OUT}
installer_generator.CONFIG += target_predeps no_link combine

QMAKE_EXTRA_COMPILERS += installer_generator

# Zip
ZIP_FILENAME = vpg-$$VPG_VERSION-win.zip

zip_generator.commands = ./../../installer/build_zip_archive.sh "../$$ZIP_FILENAME"
zip_generator.input = INPUT
zip_generator.output = $$ZIP_FILENAME
zip_generator.CONFIG += target_predeps no_link combine

QMAKE_EXTRA_COMPILERS += zip_generator
