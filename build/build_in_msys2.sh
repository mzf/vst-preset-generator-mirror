#!/bin/bash

# -------------------------------------------------------------------------------
#    This file is a part of the VST Preset Generator
#    Copyright (C) 2007  Francois Mazen
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#-------------------------------------------------------------------------------
#  Name: build_in_msys2.sh
#  Author: Francois Mazen
#  Date: 26/05/16 21:40
#  Description: This bash script can build VST Preset Generator for windows
#               32bit and 64bit via MSYS2 shell on windows platform.
#-------------------------------------------------------------------------------

START_TIME=$SECONDS

function check_return_code
{
	if [ $1 -ne 0 ]
	then
		(>&2 echo "BUILD FAILED")
		exit $1
	fi
}

function check_package
{
	echo checking package: $1
	pacman -Qi $1 > /dev/null
	check_return_code $?
}

VPGPRO_SOURCE_PATH="../../../src/vpg.pro"
BUILD_INDEX=$(date +%Y%m%d%H%M%S)
RELEASE_FOLDER=${BUILD_INDEX}
MSYS2_PATH=$PATH

echo ====================
echo Check MSYS2 packages
echo ====================

#x86
check_package mingw-w64-i686-qt5-static
check_package mingw-w64-i686-make
check_package mingw-w64-i686-gcc
check_package mingw-w64-i686-libwebp
check_package mingw-w64-i686-jasper
check_package mingw-w64-i686-dbus
check_package mingw-w64-i686-ruby
check_package mingw-w64-i686-qt-installer-framework-git

#x64
check_package mingw-w64-x86_64-qt5-static
check_package mingw-w64-x86_64-make
check_package mingw-w64-x86_64-gcc
check_package mingw-w64-x86_64-libwebp
check_package mingw-w64-x86_64-jasper
check_package mingw-w64-x86_64-dbus
check_package mingw-w64-x86_64-ruby
check_package mingw-w64-x86_64-qt-installer-framework-git

#ruby stuffs
gem which asciidoctor
check_return_code $?
gem which asciidoctor-pdf
check_return_code $?

#misc
check_package p7zip

echo =================================
echo Create root folder $RELEASE_FOLDER
echo =================================
mkdir $RELEASE_FOLDER
check_return_code $?
cd $RELEASE_FOLDER
check_return_code $?

echo ============
echo VPG Build 64
echo ============
FOLDER_NAME=build64

echo Creating folder $FOLDER_NAME
mkdir $FOLDER_NAME
check_return_code $?
cd $FOLDER_NAME
check_return_code $?

echo Export path for qt5-static in mingw64
export PATH="/mingw64/qt5-static/bin:/mingw64/bin:$MSYS2_PATH"

echo =====
echo qmake
echo =====
qmake.exe $VPGPRO_SOURCE_PATH -r -spec win32-g++ "CONFIG+=release"
check_return_code $?

echo ====
echo make
echo ====
mingw32-make.exe
check_return_code $?

cd ..

echo ============
echo VPG Build 32
echo ============
FOLDER_NAME=build32

echo Creating folder $FOLDER_NAME
mkdir $FOLDER_NAME
check_return_code $?
cd $FOLDER_NAME
check_return_code $?

echo Export path for qt5-static in mingw32
export PATH="/mingw32/qt5-static/bin:/mingw32/bin:$MSYS2_PATH"

echo =====
echo qmake
echo =====
qmake.exe $VPGPRO_SOURCE_PATH -r -spec win32-g++ "CONFIG+=release"
check_return_code $?

echo ====
echo make
echo ====
mingw32-make.exe
check_return_code $?

cd ..

echo ======================
echo Generate Documentation
echo ======================
FOLDER_NAME=doc

echo Creating folder $FOLDER_NAME
mkdir $FOLDER_NAME
check_return_code $?
cd $FOLDER_NAME
check_return_code $?

# pdf output
# need the plugin asciidoctor-pdf installed via ruby gem:
#  gem install --pre asciidoctor-pdf
asciidoctor -r asciidoctor-pdf -b pdf -o vpg-doc.pdf ../../../doc/vpg.asciidoc
check_return_code $?

cd ..

echo ========================
echo copy files for installer
echo ========================

7z a "../installer/packages/vpg.bin64/data/vpgbin64.7z" "./build64/release/vpg64.exe" 
check_return_code $?
7z a "../installer/packages/vpg.bin32/data/vpgbin32.7z" "./build32/release/vpg32.exe" 
check_return_code $?
7z a "../installer/packages/vpg.doc/data/vpgdoc.7z" "./doc/vpg-doc.pdf" 
check_return_code $?
cp "../../LICENSE" "../installer/packages/vpg/meta/"
check_return_code $?
cp "../../src/images/vpg.png" "../installer/config/"
check_return_code $?

echo ==================
echo generate installer
echo ==================
FOLDER_NAME=installer
echo Creating folder $FOLDER_NAME
mkdir $FOLDER_NAME
check_return_code $?
cd $FOLDER_NAME
check_return_code $?
qmake "../../installer/vpg-win-installer.pro"
check_return_code $?
mingw32-make.exe release
check_return_code $?

cd ..

echo ===================
echo generate check sums
echo ===================
md5sum *.exe *.zip > MD5SUMS
check_return_code $?
sha1sum *.exe *.zip > SHA1SUMS
check_return_code $?

echo ================
echo reset MSYS2 PATH
echo ================
export PATH="$MSYS2_PATH"

ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo "$(($ELAPSED_TIME/60)) min $(($ELAPSED_TIME%60)) sec"

echo "BUILD SUCCESS"

exit 0
