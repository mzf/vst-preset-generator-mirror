VST Preset Generator README
===========================

VST Preset Generator is a software to create random preset for VST plugins.

See [Official Website](http://vst-preset-generator.org) for [releases](http://vst-preset-generator.org/download) and [documentation](http://doc.vst-preset-generator.org).

# News

See `CHANGELOG` for new features and bug fixes.

# Source Structure

 - `build` contains scripts to generate the software for official release, including installers and documentation.
 - `doc` contains the [AsciiDoc](https://en.wikipedia.org/wiki/AsciiDoc) source files of the documentation.
 - `src` contains the source files of the software, mainly in C++ programming language.

# How to Build

## Prerequisites

* Qt 5.6
* Steinberg [VST2 SDK](http://www.steinberg.net/en/company/developers.html). Copy the VST SDK folder `pluginterfaces/vst2.x` into the `src` folder of VST Preset Generator. Otherwise the VeSTige header will be used as substitute.
* AsciiDoctor with the pdf plugin for the documentation. To install asciidoctor-pdf plugin, use ruby gem package manager: `gem install --pre asciidoctor-pdf`.

## Easy Way with Qt Creator

Open `src/vpg.pro` in Qt Creator and launch the compilation.

You can also build `src/test/vpgtests.pro` and launch the tests with the command `make check`.

## Command Line

You can build the software with Qt command line tools. Run `qmake` to generate the Makefile then run `make` to build the software.

See the build script `build/build_in_msys2.sh` as example.

The `sudo make install` command copies the output to `/usr/bin`. Patch the file `src/vpg_linux_deploy.pri` to change the install folder.

## Official Windows Build

The official releases for Windows are generated with Qt5 static inside MSYS2. The script `build/build_in_msys2.sh` automates the build and generation of 32bits and 64bits releases with installers and documentation.

The targets are `mingw-w64-i686-qt5-static` and `mingw-w64-x86_64-qt5-static`.

For more information about MinGW-64 with Qt: [https://wiki.qt.io/MinGW-64-bit#Qt-builds](https://wiki.qt.io/MinGW-64-bit#Qt-builds).

# License

You may use, distribute and copy the VST Preset Generator under the terms of **GNU General Public License version 2**.

See `LICENSE` for more information.

# Authors

Copyright (c) 2007-2016 François Mazen

# Contact

[http://vst-preset-generator.org](http://vst-preset-generator.org)

[contact@vst-preset-generator.org](mailto:contact@vst-preset-generator.org)

