----------------------------------------------
             VST Preset Generator
----------------------------------------------
    Copyright (c) 2007-2017 Fran�ois Mazen
----------------------------------------------
       http://vst-preset-generator.org
       contact@vst-preset-generator.org
----------------------------------------------


----------------------------------------------
***************** CHANGELOG ******************
----------------------------------------------

* v0.3.0 - 14 July 2017
 - Big User Interface change to improve the
   workflow.
 - Ability to load preset values (FS#6).
 - Fix weird character at the end of parameter
   names (FS#30).
 - Update VPG files to save parameter values.
 - Display real parameter value instead of
   internal float value (FS#22).
 - Add a rounded range control to change
   parameter values.
 - Set constant value and normal middle value
   similar (FS#33).
 - Ability to change preset name, to remove
   preset, and to delete plugin.
 - Display parameter name for all plugins,
   including non-chunk plugins (FS#14)
 - Save last VST Preset folder in settings.

* v0.2.8 - 15 December 2016
 - Release for Linux with VeSTige instead of
   Steinberg VST SDK.
 - Fix wrong chunk preset size (FS#27)
 - Fix endianness converter issue (FS#28)
 - Change folder of setting file (vpg.ini)
   from the application folder to a system 
   folder.

* v0.2.7 - 21 June 2016 - Make Music Day
 - Speed-up load of plug-in with lots of
   parameters
 - Fix parameter names display when switching
   from non-chunk plugin to chunk plugin
 - Fix value refresh when the random method
   is changed
 - Set bank default size as plug-in max
   program (FS#24)
 - Move documentation to asciidoc format
 - Windows Installer (FS#12)
 - Add Windows application information (FS#19)
 - Fix typo licence to license (FS#25)
 - Add version number in VPG xml files, for
   future use.
 - Fix typo in French translation (FS#18)

* v0.2.6 - 10 May 2016
 - Change target name according to the platform:
     - 64bit: vpg64
     - 32bit: vpg32
 - Improve random generator with normal
   distribution.
 - Fix bug FS#15: Generation failed with
   plug-ins that does not support bank preset.
 - Update website and e-mail contact

* v0.2.5 - 29 March 2016
 - Release for Windows 64 bits and 32 bits
 - UI: move VstList Toolbar to the VstList panel
 - Update English and French translations.
   Also replace all vst/Vst by VST
 - Fix bug when compiling for 64bits machine
   (thanks to Anthony Leotta).
 - Linux support, but no official release yet
 - Use Qt5 static, no third party dependencies
 - Add automatic tests with a mock VST plugin

* v0.2.4bis and v0.2.4ter - 07 April 2013
 - fix dll dependencies

* v0.2.4 - 01 February 2013
 - Fix some internal memory leaks.

* v0.2.3 - 02 October 2007
 - Can change the application's language at
   the beginning (currently only English and
   French)
 - changed the max of program in a bank
   (100 to 1000)
 - generation button in the tool bar
 - the colums of the parameters table is
   automatic resized.
 - button to assign the same random method to
   all parameters
 - button to assign random method and random
   values to all parameters (just for fun!)
 - use of the progress bar when generating a
   patch file
 - on close, don't ask to save the vst list if
   it's empty
 - No crazy progress bar behavior when you
   load a vpg file
 - Use a ini file to store the path of
   differents files (vst folder, vpg folder
   and fxb/fxp folder)
 - Can import VST parameters from a drag&drop
   of a dll or patch file (spacedad idea)
 - change value in the parameter table in only
   one click
   (just to save life of your mouse ;) )


* v0.2.2 - 25 September 2007
 - graphic design changed
 - size of the executable optimized
 - Don't load a default file when start
 - The graphic images and icones are added as
   resources
   Some of these come from the "Crystal Clear
   icon set" by Everaldo Coelho, under GNU
   Lesser General Public License
 - The list of parameters don't freeze when
   you load a big Vst, thanks to a table
 - The file type section is now in a popup
   box, that is shown when the generate button
   is clicked.
 - Only one command to import information
   from vst file or preset file, the software
   can detect the file type.


* v0.2.1 - 25 June 2007
 - Can load !!!ALL!!! VST plugins, thanks to
   the Qt's class QLibrary
 - Remember only the last file directory,
   without the filename.
 - Change the name of the programs from
   "Vst Preset Generator #" to "VPG #"
   it's more readable in the audio host
 - When you load the plug-in, the name of
   each parameter is read
 - Add menu "Import"



* v0.2.0 - 14 June 2007
 - Function to Load Vst plug-in and avoid
   "Opaque Chunk Method" in Preset File
 - Reduce graphic space between parameters
   in order to use less space
   (thanks Chimimic)
 - Remember the last file directory for
   opening presets and Vst
   (SpaceDad feature)
 - Add a progressBar, for waiting while
   parameter widgets creation
 - No error message when you press "cancel"
   button from any Open/Save Dialog Box



* v0.1.1 - 04 June 2007
 - Can create a blank vst list
 - Change graphic design, to improve space
 - Real mainwindows (no quit with esc key)
 - "About" menu with Copyright
 - "Vst List" menu
 - Some English mistakes corrected
 - Extern library to generate random numbers
   (Random Number Generator by Weili Chen and
   Zixuan Ma, OpenSource project)
 - Normal distribution option, in order to
   oscillate parameters around a value



* V0.1.0 - 28 May 2007
First Official Release, GNU GPL License

