#!/bin/bash

# -------------------------------------------------------------------------------
#    This file is a part of the VST Preset Generator
#    Copyright (C) 2007  Francois Mazen
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#-------------------------------------------------------------------------------
#  Name: build_doc.sh
#  Author: Francois Mazen
#  Date: 06/06/16 22:49
#  Description: This bash script generates documentation from asciidoc source.
#-------------------------------------------------------------------------------

function check_return_code
{
	if [ $1 -ne 0 ]
	then
		(>&2 echo "BUILD FAILED")
		exit $1
	fi
}

# html5 output
asciidoctor -b html5 vpg.asciidoc
check_return_code $?

# pdf output
# need the plugin asciidoctor-pdf installed via ruby gem:
#  gem install --pre asciidoctor-pdf
asciidoctor -r asciidoctor-pdf -b pdf vpg.asciidoc
check_return_code $?

# Tutorial
asciidoctor -b html5 tutorial.asciidoc
check_return_code $?

asciidoctor -r asciidoctor-pdf -b pdf tutorial.asciidoc
check_return_code $?

# Index
asciidoctor -b html5 index.asciidoc
check_return_code $?

exit 0
