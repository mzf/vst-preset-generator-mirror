/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: filereader.h
  Author: Francois Mazen
  Date: 10/07/17 18:25
  Description: FileReader is a safe file reader that emits exception when
               end of file is reached.
-------------------------------------------------------------------------------
*/

#ifndef FILEREADER_H
#define FILEREADER_H

#include <QSharedPointer>
#include <QException>
#include <QFile>

class FileReader
{
public:

    class EndOfFileReachedException : public QException
    {
    public:
        void raise() const { throw *this; }
        EndOfFileReachedException* clone() const { return new EndOfFileReachedException(*this); }
    };

    static QSharedPointer<FileReader> create(const QString& filename);

    QByteArray read(qint64 maxlen);

private:
    FileReader(const QString& filename);

    QSharedPointer<QFile> file;
};

#endif // FILEREADER_H
