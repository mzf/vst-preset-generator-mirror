/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: parameter.cpp
  Author: Francois Mazen
  Date: 25/05/16 22:01
  Description: Class for parameters.
-------------------------------------------------------------------------------
*/


#include "parameter.h"

#include "vpgrandom.h"

#include <QSharedPointer>

//-----------------------------------------------------------------------------
Parameter::Parameter() :
    name(""),
    randomMode(RandomMode_Uniform),
    uniformMinValue(0),
    uniformMaxValue(1),
    normalStandardDeviation(0.01),
    targetValue(0.5)
{

}

//-----------------------------------------------------------------------------
Parameter::Parameter(const QString& name) :
    name(name),
    randomMode(RandomMode_Uniform),
    uniformMinValue(0),
    uniformMaxValue(1),
    normalStandardDeviation(0.01),
    targetValue(0.5)
{

}

//-----------------------------------------------------------------------------
Parameter::~Parameter()
{

}

//-----------------------------------------------------------------------------
const QString& Parameter::getName() const
{
    return this->name;
}

//-----------------------------------------------------------------------------
void Parameter::setName(const QString& newName)
{
    this->name = newName;
    emit dataChanged();
}

//-----------------------------------------------------------------------------
float Parameter::getUniformMinValue() const
{
    return this->uniformMinValue;
}

//-----------------------------------------------------------------------------
void Parameter::setUniformMinValue(float value)
{
    this->uniformMinValue = value;
    emit dataChanged();
}

//-----------------------------------------------------------------------------
float Parameter::getUniformMaxValue() const
{
    return this->uniformMaxValue;
}

//-----------------------------------------------------------------------------
void Parameter::setUniformMaxValue(float value)
{
    this->uniformMaxValue = value;
    emit dataChanged();
}

//-----------------------------------------------------------------------------
float Parameter::getNormalMiddleValue() const
{
    // Internally the normal middle value and the constant value are the same.
    return this->targetValue;
}

//-----------------------------------------------------------------------------
void Parameter::setNormalMiddleValue(float value)
{
    // Internally the normal middle value and the constant value are the same.
    this->targetValue = value;
    emit dataChanged();
}

//-----------------------------------------------------------------------------
float Parameter::getNormalStandardDeviation() const
{
    return this->normalStandardDeviation;
}

//-----------------------------------------------------------------------------
void Parameter::setNormalStandardDeviation(float value)
{
    this->normalStandardDeviation = value;
    emit dataChanged();
}

//-----------------------------------------------------------------------------
float Parameter::getConstantValue() const
{
    return this->targetValue;
}

//-----------------------------------------------------------------------------
void Parameter::setConstantValue(float value)
{
    this->targetValue = value;
    emit dataChanged();
}

//-----------------------------------------------------------------------------
Parameter::RandomMode Parameter::getRandomMode() const
{
    return this->randomMode;
}

//-----------------------------------------------------------------------------
void Parameter::setRandomMode(Parameter::RandomMode mode)
{
    this->randomMode = mode;
    emit dataChanged();
}

//-----------------------------------------------------------------------------
float Parameter::computeRandomValue(const QSharedPointer<VpgRandom>& vpgRandom) const
{
    float val = 0.f;
    int maxTries = 100;

    switch(getRandomMode())
    {
    case Parameter::RandomMode_Constant:
        val = getConstantValue();
        break;
    case Parameter::RandomMode_Normal:
        //mu and deviation

        // keep only value between [0;1]
        do
        {
            val = vpgRandom->getNormalValue(getNormalMiddleValue(), getNormalStandardDeviation());
            maxTries--;
        }
        while((val < 0.f || val > 1.f) && maxTries > 0);

        // If generation failed after lots of tries, keep the mean value.
        if(maxTries <= 0)
        {
            val = getNormalMiddleValue();
        }
        break;

    case Parameter::RandomMode_Uniform:
        //min-max
    default:
        val = vpgRandom->getUniformValue(getUniformMinValue(), getUniformMaxValue());
        break;
    }

    return val;
}

//-----------------------------------------------------------------------------
void Parameter::copyValuesFromOtherParameter(const QSharedPointer<Parameter>& other)
{
    if(!other.isNull())
    {
        // Copy all but the name.
        randomMode = other->randomMode;
        uniformMinValue = other->uniformMinValue;
        uniformMaxValue = other->uniformMaxValue;
        normalStandardDeviation = other->normalStandardDeviation;
        targetValue = other->targetValue;

        emit dataChanged();
    }
}
