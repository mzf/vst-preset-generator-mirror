/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: filetypedialog.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: a dialog class to choose the type of the preset file (fxp or fxb)
-------------------------------------------------------------------------------
*/

#include "filetypedialog.h"

#include <QtWidgets>

//-----------------------------------------------------------------------------
FileTypeDialog::FileTypeDialog(QWidget *parent, bool isBankFileEnabled, int maxProgramPerBank) : QDialog(parent)
{
    setWindowTitle(tr("File Type"));

    gbFileType = new QGroupBox(tr("File Type"),this);
    lblNbProg = new QLabel(tr("Number of programs:"),gbFileType);
    spbxBank = new QSpinBox(gbFileType);
    rbtnProgram = new QRadioButton(tr("program preset file (.fxp)"), gbFileType);
    rbtnBank = new QRadioButton(tr("bank preset file (.fxb)"), gbFileType);
    btnOk = new QPushButton(tr("&Next"), gbFileType);
    btnCancel = new QPushButton(tr("&Cancel"), gbFileType);


    QGridLayout *fileTypeLayout = new QGridLayout; //4 rows, 2 columns
    QGridLayout *dialogLayout = new QGridLayout; //2 rows, 2 columns


    fileTypeLayout->addWidget(rbtnProgram,0,0,1,1);
    fileTypeLayout->addWidget(rbtnBank,1,0,1,1);
    fileTypeLayout->addWidget(lblNbProg,2,0);
    fileTypeLayout->addWidget(spbxBank,2,1);

    gbFileType->setLayout(fileTypeLayout);

    dialogLayout->addWidget(gbFileType,0,0,1,2);
    dialogLayout->addWidget(btnCancel,1,1);
    dialogLayout->addWidget(btnOk,1,0);

    this->setLayout(dialogLayout);

    this->layout()->setSizeConstraint(QLayout::SetFixedSize);


    //init values
    rbtnBank->setChecked(true);
    if(maxProgramPerBank < 1)
    {
        maxProgramPerBank = 1;
    }
    spbxBank->setRange(1, maxProgramPerBank);
    spbxBank->setSingleStep(10);
    spbxBank->setValue(maxProgramPerBank);

    // Disable Bank File Generation if requested.
    if(!isBankFileEnabled)
    {
        rbtnBank->setChecked(false);
        rbtnBank->setEnabled(false);
        spbxBank->setEnabled(false);
        lblNbProg->setEnabled(false);
        rbtnProgram->setChecked(true);
    }

    //Qt connections
    connect(rbtnBank, SIGNAL(toggled(bool)), spbxBank, SLOT(setEnabled(bool)));
    connect(rbtnBank, SIGNAL(toggled(bool)), lblNbProg, SLOT(setEnabled(bool)));
    connect(btnOk, SIGNAL(clicked()), this, SLOT(ProcessOk()));
    connect(btnCancel, SIGNAL(clicked()), this, SLOT(ProcessCancel()));
}



//-----------------------------------------------------------------------------
void FileTypeDialog::ProcessOk()
{
    if(rbtnBank->isChecked())
    {
        //close the dialogbox, and send the number of preset choosen
        done(FILE_TYPE_BANK);
    }
    else //single program file type is choosen
    {
        done(FILE_TYPE_PROGRAM);
    }
}

//-----------------------------------------------------------------------------
void FileTypeDialog::ProcessCancel()
{
    //close the dialogbox
    done(FILE_TYPE_INVALID);
}

//-----------------------------------------------------------------------------
int FileTypeDialog::getProgramPerBankValue() const
{
    return this->spbxBank->value();
}
