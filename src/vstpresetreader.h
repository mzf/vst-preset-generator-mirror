/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstpresetreader.h
  Author: Francois Mazen
  Date: 26/06/17 19:25
  Description: reader of preset files (fxp or fxb)
-------------------------------------------------------------------------------
*/

#ifndef VSTPRESETREADER_H
#define VSTPRESETREADER_H

#include "vstpreset.h"
#include "vsthost.h"

class FileReader;

class VSTPresetReader
{
public:
    enum ERROR_CODE
    {
        ERROR_NO_ERROR = 0,
        ERROR_CANNOT_OPEN_FILE,
        ERROR_INVALID_FILE,
        ERROR_UNKNOWN_PLUGIN,
        ERROR_WRONG_PLUGIN_VERSION,
        ERROR_END_OF_FILE
    };

    VSTPresetReader(const QSharedPointer<VSTHost>& vstHost);

    ERROR_CODE createVSTPresetsFromFile(const QString& filename, QList<QSharedPointer<VSTPreset> >& presets);

private:

    enum FILE_TYPE
    {
        UNKNOWN,
        PROGRAM_REGULAR,
        PROGRAM_CHUNK,
        BANK_REGULAR,
        BANK_CHUNK
    };

    struct FileHeader
    {
        int32_t fileSize;
        FILE_TYPE fileType;
        int32_t formatVersion;
        int32_t pluginID;
        int32_t pluginVersion;
    };

    ERROR_CODE createVSTPresetsFromFileWithException(const QString& filename, QList<QSharedPointer<VSTPreset> >& presets);
    FileHeader readFileHeader(QSharedPointer<FileReader>& file);
    QSharedPointer<VSTPreset> createPresetFromRegularProgram(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<FileReader>& file);
    QList<QSharedPointer<VSTPreset> > createPresetsFromRegularBank(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<FileReader>& file);
    QSharedPointer<VSTPreset> createPresetFromChunkProgram(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<FileReader>& file);
    QList<QSharedPointer<VSTPreset> > createPresetsFromChunkBank(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<FileReader>& file);

    QSharedPointer<VSTHost> vstHost;
};

#endif // VSTPRESETREADER_H
