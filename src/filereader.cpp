/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: filereader.cpp
  Author: Francois Mazen
  Date: 10/07/17 18:25
  Description: FileReader is a safe file reader that emits exception when
               end of file is reached.
-------------------------------------------------------------------------------
*/

#include "filereader.h"

//-----------------------------------------------------------------------------
FileReader::FileReader(const QString &filename)
{
    file = QSharedPointer<QFile>(new QFile(filename));
    if (!file->open(QIODevice::ReadOnly))
    {
        // Error!
        file.reset();
    }
}

//-----------------------------------------------------------------------------
QSharedPointer<FileReader> FileReader::create(const QString &filename)
{
    QSharedPointer<FileReader> fileReader;
    fileReader.reset(new FileReader(filename));
    if(fileReader && fileReader->file.isNull())
    {
        fileReader.reset();
    }

    return fileReader;
}

//-----------------------------------------------------------------------------
QByteArray FileReader::read(qint64 maxlen)
{
    if(file->atEnd() || file->bytesAvailable() < maxlen)
    {
        throw EndOfFileReachedException();
    }

    return file->read(maxlen);
}
