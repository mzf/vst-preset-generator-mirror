/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstlistmodel.h
  Author: Francois Mazen
  Date: 16/02/17 19:06
  Description: header file of the VSTListItem class
-------------------------------------------------------------------------------
*/

#ifndef VSTLISTITEM_H
#define VSTLISTITEM_H

#include <QSharedPointer>

class VSTPlugin;
class VSTPreset;


class VSTListItem
{
public:
    enum Type
    {
        VST_TYPE,
        PRESET_TYPE
    };

    VSTListItem(const QSharedPointer<VSTPlugin>& vstPlugin);
    VSTListItem(const QSharedPointer<VSTPreset>& vstPreset);

    Type getType() const;
    const QSharedPointer<VSTPlugin>& getVSTPlugin();
    const QSharedPointer<VSTPreset>& getVstPreset();

private:
    QSharedPointer<VSTPlugin> vstPlugin;
    QSharedPointer<VSTPreset> vstPreset;
};

#endif // VSTLISTITEM_H
