/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vsthost.cpp
  Author: Francois Mazen
  Date: 13/06/07 17:50
  Description: The class VSTHost can open a VST plug-in in order to load
               information about the plugin and avoid the "opaque chunk method"
               in the preset files.
-------------------------------------------------------------------------------
*/

#include "vsthost.h"
#include "vstsdk.h"

#include <QString>
#include <QLibrary>

// Some host constants.
#define HOST_BLOCKSIZE 512
#define HOST_SAMPLERATE 44100.0f

//-----------------------------------------------------------------------------
//VSTHost class
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
VSTHost::VSTHost() 
{
}

//-----------------------------------------------------------------------------
VSTHost::~VSTHost()
{
    unloadAll();
}

//-----------------------------------------------------------------------------
VSTHost::ERROR_CODE VSTHost::getOrLoadPlugin(const QString& pluginPath, QSharedPointer<VSTPlugin>& vstPlugin)
{
    VSTHost::ERROR_CODE errorCode = VSTHost::ERROR_NO_ERROR;

    PLUGIN_MAP::const_iterator pluginIterator = loadedPlugins.find(pluginPath);
    if(pluginIterator != loadedPlugins.end())
    {
        vstPlugin = pluginIterator->vstPlugin;
    }
    else
    {
        QLibrary* library = new QLibrary();

        library->setFileName(pluginPath);

        if(!library->load())
        {
            errorCode = ERROR_PLUGIN_LOAD;
        }

        AEffect* (__cdecl* getNewPlugInstance)(audioMasterCallback);
        if(VSTHost::ERROR_NO_ERROR == errorCode)
        {
            // DLL was loaded OK. Get the main function.
            getNewPlugInstance = (AEffect*(__cdecl*)(audioMasterCallback)) library->resolve("main");

            if (getNewPlugInstance == NULL)
            {
                // Try with the new symbol entry:
                getNewPlugInstance = (AEffect*(__cdecl*)(audioMasterCallback)) library->resolve("VSTPluginMain");
                if (getNewPlugInstance == NULL)
                {
                    errorCode = ERROR_PLUGIN_MAIN;
                }
            }
        }

        AEffect* audioEffect = NULL;
        if(VSTHost::ERROR_NO_ERROR == errorCode)
        {
            // Main function located OK
            audioEffect = getNewPlugInstance(&VSTHost::hostCallback);

            if (audioEffect == NULL)
            {
                errorCode = ERROR_PLUGIN_INSTANCE;
            }
        }

        if(VSTHost::ERROR_NO_ERROR == errorCode)
        {
            if (audioEffect->magic != kEffectMagic)
            {
               errorCode = ERROR_PLUGIN_VALID;
            }
            else
            {
                vstPlugin.reset(new VSTPlugin(pluginPath, audioEffect));
                loadedPlugins.insert(pluginPath, PluginHandle(vstPlugin, library));
            }
        }
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
QSharedPointer<VSTPlugin> VSTHost::getPluginByID(int32_t id) const
{
    QSharedPointer<VSTPlugin> plugin;

    for(PLUGIN_MAP::const_iterator pluginIterator = loadedPlugins.begin();
        pluginIterator != loadedPlugins.end() && plugin.isNull();
        ++pluginIterator)
    {
        QSharedPointer<VSTPlugin> currentPlugin = pluginIterator->vstPlugin;
        if(!currentPlugin.isNull() && currentPlugin->getFxId() == id)
        {
            plugin = currentPlugin;
        }
    }

    return plugin;
}

//-----------------------------------------------------------------------------
void VSTHost::unload(const QString& pluginPath)
{
    PLUGIN_MAP::iterator pluginIterator = loadedPlugins.find(pluginPath);
    if(pluginIterator != loadedPlugins.end())
    {
        unload(pluginIterator);
        loadedPlugins.erase(pluginIterator);
    }
}

//-----------------------------------------------------------------------------
void VSTHost::unload(const PLUGIN_MAP::iterator& pluginIterator)
{
    // VST plugin.
    pluginIterator->vstPlugin->sendCloseEvent();
    pluginIterator->vstPlugin.reset();

    // Qt library.
    if(pluginIterator->library->isLoaded())
    {
        pluginIterator->library->unload();
    }
    delete pluginIterator->library;
    pluginIterator->library = NULL;
}

//-----------------------------------------------------------------------------
void VSTHost::unloadAll()
{
    for(PLUGIN_MAP::iterator pluginIterator = loadedPlugins.begin();
        pluginIterator != loadedPlugins.end();
        ++pluginIterator)
    {
        unload(pluginIterator);
    }
    loadedPlugins.clear();
}

//-----------------------------------------------------------------------------
bool VSTHost::isVstLoaded(const QString& pluginPath) const
{
    return loadedPlugins.find(pluginPath) != loadedPlugins.end();
}

//-----------------------------------------------------------------------------
QList<QString> VSTHost::getLoadedFilenames() const
{
    QList<QString> loadedFilenames;
    for(PLUGIN_MAP::const_iterator pluginIterator = loadedPlugins.begin();
        pluginIterator != loadedPlugins.end();
        ++pluginIterator)
    {
        loadedFilenames.append(pluginIterator.key());
    }
    return loadedFilenames;
}

//-----------------------------------------------------------------------------
//host callback function
//this is called directly by the plug-in!!
//-----------------------------------------------------------------------------
// For the VPG, we just simulate the host
// so this function does nothing useful.
intptr_t VSTHost::hostCallback(AEffect *effect, int32_t opcode, int32_t /*index*/, intptr_t /*value*/, void *ptr, float /*opt*/)
{
    intptr_t retval = 0;

    switch (opcode)
    {
        //VST 1.0 opcodes
        case audioMasterVersion:
            //Input values:
            //none

            //Return Value:
            //0 or 1 for old version
            //2 or higher for VST2.0 host?
            //cout << "plug called audioMasterVersion" << endl;
            retval=2;
            break;

        case audioMasterAutomate:
            //Input values:
            //<index> parameter that has changed
            //<opt> new value

            //Return value:
            //not tested, always return 0

            //NB - this is called when the plug calls
            //setParameterAutomated

            //cout << "plug called audioMasterAutomate" << endl;
            break;

        case audioMasterCurrentId:
            //Input values:
            //none

            //Return Value
            //the unique id of a plug that's currently loading
            //zero is a default value and can be safely returned if not known
            //cout << "plug called audioMasterCurrentId" << endl;
            break;

        case audioMasterIdle:
            //Input values:
            //none

            //Return Value
            //not tested, always return 0

            //NB - idle routine should also call effEditIdle for all open editors
            //Sleep(1);
            //cout << "plug called audioMasterIdle" << endl;
            break;

        //case audioMasterPinConnected:
            //Input values:
            //<index> pin to be checked
            //<value> 0=input pin, non-zero value=output pin

            //Return values:
            //0=true, non-zero=false
            //cout << "plug called audioMasterPinConnected" << endl;
            //break;

        //VST 2.0 opcodes
        /*case audioMasterWantMidi:
            //Input Values:
            //<value> filter flags (which is currently ignored, no defined flags?)

            //Return Value:
            //not tested, always return 0
            cout << "plug called audioMasterWantMidi" << endl;
            break;*/

        case audioMasterGetTime:
            //Input Values:
            //<value> should contain a mask indicating which fields are required
            //...from the following list?
            //kVstNanosValid
            //kVstPpqPosValid
            //kVstTempoValid
            //kVstBarsValid
            //kVstCyclePosValid
            //kVstTimeSigValid
            //kVstSmpteValid
            //kVstClockValid

            //Return Value:
            //ptr to populated const VstTimeInfo structure (or 0 if not supported)

            //NB - this structure will have to be held in memory for long enough
            //for the plug to safely make use of it
            //cout << "plug called audioMasterGetTime" << endl;
            break;

        case audioMasterProcessEvents:
            //Input Values:
            //<ptr> Pointer to a populated VstEvents structure

            //Return value:
            //0 if error
            //1 if OK
            //cout << "plug called audioMasterProcessEvents" << endl;
            break;

        /*case audioMasterSetTime:
            //IGNORE!
            break;*/

        /*case audioMasterTempoAt:
            //Input Values:
            //<value> sample frame location to be checked

            //Return Value:
            //tempo (in bpm * 10000)
            cout << "plug called audioMasterTempoAt" << endl;
            break;*/

        /*case audioMasterGetNumAutomatableParameters:
            //Input Values:
            //None

            //Return Value:
            //number of automatable parameters
            //zero is a default value and can be safely returned if not known

            //NB - what exactly does this mean? can the host set a limit to the
            //number of parameters that can be automated?
            cout << "plug called audioMasterGetNumAutomatableParameters" << endl;
            break;*/

        /*case audioMasterGetParameterQuantization:
            //Input Values:
            //None

            //Return Value:
            //integer value for +1.0 representation,
            //or 1 if full single float precision is maintained
            //in automation.

            //NB - ***possibly bugged***
            //Steinberg notes say "parameter index in <value> (-1: all, any)"
            //but in aeffectx.cpp no parameters are taken or passed
            cout << "plug called audioMasterGetParameterQuantization" << endl;
            break;*/

        case audioMasterIOChanged:
            //Input Values:
            //None

            //Return Value:
            //0 if error
            //non-zero value if OK
            //cout << "plug called audioMasterIOChanged" << endl;
            break;

        /*case audioMasterNeedIdle:
            //Input Values:
            //None

            //Return Value:
            //0 if error
            //non-zero value if OK

            //NB plug needs idle calls (outside its editor window)
            //this means that effIdle must be dispatched to the plug
            //during host idle process and not effEditIdle calls only when its
            //editor is open
            //Check despatcher notes for any return codes from effIdle
            cout << "plug called audioMasterNeedIdle" << endl;
            break;*/

        case audioMasterSizeWindow:
            //Input Values:
            //<index> width
            //<value> height

            //Return Value:
            //0 if error
            //non-zero value if OK
            //cout << "plug called audioMasterSizeWindow" << endl;
            break;

        case audioMasterGetSampleRate:
            //Input Values:
            //None

            //Return Value:
            //not tested, always return 0

            //NB - Host must despatch effSetSampleRate to the plug in response
            //to this call
            //Check despatcher notes for any return codes from effSetSampleRate
            //cout << "plug called audioMasterGetSampleRate" << endl;
            effect->dispatcher(effect,effSetSampleRate,0,0,NULL,HOST_SAMPLERATE);
            break;

        case audioMasterGetBlockSize:
            //Input Values:
            //None

            //Return Value:
            //not tested, always return 0

            //NB - Host must despatch effSetBlockSize to the plug in response
            //to this call
            //Check despatcher notes for any return codes from effSetBlockSize
            //cout << "plug called audioMasterGetBlockSize" << endl;
            effect->dispatcher(effect,effSetBlockSize,0,HOST_BLOCKSIZE,NULL,0.0f);

            break;

        case audioMasterGetInputLatency:
            //Input Values:
            //None

            //Return Value:
            //input latency (in sampleframes?)
            //cout << "plug called audioMasterGetInputLatency" << endl;
            break;

        case audioMasterGetOutputLatency:
            //Input Values:
            //None

            //Return Value:
            //output latency (in sampleframes?)
            //cout << "plug called audioMasterGetOutputLatency" << endl;
            break;

        /*case audioMasterGetPreviousPlug:
            //Input Values:
            //None

            //Return Value:
            //pointer to AEffect structure or NULL if not known?

            //NB - ***possibly bugged***
            //Steinberg notes say "input pin in <value> (-1: first to come)"
            //but in aeffectx.cpp no parameters are taken or passed
            cout << "plug called audioMasterGetPreviousPlug" << endl;
            break;*/

        /*case audioMasterGetNextPlug:
            //Input Values:
            //None

            //Return Value:
            //pointer to AEffect structure or NULL if not known?

            //NB - ***possibly bugged***
            //Steinberg notes say "output pin in <value> (-1: first to come)"
            //but in aeffectx.cpp no parameters are taken or passed
            cout << "plug called audioMasterGetNextPlug" << endl;
            break;*/

        /*case audioMasterWillReplaceOrAccumulate:
            //Input Values:
            //None

            //Return Value:
            //0: not supported
            //1: replace
            //2: accumulate
            cout << "plug called audioMasterWillReplaceOrAccumulate" << endl;
            break;*/

        case audioMasterGetCurrentProcessLevel:
            //Input Values:
            //None

            //Return Value:
            //0: not supported,
            //1: currently in user thread (gui)
            //2: currently in audio thread (where process is called)
            //3: currently in 'sequencer' thread (midi, timer etc)
            //4: currently offline processing and thus in user thread
            //other: not defined, but probably pre-empting user thread.
            //cout << "plug called audioMasterGetCurrentProcessLevel" << endl;
            break;

        case audioMasterGetAutomationState:
            //Input Values:
            //None

            //Return Value:
            //0: not supported
            //1: off
            //2:read
            //3:write
            //4:read/write
            //cout << "plug called audioMasterGetAutomationState" << endl;
            break;

        case audioMasterGetVendorString:
            //Input Values:
            //<ptr> string (max 64 chars) to be populated

            //Return Value:
            //0 if error
            //non-zero value if OK
            //cout << "plug called audioMasterGetVendorString" << endl;
            break;

        case audioMasterGetProductString:
            //Input Values:
            //<ptr> string (max 64 chars) to be populated

            //Return Value:
            //0 if error
            //non-zero value if OK
            //cout << "plug called audioMasterGetProductString" << endl;
            break;

        case audioMasterGetVendorVersion:
            //Input Values:
            //None

            //Return Value:
            //Vendor specific host version as integer
            //cout << "plug called audioMasterGetVendorVersion" << endl;
            break;

        case audioMasterVendorSpecific:
            //Input Values:
            //<index> lArg1
            //<value> lArg2
            //<ptr> ptrArg
            //<opt>	floatArg

            //Return Values:
            //Vendor specific response as integer
            //cout << "plug called audioMasterVendorSpecific" << endl;
            break;

        /*case audioMasterSetIcon:
            //IGNORE
            break;*/

        case audioMasterCanDo:
            //Input Values:
            //<ptr> predefined "canDo" string

            //Return Value:
            //0 = Not Supported
            //non-zero value if host supports that feature

            //NB - Possible Can Do strings are:
            //"sendVstEvents",
            //"sendVstMidiEvent",
            //"sendVstTimeInfo",
            //"receiveVstEvents",
            //"receiveVstMidiEvent",
            //"receiveVstTimeInfo",
            //"reportConnectionChanges",
            //"acceptIOChanges",
            //"sizeWindow",
            //"asyncProcessing",
            //"offline",
            //"supplyIdle",
            //"supportShell"
            //cout << "plug called audioMasterCanDo" << endl;

            if (strcmp((char*)ptr,"sendVstEvents")==0 ||
                strcmp((char*)ptr,"sendVstMidiEvent")==0 ||
                strcmp((char*)ptr,"supplyIdle")==0)
            {
                retval=1;
            }
            else
            {
                retval=0;
            }

            break;

        case audioMasterGetLanguage:
            //Input Values:
            //None

            //Return Value:
            //kVstLangEnglish
            //kVstLangGerman
            //kVstLangFrench
            //kVstLangItalian
            //kVstLangSpanish
            //kVstLangJapanese
            //cout << "plug called audioMasterGetLanguage" << endl;
            retval=1;
            break;
    /*
        MAC SPECIFIC?

        case audioMasterOpenWindow:
            //Input Values:
            //<ptr> pointer to a VstWindow structure

            //Return Value:
            //0 if error
            //else platform specific ptr
            cout << "plug called audioMasterOpenWindow" << endl;
            break;

        case audioMasterCloseWindow:
            //Input Values:
            //<ptr> pointer to a VstWindow structure

            //Return Value:
            //0 if error
            //Non-zero value if OK
            cout << "plug called audioMasterCloseWindow" << endl;
            break;
    */
        case audioMasterGetDirectory:
            //Input Values:
            //None

            //Return Value:
            //0 if error
            //FSSpec on MAC, else char* as integer

            //NB Refers to which directory, exactly?
            //cout << "plug called audioMasterGetDirectory" << endl;
            break;

        case audioMasterUpdateDisplay:
            //Input Values:
            //None

            //Return Value:
            //Unknown
            //cout << "plug called audioMasterUpdateDisplay" << endl;
            break;
    }

    return retval;
}

