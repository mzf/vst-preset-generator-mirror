/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: paramvalueeditor.cpp
  Author: Francois Mazen
  Date: 12/04/17 19:25
  Description: Editor of a plugin parameter value, that displays the value
               from the plugin.
-------------------------------------------------------------------------------
*/

#include "parametervalueeditor.h"

#include "vstplugin.h"

#include <QDial>
#include <QEvent>
#include <QLabel>
#include <QSpinBox>
#include <QVBoxLayout>

#define SLIDER_MAX_RANGE 1000

//-----------------------------------------------------------------------------
ParameterValueEditor::ParameterValueEditor(QWidget *parent) :
    QWidget(parent),
    parameterIndex(-1),
    shouldUsePluginDisplay(true)
{
    slider = new QDial(this);
    slider->setRange(0, SLIDER_MAX_RANGE);
    slider->setSingleStep(1);
    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChanged(int)));

    display = new QLabel(this);
    display->setAlignment(Qt::AlignCenter);

    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->addWidget(display);
    layout->addWidget(slider);
    layout->setSpacing(0);
    layout->setMargin(0);
    setLayout(layout);

    setAutoFillBackground(true);

    Qt::WindowFlags flags = windowFlags();
    flags |= Qt::Popup;
    setWindowFlags(flags);
    raise();
    show();
}

//-----------------------------------------------------------------------------
void ParameterValueEditor::setPlugin(const QSharedPointer<VSTPlugin> newVSTPlugin)
{
    vstPlugin = newVSTPlugin;
}

//-----------------------------------------------------------------------------
void ParameterValueEditor::setParameterIndex(int index)
{
    parameterIndex = index;
}

//-----------------------------------------------------------------------------
void ParameterValueEditor::setShouldUsePluginDisplay(bool should)
{
    shouldUsePluginDisplay = should;
}

//-----------------------------------------------------------------------------
float ParameterValueEditor::getValue() const
{
    return float(slider->value()) / float(SLIDER_MAX_RANGE);
}

//-----------------------------------------------------------------------------
void ParameterValueEditor::setValue(float value)
{
    slider->setValue(int(value * float(SLIDER_MAX_RANGE)));
    updateParameterDisplay();
}

//-----------------------------------------------------------------------------
void ParameterValueEditor::sliderValueChanged(int /*value*/)
{
    updateParameterDisplay();
}

//-----------------------------------------------------------------------------
void ParameterValueEditor::updateParameterDisplay()
{
    if(!vstPlugin.isNull() && parameterIndex >= 0)
    {
        QString parameterDisplay;
        if(shouldUsePluginDisplay)
        {
            vstPlugin->setParameter(parameterIndex, getValue());
            parameterDisplay = vstPlugin->getParameterDisplay(parameterIndex);
        }

        if(parameterDisplay.isEmpty())
        {
            parameterDisplay.setNum(getValue());
        }

        display->setText(parameterDisplay);
    }
}


