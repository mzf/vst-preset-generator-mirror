/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgversion.cpp
  Author: Francois Mazen
  Date: 18/03/16 21:25
  Description: represents the version of the software
-------------------------------------------------------------------------------
*/

#include "vpgversion.h"

#include <QStringBuilder>

#ifdef _WIN64
    #define VPG_PLATFORM VpgVersion::PLATFORM_64_BITS
#elif __x86_64
    #define VPG_PLATFORM VpgVersion::PLATFORM_64_BITS
#else
    #define VPG_PLATFORM VpgVersion::PLATFORM_32_BITS
#endif

//-----------------------------------------------------------------------------
VpgVersion::VpgVersion():
    majorNumber(VPG_MAJOR_NUMBER),
    minorNumber(VPG_MINOR_NUMBER),
    revisionNumber(VPG_REVISION_NUMBER),
    platform(VPG_PLATFORM)
{

}

//-----------------------------------------------------------------------------
VpgVersion::VpgVersion(unsigned int majorNumber, unsigned int minorNumber, unsigned int revisionNumber):
    majorNumber(majorNumber),
    minorNumber(minorNumber),
    revisionNumber(revisionNumber),
    platform(VPG_PLATFORM)
{

}

//-----------------------------------------------------------------------------
const VpgVersion& VpgVersion::getCurrentVersion()
{
    static VpgVersion* currentVersion = NULL;
    if(currentVersion == NULL)
    {
        currentVersion = new VpgVersion();
    }

    return *currentVersion;
}

//-----------------------------------------------------------------------------
unsigned int VpgVersion::getMajorNumber() const
{
    return majorNumber;
}

//-----------------------------------------------------------------------------
unsigned int VpgVersion::getMinorNumber() const
{
    return minorNumber;
}

//-----------------------------------------------------------------------------
unsigned int VpgVersion::getRevisionNumber() const
{
    return revisionNumber;
}

//-----------------------------------------------------------------------------
VpgVersion::Platform VpgVersion::getPlatform() const
{
    return platform;
}

//-----------------------------------------------------------------------------
QString VpgVersion::getStringVersion() const
{
    QChar separator('.');
    QString version = QString::number(majorNumber) % separator % QString::number(minorNumber) % separator % QString::number(revisionNumber);
    return version;
}

//-----------------------------------------------------------------------------
QString VpgVersion::getStringPlatform() const
{
    QString platformString;
    switch(platform)
    {
    case PLATFORM_32_BITS:
        platformString = "32 bits";
        break;
    case PLATFORM_64_BITS:
        platformString = "64 bits";
        break;
    }

    return platformString;
}

//-----------------------------------------------------------------------------
QString VpgVersion::getStringVersionWithPlatform() const
{
    QString version = getStringVersion() % " - " % getStringPlatform();
    return version;
}

//-----------------------------------------------------------------------------
bool VpgVersion::operator <(const VpgVersion &other) const
{
    bool isLess = false;

    if(majorNumber < other.majorNumber)
    {
        isLess = true;
    }
    else if(majorNumber > other.majorNumber)
    {
        isLess = false;
    }
    else if(minorNumber < other.minorNumber)
    {
        isLess = true;
    }
    else if(minorNumber > other.minorNumber)
    {
        isLess = false;
    }
    else if(revisionNumber < other.revisionNumber)
    {
        isLess = true;
    }
    else if(revisionNumber > other.revisionNumber)
    {
        isLess = false;
    }

    return isLess;
}
