/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstpreset.h
  Author: Francois Mazen
  Date: 18/07/16 19:54
  Description: Class for VST Preset.
-------------------------------------------------------------------------------
*/


#ifndef VST_PRESET_H
#define VST_PRESET_H

#include "parameter.h"
#include <QVector>
#include <QObject>
#include <QSharedPointer>
#include <stdint.h>

class QFile;
class QString;
class VSTPlugin;
class VpgRandom;

class VSTPreset : public QObject
{      
    Q_OBJECT

public:
    enum ERROR_CODE
    {
        ERROR_NO_ERROR,
        ERROR_COULD_NOT_CREATE_FILE,
        ERROR_VST_HOST_INVALID_PROGRAM_NUMBER
    };

    VSTPreset(const QSharedPointer<VSTPlugin>& vstPlugin, const QString& presetName);
    VSTPreset(const QSharedPointer<VSTPlugin>& vstPlugin, int32_t programIndex);
    ~VSTPreset();

    ERROR_CODE writeProgramToFile(const QString& filename, const QSharedPointer<VpgRandom>& vpgRandom) const;
    ERROR_CODE writeBankToFile(const QString& filename, int programCountPerBank, const QSharedPointer<VpgRandom>& vpgRandom) const;
    ERROR_CODE writeProgramChunkToFile(const QString& filename, const QSharedPointer<VpgRandom>& vpgRandom) const;
    ERROR_CODE writeBankChunkToFile(const QString& filename, int programCountPerBank, const QSharedPointer<VpgRandom>& vpgRandom) const;

    const QVector<QSharedPointer<Parameter> > &getParameters() const;
    QVector<QSharedPointer<Parameter> >& getParametersForModification();
    const QSharedPointer<VSTPlugin>& getVSTPlugin() const;
    const QString& getName() const;
    void setName(const QString& newName);

signals:
    void dataChanged();

private slots:
    void parameterDataChanged();

private:
    ERROR_CODE createQFile(const QString& filename, QFile& file) const;
    ERROR_CODE writeRawProgram(QFile& file, int programNumber, const QSharedPointer<VpgRandom>& vpgRandom) const;

    QVector<QSharedPointer<Parameter> > parameters;
    const QSharedPointer<VSTPlugin> vstPlugin;
    QString name;
};

#endif //VST_PRESET_H
