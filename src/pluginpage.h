/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: pluginpage.cpp
  Author: Francois Mazen
  Date: 14/04/17 18:56
  Description: Widget to display plugin informations.
-------------------------------------------------------------------------------
*/

#ifndef PLUGINPAGE_H
#define PLUGINPAGE_H

#include <QWidget>
#include <QSharedPointer>

class VSTPlugin;
class QLabel;
class QPushButton;

class PluginPage : public QWidget
{
    Q_OBJECT
public:
    explicit PluginPage(QWidget *parent);

    void setVSTPlugin(const QSharedPointer<VSTPlugin>& newVSTPlugin);
    const QSharedPointer<VSTPlugin>& getVSTPlugin() const;

signals:
    void createPresetButtonClicked();
    void removePluginButtonClicked();
    void openPresetButtonClicked();

public slots:

private:

    void updateDisplayedInformations();
    QPushButton* createButton(const QIcon &icon, const QString& text, QWidget *parent) const;

    QSharedPointer<VSTPlugin> vstPlugin;
    QLabel* pluginPropertiesLabel;
    QLabel* pluginNameLabel;

};

#endif // PLUGINPAGE_H
