/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgversion.h
  Author: Francois Mazen
  Date: 18/03/16 21:25
  Description: represents the version of the software
-------------------------------------------------------------------------------
*/

#ifndef VPGVERSION_H
#define VPGVERSION_H

#include <QString>

// Represents a version of the vpg software.
// To get the current version use the accessor getCurrentVersion().
class VpgVersion
{
public:

    enum Platform
    {
        PLATFORM_32_BITS,
        PLATFORM_64_BITS
    };

    VpgVersion(unsigned int majorNumber, unsigned int minorNumber, unsigned int revisionNumber);
    static const VpgVersion& getCurrentVersion();

    unsigned int getMajorNumber() const;
    unsigned int getMinorNumber() const;
    unsigned int getRevisionNumber() const;
    Platform getPlatform() const;

    QString getStringVersion() const;
    QString getStringPlatform() const;
    QString getStringVersionWithPlatform() const;

    bool operator < (const VpgVersion& other) const;

private:
    VpgVersion();

    unsigned int majorNumber;
    unsigned int minorNumber;
    unsigned int revisionNumber;
    Platform platform;
};

#endif //VPGVERSION_H
