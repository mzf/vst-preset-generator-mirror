/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgsettings.h
  Author: Francois Mazen
  Date: 27/07/16 19:26
  Description: Class for application settings.
-------------------------------------------------------------------------------
*/


#ifndef VPGSETTINGS_H
#define VPGSETTINGS_H

#include <QString>
#include <QSettings>

class VpgSettings
{      
public:

    VpgSettings();
    ~VpgSettings();

    void setTranslationFilePath(const QString& filePath);
    QString getTranslationFilePath() const;
    bool hasTranslationFilePath() const;

    void setVpgFilesFolder(const QString& folder);
    QString getVpgFilesFolder() const;

    void setVSTPluginsFolder(const QString& folder);
    QString getVSTPluginsFolder() const;

    void setVSTPresetsFolder(const QString& folder);
    QString getVSTPresetsFolder() const;

    void setGeneratedPresetFolder(const QString& folder);
    QString getGeneratedPresetFolder() const;

private:
    QSettings* settings;
}; 

#endif //VPGSETTINGS_H
