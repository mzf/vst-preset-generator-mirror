############################################################################
#    This file is a part of the VST Preset Generator
#    Copyright (C) 2007-2017  Francois Mazen
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
############################################################################


TEMPLATE = app

# Target name
!contains(QMAKE_HOST.arch, x86_64) {
    message("arch: 32 bits")
    TARGET = vpg32
} else {
    message("arch: 64 bits")
    TARGET = vpg64
}
linux*:TARGET = vpg

# Version
VPG_MAJOR_NUMBER=0
VPG_MINOR_NUMBER=3
VPG_REVISION_NUMBER=0

VERSION=$${VPG_MAJOR_NUMBER}.$${VPG_MINOR_NUMBER}.$${VPG_REVISION_NUMBER}
DEFINES+= VPG_MAJOR_NUMBER=$$VPG_MAJOR_NUMBER
DEFINES+= VPG_MINOR_NUMBER=$$VPG_MINOR_NUMBER
DEFINES+= VPG_REVISION_NUMBER=$$VPG_REVISION_NUMBER
message(VPG version: $$VERSION)

# Windows dll informations
QMAKE_TARGET_PRODUCT="VST Preset Generator"
QMAKE_TARGET_COMPANY="VST Preset Generator"
QMAKE_TARGET_DESCRIPTION="Random Preset Generator for VST Plugins"
QMAKE_TARGET_COPYRIGHT="Copyright 2007-2017 Francois Mazen"

DEPENDPATH += . \
    translations
INCLUDEPATH += .

# VST SDK
exists(./vst2.x/aeffectx.h) {
    message("using VST SDK")
    HEADERS += vst2.x/aeffect.h \
               vst2.x/aeffectx.h \
               vst2.x/vstfxstore.h
} else {
    message("VST SDK not found, using VeSTige header as substitute")
    HEADERS += vestige/aeffectx.h \
               vestige/vestige.h
    DEFINES += USE_VESTIGE
}

# Input
HEADERS += filetypedialog.h \
    maindialog.h \
    parametertablemodel.h \
    parameter.h \
    randommodedelegate.h \
    vpglanguagechooser.h \
    vpgrandom.h \
    vstlistmodel.h \
    vpgversion.h \
    endiannessconverter.h \
    vpgfile.h \
    vstpreset.h \
    vpgsettings.h \
    vstlistitem.h \
    vstplugin.h \
    vsthost.h \
    vstsdk.h \
    vstlistview.h \
    actionbutton.h \
    presetitemnamedelegate.h \
    parametervalueeditor.h \
    pluginpage.h \
    parametervaluedelegate.h \
    vstpresetreader.h \
    filereader.h

SOURCES += filetypedialog.cpp \
    main.cpp \
    maindialog.cpp \
    parametertablemodel.cpp \
    parameter.cpp \
    randommodedelegate.cpp \
    vpglanguagechooser.cpp \
    vpgrandom.cpp \
    vstlistmodel.cpp \
    vpgversion.cpp \
    endiannessconverter.cpp \
    vpgfile.cpp \
    vstpreset.cpp \
    vpgsettings.cpp \
    vstlistitem.cpp \
    vstplugin.cpp \
    vsthost.cpp \
    vstlistview.cpp \
    actionbutton.cpp \
    presetitemnamedelegate.cpp \
    parametervalueeditor.cpp \
    pluginpage.cpp \
    parametervaluedelegate.cpp \
    vstpresetreader.cpp \
    filereader.cpp

DISTFILES += vpg_linux_deploy.pri

RESOURCES += vpg.qrc
TRANSLATIONS += translations/vpg_en.ts \
    translations/vpg_fr.ts
QT += xml \
    widgets

RC_ICONS="vpg.ico"

# Disable symbol __cdecl for linux platform because of issue in VST SDK.
linux*:QMAKE_CXXFLAGS += -D__cdecl=\"\"

QMAKE_LFLAGS += -static-libgcc

# Deploy on Linux:
include(vpg_linux_deploy.pri)

