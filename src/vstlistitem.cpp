/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstlistitem.cpp
  Author: Francois Mazen
  Date: 06/02/17 19:46
  Description: Holder for items in the vst list tree.
-------------------------------------------------------------------------------
*/

#include "vstlistitem.h"

//-----------------------------------------------------------------------------
VSTListItem::VSTListItem(const QSharedPointer<VSTPlugin> &vstPlugin) :
    vstPlugin(vstPlugin)
{

}

//-----------------------------------------------------------------------------
VSTListItem::VSTListItem(const QSharedPointer<VSTPreset>& vstPreset) :
    vstPreset(vstPreset)
{

}

//-----------------------------------------------------------------------------
VSTListItem::Type VSTListItem::getType() const
{
    return (vstPlugin.isNull()) ? PRESET_TYPE : VST_TYPE;
}

//-----------------------------------------------------------------------------
const QSharedPointer<VSTPlugin>& VSTListItem::getVSTPlugin()
{
    return vstPlugin;
}

//-----------------------------------------------------------------------------
const QSharedPointer<VSTPreset>& VSTListItem::getVstPreset()
{
    return vstPreset;
}
