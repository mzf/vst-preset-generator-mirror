/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: endiannessconverter.h
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: some tools because of big-endian/little-endian notation in the 
               VST preset files (fxb, fxp). See Steinberg VST SDK for more
               informations
-------------------------------------------------------------------------------
*/

#ifndef ENDIANNESS_CONVERTER_H
#define ENDIANNESS_CONVERTER_H

#include <QString>
#include <QByteArray>
#include <stdint.h>

class EndiannessConverter
{
public:
    static int32_t fromBigEndian(const QByteArray& bigEndianInputValue);
    static float floatFromBigEndian(const QByteArray& bigEndianInputValue);
    static QByteArray toBigEndian(const int32_t& littleEndianInputValue);
    static QByteArray toBigEndian(const float& littleEndianInputValue);
    static QString getMultiCharFromBigEndian(int32_t bigEndianInputValue);

private:
    static int32_t changeEndianness(const int32_t& value);
};

#endif //ENDIANNESS_CONVERTER_H
