/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: parametertablemodel.h
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: header file of the ParameterTableModel class
-------------------------------------------------------------------------------
*/

#ifndef PARAMETER_TABLE_MODEL_H
#define PARAMETER_TABLE_MODEL_H

#include <QAbstractTableModel>
#include <QSharedPointer>
#include "vstpreset.h"

class QString;


class ParameterTableModel : public  QAbstractTableModel
{
    Q_OBJECT

public:
    ParameterTableModel(const QSharedPointer<VSTPreset>& vstPreset, QObject *parent = 0);
    ~ParameterTableModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    //data display
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    //to edit data
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,
                int role = Qt::EditRole);

    void beginModelChangeWithoutRefresh();
    void endModelChangeWithoutRefresh();

    void ChangeRandomMethodForAll(Parameter::RandomMode method, const QSharedPointer<VpgRandom> &vpgRandom);

    const QSharedPointer<VSTPreset>& getVSTPreset() const;
    bool doesIndexUseParameterDisplay(const QModelIndex& index) const;

private:

    enum COLUMN_INDEX
    {
        COLUMN_NAME = 0,
        COLUMN_RANDOM_METHOD,
        COLUMN_RANDOM_VALUE_1,
        COLUMN_RANDOM_VALUE_2,
        COLUMN_COUNT // Always the last!
    };

    QString getParameterDisplay(int parameterIndex, float parameterValue) const;

    QSharedPointer<VSTPreset> vstPreset;
};

#endif //PARAMETER_TABLE_MODEL_H
