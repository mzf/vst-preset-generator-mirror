/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstpresetreader.cpp
  Author: Francois Mazen
  Date: 26/06/17 19:25
  Description: reader of preset files (fxp or fxb)
-------------------------------------------------------------------------------
*/

#include "vstpresetreader.h"

#include "endiannessconverter.h"
#include "filereader.h"
#include <QFile>

//-----------------------------------------------------------------------------
VSTPresetReader::VSTPresetReader(const QSharedPointer<VSTHost> &vstHost) :
    vstHost(vstHost)
{

}

//-----------------------------------------------------------------------------
VSTPresetReader::ERROR_CODE VSTPresetReader::createVSTPresetsFromFile(const QString &filename, QList<QSharedPointer<VSTPreset> >& presets)
{
    VSTPresetReader::ERROR_CODE errorCode = ERROR_NO_ERROR;
    try
    {
        errorCode = createVSTPresetsFromFileWithException(filename, presets);
    }
    catch(FileReader::EndOfFileReachedException&)
    {
        errorCode = ERROR_END_OF_FILE;
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
VSTPresetReader::ERROR_CODE VSTPresetReader::createVSTPresetsFromFileWithException(const QString &filename, QList<QSharedPointer<VSTPreset> >& presets)
{
    ERROR_CODE errorCode = ERROR_NO_ERROR;

    if(filename.isEmpty())
    {
        errorCode = ERROR_CANNOT_OPEN_FILE;
    }

    QSharedPointer<FileReader> file;
    if(errorCode == ERROR_NO_ERROR)
    {
        //open the file
        file = FileReader::create(filename);
        if (file.isNull())
        {
            errorCode = ERROR_CANNOT_OPEN_FILE;
        }
    }

    if(errorCode == ERROR_NO_ERROR)
    {
        QByteArray buffer = file->read(4);
        if(!buffer.startsWith("CcnK"))
        {
            //not a preset file
            errorCode = ERROR_INVALID_FILE;
        }
    }

    VSTPresetReader::FileHeader fileHeader;
    if(errorCode == ERROR_NO_ERROR)
    {
        fileHeader = readFileHeader(file);
        if(fileHeader.fileType == UNKNOWN)
        {
            errorCode = ERROR_INVALID_FILE;
        }
    }

    QSharedPointer<VSTPlugin> vstPlugin;
    if(errorCode == ERROR_NO_ERROR)
    {
        // Check if plugin is already loaded.
        vstPlugin = vstHost->getPluginByID(fileHeader.pluginID);
        if(vstPlugin.isNull())
        {
            errorCode = ERROR_UNKNOWN_PLUGIN;
        }
        // Ignore wrong plugin version for now.
        //else if(vstPlugin->getFxVersion() != fileHeader.pluginVersion)
        //{
        //    errorCode = ERROR_WRONG_PLUGIN_VERSION;
        //}
    }

    if(errorCode == ERROR_NO_ERROR)
    {
        QSharedPointer<VSTPreset> newPreset;
        QList<QSharedPointer<VSTPreset> > newPresets;
        switch(fileHeader.fileType)
        {
        case PROGRAM_REGULAR:
            newPreset = createPresetFromRegularProgram(vstPlugin, file);
            if(!newPreset.isNull())
            {
                presets.append(newPreset);
            }
            else
            {
                errorCode = ERROR_INVALID_FILE;
            }
            break;
        case PROGRAM_CHUNK:
            newPreset = createPresetFromChunkProgram(vstPlugin, file);
            if(!newPreset.isNull())
            {
                presets.append(newPreset);
            }
            else
            {
                errorCode = ERROR_INVALID_FILE;
            }
            break;
        case BANK_REGULAR:
            newPresets = createPresetsFromRegularBank(vstPlugin, file);
            if(!newPresets.empty())
            {
                presets.append(newPresets);
            }
            else
            {
                errorCode = ERROR_INVALID_FILE;
            }
            break;
        case BANK_CHUNK:
            newPresets = createPresetsFromChunkBank(vstPlugin, file);
            if(!newPresets.empty())
            {
                presets.append(newPresets);
            }
            else
            {
                errorCode = ERROR_INVALID_FILE;
            }
            break;
            break;
        default:
            errorCode = ERROR_UNKNOWN_PLUGIN;
            break;
        }
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
VSTPresetReader::FileHeader VSTPresetReader::readFileHeader(QSharedPointer<FileReader> &file)
{
    VSTPresetReader::FileHeader header;

    //chunk size
    header.fileSize = EndiannessConverter::fromBigEndian(file->read(4));

    //type of file (bank or program)
    header.fileType = UNKNOWN;
    QByteArray typeBuffer = file->read(4);
    if(typeBuffer.startsWith("FxCk"))
    {
        header.fileType = PROGRAM_REGULAR;
    }
    else if(typeBuffer.startsWith("FPCh"))
    {
        header.fileType = PROGRAM_CHUNK;
    }
    else if(typeBuffer.startsWith("FxBk") == true)
    {
        header.fileType = BANK_REGULAR;
    }
    else if(typeBuffer.startsWith("FBCh") == true)
    {
        header.fileType = BANK_CHUNK;
    }

    if(header.fileType != UNKNOWN)
    {
        //format version
        header.formatVersion = EndiannessConverter::fromBigEndian(file->read(4));

        //fx unique ID
        header.pluginID = EndiannessConverter::fromBigEndian(file->read(4));

        //fx version
        header.pluginVersion = EndiannessConverter::fromBigEndian(file->read(4));
    }

    return header;
}

//-----------------------------------------------------------------------------
QSharedPointer<VSTPreset> VSTPresetReader::createPresetFromRegularProgram(QSharedPointer<VSTPlugin>& vstPlugin, QSharedPointer<FileReader> &file)
{
    QSharedPointer<VSTPreset> newVSTPreset;

    int32_t parameterCount = EndiannessConverter::fromBigEndian(file->read(4));
    if(parameterCount == vstPlugin->getNumberParams())
    {
        // Read preset name.
        QString presetName = file->read(28);

        // Create the preset.
        newVSTPreset = QSharedPointer<VSTPreset>(new VSTPreset(vstPlugin, presetName));

        // Fill parameter values.
        QVector<QSharedPointer<Parameter> >& parameters = newVSTPreset->getParametersForModification();
        for(QVector<QSharedPointer<Parameter> >::iterator parameterIterator = parameters.begin();
            parameterIterator != parameters.end();
            ++parameterIterator)
        {
            (*parameterIterator)->setRandomMode(Parameter::RandomMode_Constant);
            (*parameterIterator)->setConstantValue(EndiannessConverter::floatFromBigEndian(file->read(4)));
        }
    }

    return newVSTPreset;
}

//-----------------------------------------------------------------------------
QList<QSharedPointer<VSTPreset> > VSTPresetReader::createPresetsFromRegularBank(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<FileReader> &file)
{
    QList<QSharedPointer<VSTPreset> > vstPresets;

    // Read number of presets.
    int32_t presetCount = EndiannessConverter::fromBigEndian(file->read(4));

    // Discard next 128 bytes.
    file->read(128);

    for(int32_t presetIndex = 0; presetIndex < presetCount; ++presetIndex)
    {
        // Discard preset header.
        file->read(4); // 'CcnK'
        file->read(4); // preset size
        file->read(4); // 'FxCk'
        file->read(4); // format version
        file->read(4); // plugin ID
        file->read(4); // plugin version

        QSharedPointer<VSTPreset> newVstPreset = createPresetFromRegularProgram(vstPlugin, file);
        if(!newVstPreset.isNull())
        {
            vstPresets.append(newVstPreset);
        }
    }


    return vstPresets;
}

//-----------------------------------------------------------------------------
QSharedPointer<VSTPreset> VSTPresetReader::createPresetFromChunkProgram(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<FileReader> &file)
{
    QSharedPointer<VSTPreset> newVSTPreset;

    // Ignore the parameter count with chunk program, it's meaningless.
    file->read(4);

    // Read preset name.
    QString presetName = file->read(28);

    // Create the preset.
    newVSTPreset = QSharedPointer<VSTPreset>(new VSTPreset(vstPlugin, presetName));

    // Read chunk size.
    int32_t chunkSize = EndiannessConverter::fromBigEndian(file->read(4));
    if(chunkSize > 0)
    {
        // Read chunk data and get parameters values.
        QByteArray chunkData = file->read(chunkSize);
        vstPlugin->setProgramChunk(chunkData.data(), chunkSize);

        QVector<QSharedPointer<Parameter> >& parameters = newVSTPreset->getParametersForModification();
        for(int parameterIndex = 0; parameterIndex < parameters.size(); ++parameterIndex)
        {
            QSharedPointer<Parameter>& parameter = parameters[parameterIndex];

            parameter->setRandomMode(Parameter::RandomMode_Constant);
            parameter->setConstantValue(vstPlugin->getParameterValue(parameterIndex));
        }
    }

    return newVSTPreset;
}

//-----------------------------------------------------------------------------
QList<QSharedPointer<VSTPreset> > VSTPresetReader::createPresetsFromChunkBank(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<FileReader> &file)
{
    QList<QSharedPointer<VSTPreset> > vstPresets;

    // Read number of presets.
    int32_t presetCount = EndiannessConverter::fromBigEndian(file->read(4));

    // Discard next 128 bytes.
    file->read(128);

    // Chunk size.
    int32_t chunkSize = EndiannessConverter::fromBigEndian(file->read(4));
    if(chunkSize > 0)
    {
        // Set the chunk and read the preset values.
        QByteArray chunkData = file->read(chunkSize);
        vstPlugin->setBankChunk(chunkData.data(), chunkSize);

        for(int32_t presetIndex = 0; presetIndex < presetCount; ++presetIndex)
        {
            QSharedPointer<VSTPreset> newPreset(new VSTPreset(vstPlugin, presetIndex));
            vstPresets.append(newPreset);
        }
    }

    return vstPresets;
}
