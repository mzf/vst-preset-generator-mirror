/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: paramvalueeditor.h
  Author: Francois Mazen
  Date: 12/04/17 19:25
  Description: Editor of a plugin parameter value, that displays the value
               from the plugin.
-------------------------------------------------------------------------------
*/

#ifndef PARAMETERVALUEEDITOR_H
#define PARAMETERVALUEEDITOR_H

#include <QWidget>
#include <QSharedPointer>

class QDial;
class QDoubleSpinBox;
class QLabel;
class VSTPlugin;

class ParameterValueEditor : public QWidget
{
    Q_OBJECT

public:
    ParameterValueEditor(QWidget *parent);

    void setPlugin(const QSharedPointer<VSTPlugin> newVSTPlugin);
    void setParameterIndex(int index);
    void setShouldUsePluginDisplay(bool should);

    float getValue() const;
    void setValue(float value);

signals:
    void editingFinished();

private slots:
    void sliderValueChanged(int value);

private:

    void updateParameterDisplay();

    QDial* slider;
    QLabel* display;

    int parameterIndex;
    QSharedPointer<VSTPlugin> vstPlugin;
    bool shouldUsePluginDisplay;
};

#endif // PARAMETERVALUEEDITOR_H
