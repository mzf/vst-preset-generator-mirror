/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: randommodedelegate.h
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: Object to edit the random mode in a table (see Qt's pattern)
-------------------------------------------------------------------------------
*/

#ifndef RANDOMMODEDELEGATE_H
#define RANDOMMODEDELEGATE_H


#include <QItemDelegate>
#include <QObject>
#include <QModelIndex>
#include <QComboBox>

class RandomModeDelegate : public QItemDelegate
{
    Q_OBJECT

public:
    RandomModeDelegate(QObject *parent = 0);

    QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;

    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;

    void updateEditorGeometry(QWidget *editor,
                              const QStyleOptionViewItem &option,
                              const QModelIndex &index) const;

private slots:
    void commitEditorData();

};

#endif //RANDOMMODEDELEGATE_H
