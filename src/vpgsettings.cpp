/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgsettings.cpp
  Author: Francois Mazen
  Date: 27/07/16 19:27
  Description: Class for application settings.
-------------------------------------------------------------------------------
*/


#include "vpgsettings.h"

#define DEFAULT_TRANSLATION_FILEPATH    ":/translations/vpg_en.qm"
#define DEFAULT_VPG_FILES_FOLDER        "C:/Program Files/Steinberg/Vstplugins"
#define DEFAULT_VSTPLUGINS_FOLDER       "C:/Program Files/Steinberg/Vstplugins"
#define DEFAULT_GENERATED_PRESET_FOLDER ""

//-----------------------------------------------------------------------------
VpgSettings::VpgSettings() :
    settings(NULL)
{
    settings = new QSettings(QSettings::IniFormat,
                             QSettings::UserScope,
                             "vpg",
                             "vpg");
}

//-----------------------------------------------------------------------------
VpgSettings::~VpgSettings()
{
    delete settings;
    settings = NULL;
}

//-----------------------------------------------------------------------------
void VpgSettings::setTranslationFilePath(const QString& filePath)
{
    settings->setValue("app/lang", filePath);
}

//-----------------------------------------------------------------------------
QString VpgSettings::getTranslationFilePath() const
{
    return settings->value("app/lang",QString(DEFAULT_TRANSLATION_FILEPATH)).toString();
}

//-----------------------------------------------------------------------------
bool VpgSettings::hasTranslationFilePath() const
{
    return settings->contains("app/lang");
}

//-----------------------------------------------------------------------------
void VpgSettings::setVpgFilesFolder(const QString& folder)
{
    settings->setValue("path/vpg", folder);
}

//-----------------------------------------------------------------------------
QString VpgSettings::getVpgFilesFolder() const
{
    return settings->value("path/vpg",QString(DEFAULT_VPG_FILES_FOLDER)).toString();
}

//-----------------------------------------------------------------------------
void VpgSettings::setVSTPluginsFolder(const QString& folder)
{
    settings->setValue("path/vst", folder);
}

//-----------------------------------------------------------------------------
QString VpgSettings::getVSTPluginsFolder() const
{
    return settings->value("path/vst",QString(DEFAULT_VSTPLUGINS_FOLDER)).toString();
}

//-----------------------------------------------------------------------------
void VpgSettings::setVSTPresetsFolder(const QString &folder)
{
    settings->setValue("path/fxp", folder);
}

//-----------------------------------------------------------------------------
QString VpgSettings::getVSTPresetsFolder() const
{
    QString folder = settings->value("path/fxp",QString(DEFAULT_VSTPLUGINS_FOLDER)).toString();
    if(folder.isEmpty())
    {
        folder = getVSTPluginsFolder();
    }

    return folder;
}

//-----------------------------------------------------------------------------
void VpgSettings::setGeneratedPresetFolder(const QString& folder)
{
    settings->setValue("path/patch", folder);
}

//-----------------------------------------------------------------------------
QString VpgSettings::getGeneratedPresetFolder() const
{
    return settings->value("path/patch",QString(DEFAULT_GENERATED_PRESET_FOLDER)).toString();
}
