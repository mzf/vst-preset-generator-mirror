/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: pluginpage.cpp
  Author: Francois Mazen
  Date: 14/04/17 18:56
  Description: Widget to display plugin informations.
-------------------------------------------------------------------------------
*/

#include "pluginpage.h"

#include "endiannessconverter.h"
#include "vstplugin.h"

#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>


//-----------------------------------------------------------------------------
PluginPage::PluginPage(QWidget *parent) : QWidget(parent)
{
    // Plugin title.
    pluginNameLabel = new QLabel(parent);
    pluginNameLabel->setAlignment(Qt::AlignCenter);

    // Plugin properties
    pluginPropertiesLabel = new QLabel(parent);
    pluginPropertiesLabel->setWordWrap(true);

    QGroupBox* infoGroupBox = new QGroupBox(tr("Plugin Information"), parent);
    QVBoxLayout* groupBoxLayout = new QVBoxLayout(infoGroupBox);
    groupBoxLayout->addWidget(pluginPropertiesLabel);
    infoGroupBox->setLayout(groupBoxLayout);

    // Buttons
    QPushButton* createPresetButton = createButton(QIcon(":/images/edit_add.png"), tr("Add new random preset for this plugin"), parent);
    connect(createPresetButton, SIGNAL(clicked(bool)), this, SIGNAL(createPresetButtonClicked()));

    QPushButton* removePluginButton = createButton(QIcon(":/images/button_cancel.png"), tr("Remove this plugin from the list"), parent);
    connect(removePluginButton, SIGNAL(clicked(bool)), this, SIGNAL(removePluginButtonClicked()));

    QPushButton* loadPresetButton = createButton(QIcon(":/images/folder_sound.png"), tr("Load Preset..."), parent);
    connect(loadPresetButton, SIGNAL(clicked(bool)), this, SIGNAL(openPresetButtonClicked()));

    QWidget* verticalButtonContainer = new QWidget(parent);
    QVBoxLayout* verticalButtonContainerLayout = new QVBoxLayout(verticalButtonContainer);
    verticalButtonContainerLayout->addWidget(createPresetButton);
    verticalButtonContainerLayout->addSpacing(5);
    verticalButtonContainerLayout->addWidget(loadPresetButton);
    verticalButtonContainerLayout->addSpacing(5);
    verticalButtonContainerLayout->addWidget(removePluginButton);
    verticalButtonContainer->setLayout(verticalButtonContainerLayout);

    QWidget* buttonContainer = new QWidget(parent);
    QHBoxLayout* buttonContainerLayout = new QHBoxLayout(buttonContainer);
    buttonContainerLayout->addStretch();
    buttonContainerLayout->addWidget(verticalButtonContainer);
    buttonContainerLayout->addStretch();
    buttonContainer->setLayout(buttonContainerLayout);



    QVBoxLayout* pluginPageLayout = new QVBoxLayout(this);
    pluginPageLayout->setAlignment(Qt::AlignHCenter);

    pluginPageLayout->addSpacing(50);
    pluginPageLayout->addWidget(pluginNameLabel);
    pluginPageLayout->addSpacing(10);
    pluginPageLayout->addWidget(buttonContainer);
    pluginPageLayout->addSpacing(10);
    pluginPageLayout->addWidget(infoGroupBox);
    pluginPageLayout->addStretch();

    setLayout(pluginPageLayout);

    updateDisplayedInformations();
}

//-----------------------------------------------------------------------------
QPushButton* PluginPage::createButton(const QIcon& icon, const QString& text, QWidget *parent) const
{
    QPushButton* button = new QPushButton(parent);
    button->setIcon(icon);
    button->setIconSize(QSize(32, 32));
    button->setStyleSheet("text-align:left;padding-left:10;");
    button->setLayout(new QGridLayout);
    button->setMinimumHeight(50);
    button->setMinimumWidth(350);
    QLabel* textLabel = new QLabel(text, button);
    textLabel->setAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    textLabel->setAttribute(Qt::WA_TransparentForMouseEvents, true);
    button->layout()->addWidget(textLabel);

    return button;
}

//-----------------------------------------------------------------------------
void PluginPage::setVSTPlugin(const QSharedPointer<VSTPlugin> &newVSTPlugin)
{
    vstPlugin = newVSTPlugin;
    updateDisplayedInformations();
}

//-----------------------------------------------------------------------------
const QSharedPointer<VSTPlugin> &PluginPage::getVSTPlugin() const
{
    return vstPlugin;
}

//-----------------------------------------------------------------------------
void PluginPage::updateDisplayedInformations()
{
    if(vstPlugin.isNull())
    {
        pluginNameLabel->setText("");
        pluginPropertiesLabel->setText(tr("No plug-in loaded"));
    }
    else
    {
        pluginNameLabel->setText("<h2>" + vstPlugin->getName() + "</h2>");

        QString description = tr("name: %1").arg(vstPlugin->getName());
        description += "\n";

        description += tr("VST ID: %1").arg(EndiannessConverter::getMultiCharFromBigEndian(vstPlugin->getFxId()));
        description += "\n";

        QString version;
        version.setNum(vstPlugin->getFxVersion());
        description += tr("version: %1").arg(version);
        description += "\n";

        QString parameterCount;
        parameterCount.setNum(vstPlugin->getNumberParams());
        description += tr("number of parameters: %1").arg(parameterCount);
        description += "\n";

        QString presetType;
        if(vstPlugin->useChunk())
        {
            presetType += tr("chunk");
        }
        else
        {
            presetType += tr("parameters");
        }
        description += tr("preset type: %1").arg(presetType);
        description += "\n";

        description += tr("file: %1").arg(vstPlugin->getFilePath());

        pluginPropertiesLabel->setText(description);
    }
}
