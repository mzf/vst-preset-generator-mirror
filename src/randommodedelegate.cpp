/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: randommodedelegate.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: Object to edit the random mode in a table (see Qt's pattern)
-------------------------------------------------------------------------------
*/

#include "randommodedelegate.h"

#include <QtWidgets>

//-----------------------------------------------------------------------------
RandomModeDelegate::RandomModeDelegate(QObject *parent) : QItemDelegate(parent)
{
}


//-----------------------------------------------------------------------------
QWidget *RandomModeDelegate::createEditor(QWidget *parent,
     const QStyleOptionViewItem &/* option */,
     const QModelIndex &/* index */) const
 {
     QComboBox *editor = new QComboBox(parent);
     editor->insertItem (0, QIcon(":/images/uniform.png"), tr("uniform"));
     editor->insertItem (1, QIcon(":/images/normal.png"), tr("normal"));
     editor->insertItem (2, QIcon(":/images/constant.png"), tr("constant"));

     editor->installEventFilter(const_cast<RandomModeDelegate*>(this));

     connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(commitEditorData()));

     return editor;
 }

//-----------------------------------------------------------------------------
void RandomModeDelegate::setEditorData(QWidget *editor,
                                     const QModelIndex &index) const
{
     int rawData = index.model()->data(index, Qt::EditRole).toInt();
     QComboBox *comboBox = static_cast<QComboBox*>(editor);
     comboBox->setCurrentIndex (rawData);
}

//-----------------------------------------------------------------------------
void RandomModeDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                    const QModelIndex &index) const
{
     QComboBox *comboBox = static_cast<QComboBox*>(editor);
     model->setData(index, comboBox->currentIndex());
}

//-----------------------------------------------------------------------------
void RandomModeDelegate::updateEditorGeometry(QWidget *editor,
     const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
     editor->setGeometry(option.rect);
}

//-----------------------------------------------------------------------------
void RandomModeDelegate::commitEditorData()
{
    QComboBox *editor = qobject_cast<QComboBox*>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
}









