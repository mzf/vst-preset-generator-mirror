/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vsthost.h
  Author: Francois Mazen
  Date: 13/06/07 17:50
  Description: header file of the VSTHost class
-------------------------------------------------------------------------------
*/

#ifndef VSTHOST_H
#define VSTHOST_H

#include "vstplugin.h"
#include <QHash>
#include <QSharedPointer>

class QLibrary;
class QString;
class AEffect;

//-----------------------------------------------------------------------------
// VSTHost
//-----------------------------------------------------------------------------

class VSTHost
{
public:

    enum ERROR_CODE
    {
        ERROR_NO_ERROR = 0,
        ERROR_PLUGIN_LOAD = 1,
        ERROR_PLUGIN_MAIN = 2,
        ERROR_PLUGIN_INSTANCE = 3,
        ERROR_PLUGIN_VALID = 4
    };

    VSTHost();
    ~VSTHost();

    ERROR_CODE getOrLoadPlugin(const QString& pluginPath, QSharedPointer<VSTPlugin>& vstPlugin);
    QSharedPointer<VSTPlugin> getPluginByID(int32_t id) const;
    void unload(const QString& pluginPath);
    void unloadAll();
    bool isVstLoaded(const QString& pluginPath) const;
    QList<QString> getLoadedFilenames() const;

private:  

    // Host call back, called directly by the loaded plugin.
    static intptr_t hostCallback(AEffect* effect, int32_t opcode, int32_t index, intptr_t, void * , float );

    //Nested class to handle pointers on the DLLs.
    class PluginHandle
    {
    public:
        PluginHandle(const QSharedPointer<VSTPlugin>& vstPlugin, QLibrary* library) :
            vstPlugin(vstPlugin), library(library)
        {

        }

        QSharedPointer<VSTPlugin> vstPlugin;
        QLibrary* library;
    };

    // Shortcut for plugin map manipulation.
    typedef QHash<QString, PluginHandle> PLUGIN_MAP;

    void unload(const PLUGIN_MAP::iterator& pluginIterator);

    // List of loaded plugins.
    PLUGIN_MAP loadedPlugins;
};

#endif //VSTHOST_H
