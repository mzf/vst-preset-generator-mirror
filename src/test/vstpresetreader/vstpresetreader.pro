include(../vpgtests_commons.pri)

QT       -= gui
QT += widgets

TARGET = vstpresetreader_test

HEADERS += ../../vstpresetreader.h \
           ../../vstpreset.h \
           ../../vsthost.h \
           ../../vstplugin.h \
           ../../endiannessconverter.h \
           ../../vpgrandom.h \
           ../../parameter.h \
           ../../filereader.h

SOURCES += vstpresetreader_test.cpp \
           ../../vstpresetreader.cpp \
           ../../vstpreset.cpp \
           ../../vsthost.cpp \
           ../../vstplugin.cpp \
           ../../endiannessconverter.cpp \
           ../../vpgrandom.cpp \
           ../../parameter.cpp \
           ../../filereader.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
