/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstpresetreader_test.cpp
  Author: Francois Mazen
  Date: 10/07/17 19:06
  Description: a test class for the VSTPresetReader class
-------------------------------------------------------------------------------
*/

#include "../../vstpresetreader.h"
#include "../../endiannessconverter.h"
#include "../../parameter.h"

#include <QtTest>
#include <cstdio>

#ifdef WIN64
    #define MOCKPLUGIN_FILENAME "mockplugin_win64.dll"
    #define MOCKCHUNKPLUGIN_FILENAME "mockchunkplugin_win64.dll"
#elif WIN32
    #define MOCKPLUGIN_FILENAME "mockplugin_win32.dll"
    #define MOCKCHUNKPLUGIN_FILENAME "mockchunkplugin_win32.dll"
#elif __x86_64
    #define MOCKPLUGIN_FILENAME "libmockplugin_linux.so"
    #define MOCKCHUNKPLUGIN_FILENAME "libmockchunkplugin_linux.so"
#endif

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)


class VSTPresetReaderTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testEmptyFilename();
    void testInvalidFilename();
    void testInvalidFile();
    void testUnknownPresetTypeFile();
    void testUnknownPluginID();
    void testEndOfFile();

    void testLoadRegularPreset();
    void testLoadRegularBank();
    void testLoadChunkPreset();
    void testLoadChunkBank();

private:
    void createAndInitVSTHost(QSharedPointer<VSTHost> &vstHost);
};

void VSTPresetReaderTest::createAndInitVSTHost(QSharedPointer<VSTHost>& vstHost)
{
    vstHost.reset(new VSTHost());
    QVERIFY(!vstHost.isNull());

    QDir mockPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo plugInFileInfo(mockPluginDir, MOCKPLUGIN_FILENAME);
    QVERIFY(plugInFileInfo.exists());

    QSharedPointer<VSTPlugin> mockPlugin;
    VSTHost::ERROR_CODE loadingErrorCode = vstHost->getOrLoadPlugin(plugInFileInfo.absoluteFilePath(), mockPlugin);
    QCOMPARE(loadingErrorCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(mockPlugin.isNull(), false);

    QDir mockChunkPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo chunkPlugInFileInfo(mockChunkPluginDir, MOCKCHUNKPLUGIN_FILENAME);
    QVERIFY(chunkPlugInFileInfo.exists());

    QSharedPointer<VSTPlugin> mockChunkPlugin;
    loadingErrorCode = vstHost->getOrLoadPlugin(chunkPlugInFileInfo.absoluteFilePath(), mockChunkPlugin);
    QCOMPARE(loadingErrorCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(mockChunkPlugin.isNull(), false);

    QCOMPARE(vstHost->getLoadedFilenames().size(), 2);
}

void VSTPresetReaderTest::testEmptyFilename()
{
    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile("", presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_CANNOT_OPEN_FILE);
    QVERIFY(presets.isEmpty());
}

void VSTPresetReaderTest::testInvalidFilename()
{
    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile("/invalid/file/path", presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_CANNOT_OPEN_FILE);
    QVERIFY(presets.isEmpty());
}

void VSTPresetReaderTest::testInvalidFile()
{
    QFile presetFile("invalid.fxp");
    QVERIFY(presetFile.open(QIODevice::WriteOnly));
    presetFile.write("This is not a preset file!!!!");
    presetFile.close();

    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile(presetFile.fileName(), presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_INVALID_FILE);
    QVERIFY(presets.isEmpty());
}

void VSTPresetReaderTest::testUnknownPresetTypeFile()
{
    QFile presetFile("invalid.fxp");
    QVERIFY(presetFile.open(QIODevice::WriteOnly));
    presetFile.write("CcnK");
    presetFile.write(EndiannessConverter::toBigEndian(60));
    presetFile.write("INVALID MAGIC WORD");
    presetFile.close();

    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile(presetFile.fileName(), presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_INVALID_FILE);
    QVERIFY(presets.isEmpty());
}

void VSTPresetReaderTest::testUnknownPluginID()
{
    QFile presetFile("invalid.fxp");
    QVERIFY(presetFile.open(QIODevice::WriteOnly));
    presetFile.write("CcnK");
    presetFile.write(EndiannessConverter::toBigEndian(60));
    presetFile.write("FxCk");
    presetFile.write(EndiannessConverter::toBigEndian(1)); // format version
    presetFile.write("UNKNOWN PLUGIN ID");
    presetFile.close();

    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile(presetFile.fileName(), presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_UNKNOWN_PLUGIN);
    QVERIFY(presets.isEmpty());
}

void VSTPresetReaderTest::testEndOfFile()
{
    QFile presetFile("invalid.fxp");
    QVERIFY(presetFile.open(QIODevice::WriteOnly));
    presetFile.write("CcnK");
    presetFile.write(EndiannessConverter::toBigEndian(60));
    presetFile.write("FxCk");
    presetFile.write(EndiannessConverter::toBigEndian(1)); // format version
    presetFile.write("MCK1");
    presetFile.write(EndiannessConverter::toBigEndian(1)); // fx version
    presetFile.write(EndiannessConverter::toBigEndian(3)); // number of parameters
    presetFile.close();

    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile(presetFile.fileName(), presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_END_OF_FILE);
    QVERIFY(presets.isEmpty());
}

void VSTPresetReaderTest::testLoadRegularPreset()
{
    // Create a regular preset for mockplugin.
    QFile presetFile("regularPreset.fxp");
    QVERIFY(presetFile.open(QIODevice::WriteOnly));
    presetFile.write("CcnK");
    presetFile.write(EndiannessConverter::toBigEndian(60));
    presetFile.write("FxCk");
    presetFile.write(EndiannessConverter::toBigEndian(1)); // format version
    presetFile.write("MCK1");
    presetFile.write(EndiannessConverter::toBigEndian(1)); // fx version
    presetFile.write(EndiannessConverter::toBigEndian(3)); // number of parameters
    char name[28];
    memset(name, 0, 28);
    memcpy(name, "The Preset Name", 28);
    presetFile.write(name, 28); // preset name (28 chars)
    presetFile.write(EndiannessConverter::toBigEndian(0.756f));
    presetFile.write(EndiannessConverter::toBigEndian(0.336f));
    presetFile.write(EndiannessConverter::toBigEndian(0.997f));
    presetFile.close();

    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile(presetFile.fileName(), presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_NO_ERROR);
    QCOMPARE(presets.size(), 1);
    QSharedPointer<VSTPreset> preset = presets[0];
    QCOMPARE(preset.isNull(), false);
    QCOMPARE(preset->getVSTPlugin()->getName(), QString("VPG Mock Plugin"));
    QCOMPARE(preset->getName(), QString("The Preset Name"));
    QCOMPARE(preset->getParameters().size(), 3);
    QCOMPARE(preset->getParameters()[0]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[0]->getConstantValue(), 0.756f);
    QCOMPARE(preset->getParameters()[1]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[1]->getConstantValue(), 0.336f);
    QCOMPARE(preset->getParameters()[2]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[2]->getConstantValue(), 0.997f);
}

void VSTPresetReaderTest::testLoadRegularBank()
{
    // Create a regular bank for mockplugin.
    QFile presetFile("regularBank.fxb");
    QVERIFY(presetFile.open(QIODevice::WriteOnly));
    presetFile.write("CcnK");
    presetFile.write(EndiannessConverter::toBigEndian(284));
    presetFile.write("FxBk");
    presetFile.write(EndiannessConverter::toBigEndian(2)); // format version
    presetFile.write("MCK1");
    presetFile.write(EndiannessConverter::toBigEndian(1)); // fx version
    presetFile.write(EndiannessConverter::toBigEndian(2)); // number of programs
    presetFile.write(EndiannessConverter::toBigEndian(1)); // current program
    char dummy[124];
    memset(dummy,0,124);
    presetFile.write(dummy, 124); // 124 dummy bytes.

    // Write 2 presets.

    for(int presetIndex = 0; presetIndex < 2; ++presetIndex)
    {
        presetFile.write("CcnK");
        presetFile.write(EndiannessConverter::toBigEndian(60));
        presetFile.write("FxCk");
        presetFile.write(EndiannessConverter::toBigEndian(1)); // format version
        presetFile.write("MCK1");
        presetFile.write(EndiannessConverter::toBigEndian(1)); // fx version
        presetFile.write(EndiannessConverter::toBigEndian(3)); // number of parameters
        char name[28];
        memset(name, 0, 28);
        memcpy(name, "Preset Name ", 28);
        sprintf(name+12, "%d", presetIndex);
        presetFile.write(name, 28); // preset name (28 chars)
        presetFile.write(EndiannessConverter::toBigEndian(0.756f + ((float)presetIndex) * 0.01f));
        presetFile.write(EndiannessConverter::toBigEndian(0.336f + ((float)presetIndex) * 0.01f));
        presetFile.write(EndiannessConverter::toBigEndian(0.997f - ((float)presetIndex) * 0.01f));
    }

    presetFile.close();

    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile(presetFile.fileName(), presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_NO_ERROR);
    QCOMPARE(presets.size(), 2);

    // First Preset
    QSharedPointer<VSTPreset> preset = presets[0];
    QCOMPARE(preset.isNull(), false);
    QCOMPARE(preset->getVSTPlugin()->getName(), QString("VPG Mock Plugin"));
    QCOMPARE(preset->getName(), QString("Preset Name 0"));
    QCOMPARE(preset->getParameters().size(), 3);
    QCOMPARE(preset->getParameters()[0]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[0]->getConstantValue(), 0.756f);
    QCOMPARE(preset->getParameters()[1]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[1]->getConstantValue(), 0.336f);
    QCOMPARE(preset->getParameters()[2]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[2]->getConstantValue(), 0.997f);

    // Second Preset
    preset = presets[1];
    QCOMPARE(preset.isNull(), false);
    QCOMPARE(preset->getVSTPlugin()->getName(), QString("VPG Mock Plugin"));
    QCOMPARE(preset->getName(), QString("Preset Name 1"));
    QCOMPARE(preset->getParameters().size(), 3);
    QCOMPARE(preset->getParameters()[0]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[0]->getConstantValue(), 0.766f);
    QCOMPARE(preset->getParameters()[1]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[1]->getConstantValue(), 0.346f);
    QCOMPARE(preset->getParameters()[2]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[2]->getConstantValue(), 0.987f);
}

void VSTPresetReaderTest::testLoadChunkPreset()
{
    // Create a chunk preset for mockplugin.
    QFile presetFile("chunkPreset.fxp");
    QVERIFY(presetFile.open(QIODevice::WriteOnly));
    presetFile.write("CcnK");
    presetFile.write(EndiannessConverter::toBigEndian(87));
    presetFile.write("FPCh");
    presetFile.write(EndiannessConverter::toBigEndian(2)); // format version
    presetFile.write("MCK2");
    presetFile.write(EndiannessConverter::toBigEndian(1)); // fx version
    presetFile.write(EndiannessConverter::toBigEndian(3)); // number of parameters
    char name[28];
    memset(name, 0, 28);
    memcpy(name, "The Preset Name", 28);
    presetFile.write(name, 28); // preset name (28 chars)
    presetFile.write(EndiannessConverter::toBigEndian(35)); // Chunk size
    presetFile.write("PROGRAM;0.886595;0.546395;0.0876326");
    presetFile.close();

    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile(presetFile.fileName(), presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_NO_ERROR);
    QCOMPARE(presets.size(), 1);
    QSharedPointer<VSTPreset> preset = presets[0];
    QCOMPARE(preset.isNull(), false);
    QCOMPARE(preset->getVSTPlugin()->getName(), QString("VPG Mock Chunk Plugin"));
    QCOMPARE(preset->getName(), QString("The Preset Name"));
    QCOMPARE(preset->getParameters().size(), 3);
    QCOMPARE(preset->getParameters()[0]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[0]->getConstantValue(), 0.886595f);
    QCOMPARE(preset->getParameters()[1]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[1]->getConstantValue(), 0.546395f);
    QCOMPARE(preset->getParameters()[2]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[2]->getConstantValue(), 0.0876326f);
}

void VSTPresetReaderTest::testLoadChunkBank()
{
    // Create a chunk bank for mockplugin.
    QFile presetFile("chunkBank.fxb");
    QVERIFY(presetFile.open(QIODevice::WriteOnly));
    presetFile.write("CcnK");
    presetFile.write(EndiannessConverter::toBigEndian(184));
    presetFile.write("FBCh");
    presetFile.write(EndiannessConverter::toBigEndian(2)); // format version
    presetFile.write("MCK2");
    presetFile.write(EndiannessConverter::toBigEndian(1)); // fx version
    presetFile.write(EndiannessConverter::toBigEndian(2)); // number of presets
    char name[128];
    memset(name, 0, 128);
    presetFile.write(name, 128); // 128 dummy bytes
    presetFile.write(EndiannessConverter::toBigEndian(32)); // Chunk size
    presetFile.write("BANK;0.886123;0.546123;0.0876456");
    presetFile.close();

    QSharedPointer<VSTHost> vstHost;
    createAndInitVSTHost(vstHost);
    VSTPresetReader presetReader(vstHost);

    QList<QSharedPointer<VSTPreset> > presets;
    VSTPresetReader::ERROR_CODE errorCode = presetReader.createVSTPresetsFromFile(presetFile.fileName(), presets);
    QCOMPARE(errorCode, VSTPresetReader::ERROR_NO_ERROR);
    QCOMPARE(presets.size(), 2);

    QSharedPointer<VSTPreset> preset = presets[0];
    QCOMPARE(preset.isNull(), false);
    QCOMPARE(preset->getVSTPlugin()->getName(), QString("VPG Mock Chunk Plugin"));
    QCOMPARE(preset->getName(), QString("Default"));
    QCOMPARE(preset->getParameters().size(), 3);
    QCOMPARE(preset->getParameters()[0]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[0]->getConstantValue(), 0.886123f);
    QCOMPARE(preset->getParameters()[1]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[1]->getConstantValue(), 0.546123f);
    QCOMPARE(preset->getParameters()[2]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[2]->getConstantValue(), 0.0876456f);

    preset = presets[1];
    QCOMPARE(preset.isNull(), false);
    QCOMPARE(preset->getVSTPlugin()->getName(), QString("VPG Mock Chunk Plugin"));
    QCOMPARE(preset->getName(), QString("Default"));
    QCOMPARE(preset->getParameters().size(), 3);
    QCOMPARE(preset->getParameters()[0]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[0]->getConstantValue(), 0.886123f);
    QCOMPARE(preset->getParameters()[1]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[1]->getConstantValue(), 0.546123f);
    QCOMPARE(preset->getParameters()[2]->getRandomMode(), Parameter::RandomMode_Constant);
    QCOMPARE(preset->getParameters()[2]->getConstantValue(), 0.0876456f);
}




QTEST_GUILESS_MAIN(VSTPresetReaderTest)

#include "vstpresetreader_test.moc"
