/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vsthost_test.cpp
  Author: Francois Mazen
  Date: 14/11/13 20:10
  Description: a test class for the VpgVstHost class
-------------------------------------------------------------------------------
*/

#include <QtTest>
#include "../../vsthost.h"
#include "../../vstsdk.h"

#ifdef WIN64
    #define MOCKPLUGIN_FILENAME "mockplugin_win64.dll"
    #define MOCKCHUNKPLUGIN_FILENAME "mockchunkplugin_win64.dll"
#elif WIN32
    #define MOCKPLUGIN_FILENAME "mockplugin_win32.dll"
    #define MOCKCHUNKPLUGIN_FILENAME "mockchunkplugin_win32.dll"
#elif __x86_64
    #define MOCKPLUGIN_FILENAME "libmockplugin_linux.so"
    #define MOCKCHUNKPLUGIN_FILENAME "libmockchunkplugin_linux.so"
#endif

#define MOCKPLUGIN_ID (CCONST('M', 'C', 'K', '1'))
#define MOCKPLUGIN_NAME "VPG Mock Plugin"

#define MOCKCHUNKPLUGIN_ID CCONST('M','C','K','2')
#define MOCKCHUNKPLUGIN_NAME "VPG Mock Chunk Plugin"

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

class VSTHostTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testEmptyHost();
    void loadUnloadPlugIn();
    void testVSTPlugin();
    void testProgramChunkVSTPlugin();
    void testBankChunkVSTPlugin();

private:
    void loadRegularPlugin(VSTHost& vstHost, QSharedPointer<VSTPlugin>& vstPlugin);
    void loadChunkPlugin(VSTHost& vstHost, QSharedPointer<VSTPlugin>& vstPlugin);
};

void VSTHostTest::testEmptyHost()
{
    VSTHost vstHost;
    QVERIFY(vstHost.getLoadedFilenames().isEmpty());
    QCOMPARE(vstHost.isVstLoaded("DUMMY"), false);
}

void VSTHostTest::loadUnloadPlugIn()
{
    VSTHost vstHost;

    QDir mockPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo plugInFileInfo(mockPluginDir, MOCKPLUGIN_FILENAME);
    QVERIFY(plugInFileInfo.exists());

    QSharedPointer<VSTPlugin> vstPlugin;
    VSTHost::ERROR_CODE returnCode = vstHost.getOrLoadPlugin(plugInFileInfo.absoluteFilePath(), vstPlugin);
    QCOMPARE(returnCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(vstPlugin.isNull(), false);

    QList<QString> loadedFilenames = vstHost.getLoadedFilenames();
    QCOMPARE(loadedFilenames.size(), 1);
    QFileInfo loadedFileInfo(loadedFilenames[0]);
    QCOMPARE(loadedFileInfo, plugInFileInfo);
    QCOMPARE(vstHost.isVstLoaded(plugInFileInfo.absoluteFilePath()), true);

    vstHost.unload(plugInFileInfo.absoluteFilePath());
    loadedFilenames = vstHost.getLoadedFilenames();
    QCOMPARE(loadedFilenames.size(), 0);
    QCOMPARE(vstHost.isVstLoaded(plugInFileInfo.absoluteFilePath()), false);

    // Load two times the same.
    vstPlugin.reset();
    returnCode = vstHost.getOrLoadPlugin(plugInFileInfo.absoluteFilePath(), vstPlugin);
    QCOMPARE(returnCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(vstPlugin.isNull(), false);
    QSharedPointer<VSTPlugin> previousPointer = vstPlugin;
    returnCode = vstHost.getOrLoadPlugin(plugInFileInfo.absoluteFilePath(), vstPlugin);
    QCOMPARE(returnCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(vstPlugin.isNull(), false);
    QCOMPARE(vstPlugin.data(), previousPointer.data());
    loadedFilenames = vstHost.getLoadedFilenames();
    QCOMPARE(loadedFilenames.size(), 1);

    vstHost.unloadAll();
    loadedFilenames = vstHost.getLoadedFilenames();
    QCOMPARE(loadedFilenames.size(), 0);
}

void VSTHostTest::loadRegularPlugin(VSTHost &vstHost, QSharedPointer<VSTPlugin> &vstPlugin)
{
    QDir mockPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo plugInFileInfo(mockPluginDir, MOCKPLUGIN_FILENAME);
    QVERIFY(plugInFileInfo.exists());
    VSTHost::ERROR_CODE returnCode = vstHost.getOrLoadPlugin(plugInFileInfo.absoluteFilePath(), vstPlugin);
    QCOMPARE(returnCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(vstPlugin.isNull(), false);

    int32_t fxId = MOCKPLUGIN_ID;
    QCOMPARE(vstPlugin->getFxId(), fxId);
    QCOMPARE(vstPlugin->getFxVersion(), 1000);

    QString name = vstPlugin->getName();
    QCOMPARE(name.isEmpty(), false);
    QCOMPARE(name, QString(MOCKPLUGIN_NAME));
}

void VSTHostTest::loadChunkPlugin(VSTHost &vstHost, QSharedPointer<VSTPlugin> &vstPlugin)
{
    QDir mockPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo plugInFileInfo(mockPluginDir, MOCKCHUNKPLUGIN_FILENAME);
    QVERIFY(plugInFileInfo.exists());
    VSTHost::ERROR_CODE returnCode = vstHost.getOrLoadPlugin(plugInFileInfo.absoluteFilePath(), vstPlugin);
    QCOMPARE(returnCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(vstPlugin.isNull(), false);

    int32_t fxId = MOCKCHUNKPLUGIN_ID;
    QCOMPARE(vstPlugin->getFxId(), fxId);
    QCOMPARE(vstPlugin->getFxVersion(), 1000);

    QString name = vstPlugin->getName();
    QCOMPARE(name.isEmpty(), false);
    QCOMPARE(name, QString(MOCKCHUNKPLUGIN_NAME));
}

void VSTHostTest::testVSTPlugin()
{
    VSTHost vstHost;
    QSharedPointer<VSTPlugin> vstPlugin;
    loadRegularPlugin(vstHost, vstPlugin);

    QCOMPARE(vstPlugin->getNumberParams(), 3);
    QCOMPARE(vstPlugin->getNumberPrograms(), 2);

    for(int32_t paramIndex = 0; paramIndex < 3; paramIndex++)
    {
        QString parameterName = vstPlugin->getParameterName(paramIndex);
        QCOMPARE(parameterName.isEmpty(), false);
        QString paramName("Param");
        paramName.append(QString::number(paramIndex + 1));
        QCOMPARE(parameterName, paramName);
    }

    QCOMPARE(vstPlugin->getParameterValue(0), 1.0f);
    QCOMPARE(vstPlugin->getParameterValue(1), 0.5f);
    QCOMPARE(vstPlugin->getParameterValue(2), 0.0f);

    QCOMPARE(vstPlugin->getParameterDisplay(0), QString("0.000000 Label1"));
    QCOMPARE(vstPlugin->getParameterDisplay(1), QString("-6.02059 Label2"));
    QCOMPARE(vstPlugin->getParameterDisplay(2), QString("-oo Label3"));

    vstPlugin->setParameter(0, 0.f);
    QCOMPARE(vstPlugin->getParameterDisplay(0), QString("-oo Label1"));
    vstPlugin->setParameter(0, 1.f);
    QCOMPARE(vstPlugin->getParameterDisplay(0), QString("0.000000 Label1"));


    void* chunkPointer = NULL;
    vstPlugin->getProgramChunk(&chunkPointer);
    QCOMPARE(chunkPointer, (void*)NULL);

    chunkPointer = NULL;
    vstPlugin->getBankChunk(&chunkPointer);
    QCOMPARE(chunkPointer, (void*)NULL);

    QCOMPARE(vstPlugin->useChunk(), false);

    vstHost.unloadAll();
    QCOMPARE(vstHost.getLoadedFilenames().size(), 0);
}

void VSTHostTest::testProgramChunkVSTPlugin()
{
    VSTHost vstHost;
    QSharedPointer<VSTPlugin> vstPlugin;
    loadChunkPlugin(vstHost, vstPlugin);

    QString expectedChunk = "PROGRAM;1;0.5;0";
    void* chunkPointer = NULL;
    int32_t chunkSize = vstPlugin->getProgramChunk(&chunkPointer);
    QCOMPARE(chunkSize, expectedChunk.size());

    QString result = (char*)chunkPointer;
    QCOMPARE(result, expectedChunk);

    // Test write chunk.
    QString newChunk = "PROGRAM;0.1;0.2;0.3";
    vstPlugin->setProgramChunk(newChunk.toLatin1().data(), newChunk.size());

    QCOMPARE(vstPlugin->getParameterValue(0), 0.1f);
    QCOMPARE(vstPlugin->getParameterValue(1), 0.2f);
    QCOMPARE(vstPlugin->getParameterValue(2), 0.3f);

}

void VSTHostTest::testBankChunkVSTPlugin()
{
    VSTHost vstHost;
    QSharedPointer<VSTPlugin> vstPlugin;
    loadChunkPlugin(vstHost, vstPlugin);

    QString expectedChunk = "BANK;1;0.5;0";
    void* chunkPointer = NULL;
    int32_t chunkSize = vstPlugin->getBankChunk(&chunkPointer);
    QCOMPARE(chunkSize, expectedChunk.size());

    QString result = (char*)chunkPointer;
    QCOMPARE(result, expectedChunk);

    // Test write chunk.
    QString newChunk = "BANK;0.4;0.5;0.6";
    vstPlugin->setBankChunk(newChunk.toLatin1().data(), newChunk.size());

    QCOMPARE(vstPlugin->getParameterValue(0), 0.4f);
    QCOMPARE(vstPlugin->getParameterValue(1), 0.5f);
    QCOMPARE(vstPlugin->getParameterValue(2), 0.6f);
}

QTEST_GUILESS_MAIN(VSTHostTest)

#include "vsthost_test.moc"
