include(../vpgtests_commons.pri)

QT       -= gui
QT += widgets

TARGET = vsthost_test

HEADERS += ../../vsthost.h

SOURCES += vsthost_test.cpp \
           ../../vsthost.cpp \
           ../../vstplugin.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
