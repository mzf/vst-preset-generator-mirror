include(../vpgtests_commons.pri)

QT       -= gui

TARGET = vpgsettings_test

HEADERS += ../../vpgsettings.h

SOURCES += vpgsettings_test.cpp \
           ../../vpgsettings.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
