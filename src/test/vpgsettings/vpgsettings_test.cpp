/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgsettings_test.cpp
  Author: Francois Mazen
  Date: 27/07/16 20:13
  Description: a test class for the VpgSettings class
-------------------------------------------------------------------------------
*/

#include "../../vpgsettings.h"

#include <QtTest>

class VpgSettingsTest : public QObject
{
    Q_OBJECT

private:
    QString getIniFilePath() const;
    QString iniFilePath;
    QString backupIniFilePath;

private Q_SLOTS:

    void initTestCase();

    void init();
    void cleanup();

    void testDefaultValues();
    void testPersistency();
};

QString VpgSettingsTest::getIniFilePath() const
{
    QString filePath;
    QProcessEnvironment processEnvironement = QProcessEnvironment::systemEnvironment();

    // Compute ini file path according to the platform.
    if(QSysInfo::windowsVersion() == QSysInfo::WV_None)
    {
        // Assume linux platform.
        QString homeFolder = processEnvironement.value("HOME");
        filePath = homeFolder + "/.config/vpg/vpg.ini";
    }
    else
    {
        // Assume windows platform.
        QString appDataFolder = processEnvironement.value("APPDATA");
        filePath = appDataFolder + "/vpg/vpg.ini";
    }

    return filePath;
}

void VpgSettingsTest::initTestCase()
{
    iniFilePath = getIniFilePath();
    backupIniFilePath = "vpg_ini.backup";
}

void VpgSettingsTest::init()
{
    // Before each test, save the current ini file to backup,
    // and remove it.

    if(QFile(backupIniFilePath).exists())
    {
        QFile(backupIniFilePath).remove();
    }

    if(QFile(iniFilePath).exists())
    {
        // Back up before removing.
        QFile::copy(iniFilePath, backupIniFilePath);
        QFile(iniFilePath).remove();
    }
}

void VpgSettingsTest::cleanup()
{
    // After each test, restore the backup ini file.

    if(QFile(iniFilePath).exists())
    {
        QFile(iniFilePath).remove();
    }

    if(QFile(backupIniFilePath).exists())
    {
        // Restore backup ini file.
        QFile::copy(backupIniFilePath, iniFilePath);
        QFile(backupIniFilePath).remove();
    }

}

void VpgSettingsTest::testDefaultValues()
{
    QCOMPARE(QFile(getIniFilePath()).exists(), false);

    VpgSettings vpgSettings;
    QCOMPARE(vpgSettings.hasTranslationFilePath(), false);
    QCOMPARE(vpgSettings.getTranslationFilePath(), QString(":/translations/vpg_en.qm"));
    QCOMPARE(vpgSettings.getVpgFilesFolder(), QString("C:/Program Files/Steinberg/Vstplugins"));
    QCOMPARE(vpgSettings.getVSTPluginsFolder(), QString("C:/Program Files/Steinberg/Vstplugins"));
    QCOMPARE(vpgSettings.getVSTPresetsFolder(), QString("C:/Program Files/Steinberg/Vstplugins"));
    QCOMPARE(vpgSettings.getGeneratedPresetFolder(), QString());
}

void VpgSettingsTest::testPersistency()
{
    QCOMPARE(QFile(getIniFilePath()).exists(), false);

    QString translationFilePath("translation/file/path");
    QString generatedPresetFolder("generated/preset/folder");
    QString vpgFilesFolder("vpg/file/folder");
    QString vstPluginsFolder("vst/plugins/folder");
    QString vstPresetsFolder("vst/preset/folder");

    {
        VpgSettings vpgSettings;
        vpgSettings.setTranslationFilePath(translationFilePath);
        vpgSettings.setGeneratedPresetFolder(generatedPresetFolder);
        vpgSettings.setVpgFilesFolder(vpgFilesFolder);
        vpgSettings.setVSTPluginsFolder(vstPluginsFolder);
        vpgSettings.setVSTPresetsFolder(vstPresetsFolder);
    }

    QCOMPARE(QFile(getIniFilePath()).exists(), true);

    VpgSettings newVpgSettings;
    QCOMPARE(newVpgSettings.hasTranslationFilePath(), true);
    QCOMPARE(newVpgSettings.getTranslationFilePath(),translationFilePath);
    QCOMPARE(newVpgSettings.getVpgFilesFolder(), vpgFilesFolder);
    QCOMPARE(newVpgSettings.getVSTPluginsFolder(), vstPluginsFolder);
    QCOMPARE(newVpgSettings.getVSTPresetsFolder(), vstPresetsFolder);
    QCOMPARE(newVpgSettings.getGeneratedPresetFolder(), generatedPresetFolder);
}

QTEST_APPLESS_MAIN(VpgSettingsTest)

#include "vpgsettings_test.moc"
