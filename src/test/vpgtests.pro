TEMPLATE = subdirs

SUBDIRS += endiannessconverter \
    vpgfile \
    vpgrandom \
    vpgsettings \
    vpgversion \
    vsthost \
    vstpreset \
    vpglanguagechooser \
    vstpresetreader

DISTFILES += vpgtests_commons.pri
