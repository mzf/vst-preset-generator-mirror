
include(../vpgtests_commons.pri)

QT       -= gui

TARGET = endiannessconverter_test

HEADERS += ../../endiannessconverter.h

SOURCES += endiannessconverter_test.cpp \
           ../../endiannessconverter.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
