/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: endiannessconverter_test.cpp
  Author: Francois Mazen
  Date: 27/06/16 20:47
  Description: a test class for the EndiannessConverter class
-------------------------------------------------------------------------------
*/

#include <QtTest>
#include "../../endiannessconverter.h"

class EndiannessConverterTest : public QObject
{
    Q_OBJECT

public:

private Q_SLOTS:
    void testfromBigEndian();
    void testToBigEndian();
    void testMulticharFromBigEndian();
    void testToAndFromBigEndian();
    void testFloatFromBigEndian();
    void testFloatToBigEndian();
};

void EndiannessConverterTest::testfromBigEndian()
{
    QByteArray input;
    input.resize(4);
    input[0] = 0xAB;
    input[1] = 0xCD;
    input[2] = 0xEF;
    input[3] = 0x42;
    QCOMPARE(EndiannessConverter::fromBigEndian(input), -1412567230);
}

void EndiannessConverterTest::testToBigEndian()
{
    int32_t value = -1412567230;
    QByteArray output = EndiannessConverter::toBigEndian(value);
    QCOMPARE(output.at(0), char(0xAB));
    QCOMPARE(output.at(1), char(0xCD));
    QCOMPARE(output.at(2), char(0xEF));
    QCOMPARE(output.at(3), char(0x42));
}

void EndiannessConverterTest::testMulticharFromBigEndian()
{
    int32_t value = 1145258561; // == CCONST('D', 'C', 'B', 'A');
    QString result = EndiannessConverter::getMultiCharFromBigEndian(value);
    QVERIFY(result == "DCBA");
}

void EndiannessConverterTest::testToAndFromBigEndian()
{
    int32_t value = 1000;
    QByteArray output = EndiannessConverter::toBigEndian(value);
    QCOMPARE(EndiannessConverter::fromBigEndian(output), value);
}

void EndiannessConverterTest::testFloatFromBigEndian()
{
    float value = 0.123456f;
    int32_t memory = 0;
    memcpy(&memory, &value, sizeof(memory));
    QByteArray output = EndiannessConverter::toBigEndian(memory);

    QCOMPARE(EndiannessConverter::floatFromBigEndian(output), value);
}

void EndiannessConverterTest::testFloatToBigEndian()
{
    float value = 0.768048f;
    QByteArray output = EndiannessConverter::toBigEndian(value);

    QCOMPARE(output.at(0), char(0x3F));
    QCOMPARE(output.at(1), char(0x44));
    QCOMPARE(output.at(2), char(0x9E));
    QCOMPARE(output.at(3), char(0xCB));
}

QTEST_APPLESS_MAIN(EndiannessConverterTest)

#include "endiannessconverter_test.moc"
