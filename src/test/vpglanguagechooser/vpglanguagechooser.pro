include(../vpgtests_commons.pri)

QT += widgets

TARGET = vpglanguagechooser_test

HEADERS += ../../vpglanguagechooser.h

SOURCES += vpglanguagechooser_test.cpp \
           ../../vpglanguagechooser.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

RESOURCES += ../../vpg.qrc
