/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpglanguagechooser_test.cpp
  Author: Francois Mazen
  Date: 01/08/16 21:42
  Description: a test class for the VpgLanguageChooser class
-------------------------------------------------------------------------------
*/

#include <QtTest>
#include <QComboBox>

#include "../../vpglanguagechooser.h"

#define ENGLISH_LANGUAGE_PATH ":/translations/vpg_en.qm"
#define FRENCH_LANGUAGE_PATH ":/translations/vpg_fr.qm"

class VpgLanguageChooserTest : public QObject
{
    Q_OBJECT

public:

private Q_SLOTS:
    void testClickOK();
    void testClickOKWithComboBox();
    void testClickCancel();
    void testClickClose();
};


void VpgLanguageChooserTest::testClickCancel()
{
#if defined(WIN64) || defined(WIN32)
    QPoint cancelButtonPosition(120, 70);
#else //linux
    QPoint cancelButtonPosition(50, 85);
#endif

    VpgLanguageChooser languageChooser;
    languageChooser.show();
    //QTest::mouseMove(&languageChooser, cancelButtonPosition); // To check position.
    QWidget* cancelButton = languageChooser.childAt(cancelButtonPosition);
    QVERIFY(cancelButton != 0);

    QTest::mouseClick(cancelButton, Qt::LeftButton, Qt::NoModifier, QPoint(10,10), 1);
    QCOMPARE(languageChooser.result(), (int)VpgLanguageChooser::RETURN_CODE_NO_LANGUAGE_CHOOSEN);

    QCOMPARE(languageChooser.getChoosenLanguageFilePath(), QString(ENGLISH_LANGUAGE_PATH));
}

void VpgLanguageChooserTest::testClickOK()
{
#if defined(WIN64) || defined(WIN32)
    QPoint okButtonPosition(50, 70);
#else //linux
    QPoint okButtonPosition(150, 85);
#endif

    VpgLanguageChooser languageChooser;
    languageChooser.show();
    //QTest::mouseMove(&languageChooser, okButtonPosition); // To check position.
    QWidget* okButton = languageChooser.childAt(okButtonPosition);
    QVERIFY(okButton != 0);

    QTest::mouseClick(okButton, Qt::LeftButton, Qt::NoModifier, QPoint(10,10), 1);
    QCOMPARE(languageChooser.result(), (int)VpgLanguageChooser::RETURN_CODE_LANGUAGE_CHOOSEN);

    QCOMPARE(languageChooser.getChoosenLanguageFilePath(), QString(ENGLISH_LANGUAGE_PATH));
}

void VpgLanguageChooserTest::testClickOKWithComboBox()
{
#if defined(WIN64) || defined(WIN32)
    QPoint okButtonPosition(50, 70);
    QPoint comboBoxPosition(50, 40);
#else //linux
    QPoint okButtonPosition(150, 85);
    QPoint comboBoxPosition(50, 50);
#endif

    VpgLanguageChooser languageChooser;
    languageChooser.show();
    //QTest::mouseMove(&languageChooser, comboBoxPosition); // To check position.
    QWidget* okButton = languageChooser.childAt(okButtonPosition);
    QVERIFY(okButton != 0);

    QWidget* widget = languageChooser.childAt(comboBoxPosition);
    QComboBox* comboBox = dynamic_cast<QComboBox*>(widget);
    QVERIFY(comboBox != 0);

    QVERIFY(comboBox->count() >= 2);
    comboBox->setCurrentIndex(1);

    QTest::mouseClick(okButton, Qt::LeftButton, Qt::NoModifier, QPoint(10,10), 1);
    QCOMPARE(languageChooser.result(), (int)VpgLanguageChooser::RETURN_CODE_LANGUAGE_CHOOSEN);

    QCOMPARE(languageChooser.getChoosenLanguageFilePath(), QString(FRENCH_LANGUAGE_PATH));
}

void VpgLanguageChooserTest::testClickClose()
{
    VpgLanguageChooser languageChooser;
    languageChooser.show();
    languageChooser.close();

    QCOMPARE(languageChooser.result(), (int)VpgLanguageChooser::RETURN_CODE_NO_LANGUAGE_CHOOSEN);

    QCOMPARE(languageChooser.getChoosenLanguageFilePath(), QString(ENGLISH_LANGUAGE_PATH));
}

QTEST_MAIN(VpgLanguageChooserTest)

#include "vpglanguagechooser_test.moc"
