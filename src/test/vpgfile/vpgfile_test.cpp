/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgfile_test.cpp
  Author: Francois Mazen
  Date: 28/06/16 20:47
  Description: a test class for the VpgFile class
-------------------------------------------------------------------------------
*/

#include "../../vpgfile.h"
#include "../../vpgversion.h"
#include "../../vstlistmodel.h"
#include "../../vsthost.h"
#include "../../endiannessconverter.h"
#include "../../vstsdk.h"
#include "../../vstpreset.h"

#include <QtXml/QDomDocument>

#include <QtTest>

#ifdef WIN64
    #define MOCKPLUGIN_FILENAME "mockplugin_win64.dll"
    #define MOCKCHUNKPLUGIN_FILENAME "mockchunkplugin_win64.dll"
#elif WIN32
    #define MOCKPLUGIN_FILENAME "mockplugin_win32.dll"
    #define MOCKCHUNKPLUGIN_FILENAME "mockchunkplugin_win32.dll"
#elif __x86_64
    #define MOCKPLUGIN_FILENAME "libmockplugin_linux.so"
    #define MOCKCHUNKPLUGIN_FILENAME "libmockchunkplugin_linux.so"
#endif

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

class VpgFileTest : public QObject
{
    Q_OBJECT

public:
    VpgFileTest();

private Q_SLOTS:

    void initTestCase();
    void cleanupTestCase();

    void init();
    void cleanup();

    void testWriteFileErrorCodes();
    void testWriteFile();

    void testReadFileErrorCodes();
    void testReadLegacy026File();
    void testReadFile();

    void testWriteThenReadFile();

private:

    bool areEqual(float first, float second) const;

    QSharedPointer<VSTListModel> vstListModel;
    QSharedPointer<VSTHost> vstHost;

    QFileInfo testVpgFile;
    QFileInfo validVPGFile;
    QFileInfo testReadWriteVpgFile;
    QFileInfo nonXMLTestFile;
    QFileInfo nonVPGTestFile;
    QFileInfo nonVstTestFile;
    QFileInfo invalidXMlElementTestFile;
};

//-----------------------------------------------------------------------------
VpgFileTest::VpgFileTest() :
    testVpgFile("testVpgFile.vpg"),
    validVPGFile("validVPGFile"),
    testReadWriteVpgFile("testReadWriteVpgFile.vpg"),
    nonXMLTestFile("nonXMLTestFile"),
    nonVPGTestFile("nonVPGTestFile"),
    nonVstTestFile("nonVstTestFile"),
    invalidXMlElementTestFile("invalidXMlElementTestFile")
{
    this->vstHost.reset(new VSTHost());
    this->testVpgFile.setCaching(false);
    this->validVPGFile.setCaching(false);
    this->testReadWriteVpgFile.setCaching(false);
    this->nonXMLTestFile.setCaching(false);
    this->nonVPGTestFile.setCaching(false);
    this->nonVstTestFile.setCaching(false);
    this->invalidXMlElementTestFile.setCaching(false);
}

//-----------------------------------------------------------------------------
void VpgFileTest::initTestCase()
{
    this->vstListModel.reset(new VSTListModel(NULL));
}

//-----------------------------------------------------------------------------
void VpgFileTest::cleanupTestCase()
{
    this->vstListModel.reset();

    // Remove all written files during tests.
    if(this->testVpgFile.exists())
    {
        QFile::remove(this->testVpgFile.absoluteFilePath());
    }
    if(this->validVPGFile.exists())
    {
        QFile::remove(this->validVPGFile.absoluteFilePath());
    }
    if(this->testReadWriteVpgFile.exists())
    {
        QFile::remove(this->testReadWriteVpgFile.absoluteFilePath());
    }
    if(this->nonXMLTestFile.exists())
    {
        QFile::remove(this->nonXMLTestFile.absoluteFilePath());
    }
    if(this->nonVPGTestFile.exists())
    {
        QFile::remove(this->nonVPGTestFile.absoluteFilePath());
    }
    if(this->nonVstTestFile.exists())
    {
        QFile::remove(this->nonVstTestFile.absoluteFilePath());
    }
    if(this->invalidXMlElementTestFile.exists())
    {
        QFile::remove(this->invalidXMlElementTestFile.absoluteFilePath());
    }
}

//-----------------------------------------------------------------------------
bool VpgFileTest::areEqual(float first, float second) const
{
    return fabs(first - second) < 0.001;
}

//-----------------------------------------------------------------------------
void VpgFileTest::init()
{
    this->vstListModel->clearList();

    // Load the mock plugins.
    QDir mockPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo plugInFileInfo(mockPluginDir, MOCKPLUGIN_FILENAME);
    QVERIFY(plugInFileInfo.exists());

    QSharedPointer<VSTPlugin> mockPlugin;
    VSTHost::ERROR_CODE loadingErrorCode = this->vstHost->getOrLoadPlugin(plugInFileInfo.absoluteFilePath(), mockPlugin);
    QCOMPARE(loadingErrorCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(mockPlugin.isNull(), false);
    this->vstListModel->addPlugin(mockPlugin);

    QDir mockChunkPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo chunkPlugInFileInfo(mockChunkPluginDir, MOCKCHUNKPLUGIN_FILENAME);
    QVERIFY(chunkPlugInFileInfo.exists());

    QSharedPointer<VSTPlugin> mockChunkPlugin;
    loadingErrorCode = this->vstHost->getOrLoadPlugin(chunkPlugInFileInfo.absoluteFilePath(), mockChunkPlugin);
    QCOMPARE(loadingErrorCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(mockChunkPlugin.isNull(), false);
    this->vstListModel->addPlugin(mockChunkPlugin);

    QCOMPARE(this->vstListModel->getPlugins().size(), 2);

    // Add preset to the plugins.
    QSharedPointer<VSTPreset> mockPluginPreset(new VSTPreset(mockPlugin, "Mock Plugin Preset"));
    mockPluginPreset->getParametersForModification()[0]->setRandomMode(Parameter::RandomMode_Constant);
    mockPluginPreset->getParametersForModification()[0]->setConstantValue(0.5);
    mockPluginPreset->getParametersForModification()[1]->setRandomMode(Parameter::RandomMode_Uniform);
    mockPluginPreset->getParametersForModification()[1]->setUniformMinValue(0.1);
    mockPluginPreset->getParametersForModification()[1]->setUniformMaxValue(0.9);
    mockPluginPreset->getParametersForModification()[2]->setRandomMode(Parameter::RandomMode_Normal);
    mockPluginPreset->getParametersForModification()[2]->setNormalMiddleValue(0.4);
    mockPluginPreset->getParametersForModification()[2]->setNormalStandardDeviation(0.2);
    this->vstListModel->addPreset(mockPluginPreset);
    QCOMPARE(this->vstListModel->getPluginPresets(mockPlugin).size(), 1);

    QSharedPointer<VSTPreset> mockChunkPluginPreset1(new VSTPreset(mockChunkPlugin, "Mock Chunk Plugin Preset 1"));
    mockChunkPluginPreset1->getParametersForModification()[0]->setRandomMode(Parameter::RandomMode_Constant);
    mockChunkPluginPreset1->getParametersForModification()[0]->setConstantValue(0.5);
    mockChunkPluginPreset1->getParametersForModification()[1]->setRandomMode(Parameter::RandomMode_Uniform);
    mockChunkPluginPreset1->getParametersForModification()[1]->setUniformMinValue(0.1);
    mockChunkPluginPreset1->getParametersForModification()[1]->setUniformMaxValue(0.9);
    mockChunkPluginPreset1->getParametersForModification()[2]->setRandomMode(Parameter::RandomMode_Normal);
    mockChunkPluginPreset1->getParametersForModification()[2]->setNormalMiddleValue(0.4);
    mockChunkPluginPreset1->getParametersForModification()[2]->setNormalStandardDeviation(0.2);
    this->vstListModel->addPreset(mockChunkPluginPreset1);

    QSharedPointer<VSTPreset> mockChunkPluginPreset2(new VSTPreset(mockChunkPlugin, "Mock Chunk Plugin Preset 2"));
    mockChunkPluginPreset2->getParametersForModification()[2]->setRandomMode(Parameter::RandomMode_Constant);
    mockChunkPluginPreset2->getParametersForModification()[2]->setConstantValue(0.5);
    mockChunkPluginPreset2->getParametersForModification()[1]->setRandomMode(Parameter::RandomMode_Uniform);
    mockChunkPluginPreset2->getParametersForModification()[1]->setUniformMinValue(0.1);
    mockChunkPluginPreset2->getParametersForModification()[1]->setUniformMaxValue(0.9);
    mockChunkPluginPreset2->getParametersForModification()[0]->setRandomMode(Parameter::RandomMode_Normal);
    mockChunkPluginPreset2->getParametersForModification()[0]->setNormalMiddleValue(0.4);
    mockChunkPluginPreset2->getParametersForModification()[0]->setNormalStandardDeviation(0.2);
    this->vstListModel->addPreset(mockChunkPluginPreset2);
    QCOMPARE(this->vstListModel->getPluginPresets(mockChunkPlugin).size(), 2);
}

//-----------------------------------------------------------------------------
void VpgFileTest::cleanup()
{
    vstListModel->clearList();
}

//-----------------------------------------------------------------------------
void VpgFileTest::testWriteFile()
{
    VpgFile::ERROR_CODE errorCode = VpgFile::WriteModelToFile("testVpgFile.vpg", *(this->vstListModel.data()));
    QCOMPARE(errorCode, VpgFile::ERROR_NO_ERROR);

    QFile vpgFile("testVpgFile.vpg");
    QVERIFY(vpgFile.open(QIODevice::ReadOnly));
    QDomDocument doc;
    QVERIFY(doc.setContent(&vpgFile));
    vpgFile.close();

    QDomElement root = doc.documentElement();
    QCOMPARE(root.tagName(), QString("vpg_vst_list"));
    QCOMPARE(root.attribute("major_number").toUInt(), VpgVersion::getCurrentVersion().getMajorNumber());
    QCOMPARE(root.attribute("minor_number").toUInt(), VpgVersion::getCurrentVersion().getMinorNumber());
    QCOMPARE(root.attribute("revision_number").toUInt(), VpgVersion::getCurrentVersion().getRevisionNumber());

    QCOMPARE(root.childNodes().size(), 2);

    // First plugin.
    QDomNode vstNode1 = root.childNodes().at(0);
    QVERIFY(!vstNode1.isNull());
    QVERIFY(vstNode1.isElement());
    QDomElement vstElement1 = vstNode1.toElement();
    QCOMPARE(vstElement1.tagName(), QString("vst"));
    QCOMPARE(vstElement1.attributes().size(), 4);
    QCOMPARE(vstElement1.attribute("fx_version"), QString("1000"));
    QCOMPARE(vstElement1.attribute("fx_id").toInt(), CCONST('M','C','K','1'));
    QDir mockPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo plugInFileInfo(mockPluginDir, MOCKPLUGIN_FILENAME);
    QCOMPARE(vstElement1.attribute("fx_path"), plugInFileInfo.absoluteFilePath());
    QCOMPARE(vstElement1.attribute("name"), QString("VPG Mock Plugin"));

    // First plugin preset.
    QCOMPARE(vstElement1.childNodes().size(), 1);
    QDomElement presetElement = vstElement1.childNodes().at(0).toElement();
    QCOMPARE(presetElement.tagName(), QString("preset"));
    QCOMPARE(presetElement.attributes().size(), 1);
    QCOMPARE(presetElement.attribute("name"), QString("Mock Plugin Preset"));
    QCOMPARE(presetElement.childNodes().size(), 3);

    // First parameter.
    QDomElement parameterElement = presetElement.childNodes().at(0).toElement();
    QCOMPARE(parameterElement.attributes().size(), 2);
    QCOMPARE(parameterElement.attribute("mode"), QString("constant"));
    QCOMPARE(parameterElement.attribute("val"), QString("0.5"));

    // Second parameter.
    parameterElement = presetElement.childNodes().at(1).toElement();
    QCOMPARE(parameterElement.attributes().size(), 3);
    QCOMPARE(parameterElement.attribute("mode"), QString("uniform"));
    QCOMPARE(parameterElement.attribute("min"), QString("0.1"));
    QCOMPARE(parameterElement.attribute("max"), QString("0.9"));

    // Third parameter.
    parameterElement = presetElement.childNodes().at(2).toElement();
    QCOMPARE(parameterElement.attributes().size(), 3);
    QCOMPARE(parameterElement.attribute("mode"), QString("normal"));
    QCOMPARE(parameterElement.attribute("mid"), QString("0.4"));
    QCOMPARE(parameterElement.attribute("dev"), QString("0.2"));



    // Second plugin.
    QDomNode vstNode2 = root.childNodes().at(1);
    QVERIFY(!vstNode2.isNull());
    QVERIFY(vstNode2.isElement());
    QDomElement vstElement2 = vstNode2.toElement();
    QCOMPARE(vstElement2.tagName(), QString("vst"));
    QCOMPARE(vstElement2.attributes().size(), 4);
    QCOMPARE(vstElement2.attribute("fx_version"), QString("1000"));
    QCOMPARE(vstElement2.attribute("fx_id").toInt(), CCONST('M','C','K','2'));
    QDir mockChunkPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo chunkPlugInFileInfo(mockChunkPluginDir, MOCKCHUNKPLUGIN_FILENAME);
    QCOMPARE(vstElement2.attribute("fx_path"), chunkPlugInFileInfo.absoluteFilePath());
    QCOMPARE(vstElement2.attribute("name"), QString("VPG Mock Chunk Plugin"));

    // Second plugin presets.
    QCOMPARE(vstElement2.childNodes().size(), 2);

}

//-----------------------------------------------------------------------------
void VpgFileTest::testWriteFileErrorCodes()
{
    // ERROR_COULD_NOT_OPEN_FILE: Test with an invalid filename.
    VpgFile::ERROR_CODE errorCode = VpgFile::WriteModelToFile("", *(this->vstListModel.data()));
    QCOMPARE(errorCode, VpgFile::ERROR_COULD_NOT_OPEN_FILE);
}

//-----------------------------------------------------------------------------
void VpgFileTest::testReadLegacy026File()
{
    QFile vpgFile("validVPGFile");
    QVERIFY(vpgFile.open(QIODevice::WriteOnly));
    vpgFile.write("<vpg_vst_list major_number=\"0\" minor_number=\"2\" revision_number=\"6\">");
    vpgFile.write("<vst fx_path=\"\" fx_id=\"gyAm\" chunk=\"0\" nb_param=\"8\" name=\"GComp2\" fx_version=\"1100\"/>");
    vpgFile.write("<vst fx_path=\"\" fx_id=\"gyAh\" chunk=\"0\" nb_param=\"3\" name=\"GHi\" fx_version=\"100\"/>");
    vpgFile.write("<vst fx_path=\"\" fx_id=\"gyAd\" chunk=\"0\" nb_param=\"4\" name=\"GClip\" fx_version=\"10\"/>");
    vpgFile.write("<vst fx_path=\"path/to/plug1\" fx_id=\"Tf40\" chunk=\"1\" nb_param=\"112\" name=\"tunefish4\" fx_version=\"100\"/>");
    vpgFile.write("<vst fx_path=\"path/to/plug2\" fx_id=\"LGPd\" chunk=\"1\" nb_param=\"36\" name=\"Digits\" fx_version=\"0\"/>");
    vpgFile.write("</vpg_vst_list>");
    vpgFile.close();

    QList<VpgFile::Plugin> readPlugins;
    VpgFile::ERROR_CODE errorCode = VpgFile::ExtractPluginsFromFile(vpgFile.fileName(), readPlugins);
    QCOMPARE(errorCode, VpgFile::ERROR_NO_ERROR);

    QCOMPARE(readPlugins.size(), 5);

    VpgFile::Plugin& plugin = readPlugins[0];
    QCOMPARE(plugin.filePath, QString(""));
    QCOMPARE(EndiannessConverter::getMultiCharFromBigEndian(plugin.id), QString("gyAm"));
    QCOMPARE(plugin.name, QString("GComp2"));
    QCOMPARE(plugin.version, 1100);
    QVERIFY(plugin.presets.isEmpty());

    plugin = readPlugins[1];
    QCOMPARE(plugin.filePath, QString(""));
    QCOMPARE(EndiannessConverter::getMultiCharFromBigEndian(plugin.id), QString("gyAh"));
    QCOMPARE(plugin.name, QString("GHi"));
    QCOMPARE(plugin.version, 100);
    QVERIFY(plugin.presets.isEmpty());

    plugin = readPlugins[2];
    QCOMPARE(plugin.filePath, QString(""));
    QCOMPARE(EndiannessConverter::getMultiCharFromBigEndian(plugin.id), QString("gyAd"));
    QCOMPARE(plugin.name, QString("GClip"));
    QCOMPARE(plugin.version, 10);
    QVERIFY(plugin.presets.isEmpty());

    plugin = readPlugins[3];
    QCOMPARE(plugin.filePath, QString("path/to/plug1"));
    QCOMPARE(EndiannessConverter::getMultiCharFromBigEndian(plugin.id), QString("Tf40"));
    QCOMPARE(plugin.name, QString("tunefish4"));
    QCOMPARE(plugin.version, 100);
    QVERIFY(plugin.presets.isEmpty());

    plugin = readPlugins[4];
    QCOMPARE(plugin.filePath, QString("path/to/plug2"));
    QCOMPARE(EndiannessConverter::getMultiCharFromBigEndian(plugin.id), QString("LGPd"));
    QCOMPARE(plugin.name, QString("Digits"));
    QCOMPARE(plugin.version, 0);
    QVERIFY(plugin.presets.isEmpty());
}

//-----------------------------------------------------------------------------
void VpgFileTest::testReadFile()
{
    QFile vpgFile("validVPGFile");
    QVERIFY(vpgFile.open(QIODevice::WriteOnly));
    vpgFile.write("<vpg_vst_list major_number=\"0\" minor_number=\"3\" revision_number=\"0\">");
    vpgFile.write("<vst fx_version=\"100\" fx_id=\"1415984176\" fx_path=\"C:/path/to/the/funkyplugin.dll\" name=\"funky plugin\">");
    vpgFile.write("<preset name=\"Ultimate Preset\">");
    vpgFile.write("<param min=\"0.1\" max=\"0.8\" mode=\"uniform\"/>");
    vpgFile.write("<param val=\"0.2\" mode=\"constant\"/>");
    vpgFile.write("<param mid=\"0.3\" dev=\"0.01\" mode=\"normal\"/>");
    vpgFile.write("</preset>");
    vpgFile.write("<preset name=\"WIP Preset\">");
    vpgFile.write("<param val=\"0.1\" mode=\"constant\"/>");
    vpgFile.write("<param val=\"0.2\" mode=\"constant\"/>");
    vpgFile.write("<param val=\"0.3\" mode=\"constant\"/>");
    vpgFile.write("</preset>");
    vpgFile.write("</vst>");
    vpgFile.write("<vst fx_version=\"101\" fx_id=\"1415984180\" fx_path=\"C:/path/to/the/plugin with space.dll\" name=\"jazzy plugin\">");
    vpgFile.write("<preset name=\"Jazz Preset\">");
    vpgFile.write("<param min=\"0.1\" max=\"0.1\" mode=\"uniform\"/>");
    vpgFile.write("<param val=\"0.1\" mode=\"constant\"/>");
    vpgFile.write("<param mid=\"0.1\" dev=\"0.1\" mode=\"normal\"/>");
    vpgFile.write("</preset>");
    vpgFile.write("</vst>");
    vpgFile.write("</vpg_vst_list>");
    vpgFile.close();

    QList<VpgFile::Plugin> readPlugins;
    VpgFile::ERROR_CODE errorCode = VpgFile::ExtractPluginsFromFile(vpgFile.fileName(), readPlugins);
    QCOMPARE(errorCode, VpgFile::ERROR_NO_ERROR);

    QCOMPARE(readPlugins.size(), 2);

    // First plugin.
    VpgFile::Plugin& plugin = readPlugins[0];
    QCOMPARE(plugin.filePath, QString("C:/path/to/the/funkyplugin.dll"));
    QCOMPARE(plugin.id, 1415984176);
    QCOMPARE(plugin.name, QString("funky plugin"));
    QCOMPARE(plugin.version, 100);
    QCOMPARE(plugin.presets.size(), 2);

    // First preset.
    VpgFile::Preset& preset = plugin.presets[0];
    QCOMPARE(preset.name, QString("Ultimate Preset"));

    // Parameters.
    QCOMPARE(preset.parameters.size(), 3);
    QSharedPointer<Parameter>& parameter = preset.parameters[0];
    QCOMPARE(parameter->getRandomMode(), Parameter::RandomMode_Uniform);
    QVERIFY(areEqual(parameter->getUniformMinValue(), 0.1));
    QVERIFY(areEqual(parameter->getUniformMaxValue(), 0.8));
    parameter = preset.parameters[1];
    QCOMPARE(parameter->getRandomMode(), Parameter::RandomMode_Constant);
    QVERIFY(areEqual(parameter->getConstantValue(), 0.2));
    parameter = preset.parameters[2];
    QCOMPARE(parameter->getRandomMode(), Parameter::RandomMode_Normal);
    QVERIFY(areEqual(parameter->getNormalMiddleValue(), 0.3));
    QVERIFY(areEqual(parameter->getNormalStandardDeviation(), 0.01));

    // Second Preset.
    preset = plugin.presets[1];
    QCOMPARE(preset.name, QString("WIP Preset"));

    // Parameters.
    QCOMPARE(preset.parameters.size(), 3);
    parameter = preset.parameters[0];
    QCOMPARE(parameter->getRandomMode(), Parameter::RandomMode_Constant);
    QVERIFY(areEqual(parameter->getConstantValue(), 0.1));
    parameter = preset.parameters[1];
    QCOMPARE(parameter->getRandomMode(), Parameter::RandomMode_Constant);
    QVERIFY(areEqual(parameter->getConstantValue(), 0.2));
    parameter = preset.parameters[2];
    QCOMPARE(parameter->getRandomMode(), Parameter::RandomMode_Constant);
    QVERIFY(areEqual(parameter->getConstantValue(), 0.3));

    // Second Plugin.
    plugin = readPlugins[1];
    QCOMPARE(plugin.filePath, QString("C:/path/to/the/plugin with space.dll"));
    QCOMPARE(plugin.id, 1415984180);
    QCOMPARE(plugin.name, QString("jazzy plugin"));
    QCOMPARE(plugin.version, 101);
    QCOMPARE(plugin.presets.size(), 1);
}

//-----------------------------------------------------------------------------
void VpgFileTest::testWriteThenReadFile()
{
    VpgFile::ERROR_CODE errorCode = VpgFile::WriteModelToFile("testReadWriteVpgFile.vpg", *(this->vstListModel.data()));
    QCOMPARE(errorCode, VpgFile::ERROR_NO_ERROR);

    QList<VpgFile::Plugin> readPlugins;
    errorCode = VpgFile::ExtractPluginsFromFile("testReadWriteVpgFile.vpg", readPlugins);
    QCOMPARE(errorCode, VpgFile::ERROR_NO_ERROR);

    QCOMPARE(readPlugins.size(), this->vstListModel->getPlugins().size());
    QCOMPARE(readPlugins.size(), 2);

    for(int i=0; i<2; i++)
    {
        QCOMPARE(readPlugins[i].id, this->vstListModel->getPluginAtIndex(i)->getFxId());
        QCOMPARE(readPlugins[i].filePath, this->vstListModel->getPluginAtIndex(i)->getFilePath());
        QCOMPARE(readPlugins[i].version, this->vstListModel->getPluginAtIndex(i)->getFxVersion());
        QCOMPARE(readPlugins[i].name, this->vstListModel->getPluginAtIndex(i)->getName());
    }
}

//-----------------------------------------------------------------------------
void VpgFileTest::testReadFileErrorCodes()
{
    QList<VpgFile::Plugin> unusedReadPlugins;

    // ERROR_COULD_NOT_OPEN_FILE
    VpgFile::ERROR_CODE errorCode = VpgFile::ExtractPluginsFromFile("total_invalid_file", unusedReadPlugins);
    QCOMPARE(errorCode, VpgFile::ERROR_COULD_NOT_OPEN_FILE);

    // ERROR_COULD_NOT_PARSE_FILE
    QFile invalidFile("nonXMLTestFile");
    QVERIFY(invalidFile.open(QIODevice::WriteOnly));
    invalidFile.write("this is not a vpg file!");
    invalidFile.close();
    errorCode = VpgFile::ExtractPluginsFromFile(invalidFile.fileName(), unusedReadPlugins);
    QCOMPARE(errorCode, VpgFile::ERROR_COULD_NOT_PARSE_FILE);

    // ERROR_INVALID_VPG_FILE
    QFile invalidVPGFile("nonVPGTestFile");
    QVERIFY(invalidVPGFile.open(QIODevice::WriteOnly));
    invalidVPGFile.write("<root><item/><item/></root>");
    invalidVPGFile.close();
    errorCode = VpgFile::ExtractPluginsFromFile(invalidVPGFile.fileName(), unusedReadPlugins);
    QCOMPARE(errorCode, VpgFile::ERROR_INVALID_VPG_FILE);

    // ERROR_UNKNOWN_XML_TAG
    QFile invalidVstFile("nonVstTestFile");
    QVERIFY(invalidVstFile.open(QIODevice::WriteOnly));
    invalidVstFile.write("<vpg_vst_list>");
    invalidVstFile.write("  <vst fx_path=\"\" fx_id=\"gyAm\" chunk=\"0\" nb_param=\"8\" name=\"GComp2\" fx_version=\"1100\"/>");
    invalidVstFile.write("  <invalid_tag/>");
    invalidVstFile.write("</vpg_vst_list>");
    invalidVstFile.close();
    errorCode = VpgFile::ExtractPluginsFromFile(invalidVstFile.fileName(), unusedReadPlugins);
    QCOMPARE(errorCode, VpgFile::ERROR_UNKNOWN_XML_TAG);

    // ERROR_INVALID_XML_ELEMENT
    QFile invalidXMLElementFile("invalidXMlElementTestFile");
    QVERIFY(invalidXMLElementFile.open(QIODevice::WriteOnly));
    invalidXMLElementFile.write("<vpg_vst_list>");
    invalidXMLElementFile.write("  <!-- This is a comment -->");
    invalidXMLElementFile.write("</vpg_vst_list>");
    invalidXMLElementFile.close();
    errorCode = VpgFile::ExtractPluginsFromFile(invalidXMLElementFile.fileName(), unusedReadPlugins);
    QCOMPARE(errorCode, VpgFile::ERROR_INVALID_XML_ELEMENT);
}

QTEST_APPLESS_MAIN(VpgFileTest)

#include "vpgfile_test.moc"
