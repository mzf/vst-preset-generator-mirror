
include(../vpgtests_commons.pri)

QT -= gui
QT += widgets \
      xml

TARGET = vpgfile_test


HEADERS += ../../vpgfile.h \
           ../../vpgversion.h \
           ../../vstlistmodel.h \
           ../../vsthost.h \
           ../../vstplugin.h \
           ../../parameter.h \
           ../../vstlistitem.h \
           ../../vstpreset.h \
           ../../vpgrandom.h \
           ../../endiannessconverter.h

SOURCES += vpgfile_test.cpp \
           ../../vpgfile.cpp \
           ../../vpgversion.cpp \
           ../../vstlistmodel.cpp \
           ../../vsthost.cpp \
           ../../vstplugin.cpp \
           ../../parameter.cpp \
           ../../vstlistitem.cpp \
           ../../vstpreset.cpp \
           ../../vpgrandom.cpp \
           ../../endiannessconverter.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

