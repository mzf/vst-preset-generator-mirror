/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgversion_test.cpp
  Author: Francois Mazen
  Date: 26/07/16 18:58
  Description: a test class for the VpgVersion class
-------------------------------------------------------------------------------
*/

#include "../../vpgversion.h"

#include <QtTest>

class VpgVersion;

class VpgVersionTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void testGetMajorNumber();
    void testGetMinorNumber();
    void testGetRevisionNumber();
    void testGetPlatform();
    void testGetStringVersion();
    void testGetStringPlatform();
    void testGetStringVersionWithPlatform();
    void testLessOperator();
};

void VpgVersionTest::testGetMajorNumber()
{
    QCOMPARE(VpgVersion::getCurrentVersion().getMajorNumber(), (unsigned int)VPG_MAJOR_NUMBER);
}

void VpgVersionTest::testGetMinorNumber()
{
    QCOMPARE(VpgVersion::getCurrentVersion().getMinorNumber(), (unsigned int)VPG_MINOR_NUMBER);
}

void VpgVersionTest::testGetRevisionNumber()
{
    QCOMPARE(VpgVersion::getCurrentVersion().getRevisionNumber(), (unsigned int)VPG_REVISION_NUMBER);
}

void VpgVersionTest::testGetPlatform()
{
    VpgVersion::Platform currentPlatform = VpgVersion::PLATFORM_32_BITS;
    if(QSysInfo::buildCpuArchitecture() == "x86_64")
    {
        currentPlatform = VpgVersion::PLATFORM_64_BITS;
    }
    QCOMPARE(VpgVersion::getCurrentVersion().getPlatform(), currentPlatform);
}

void VpgVersionTest::testGetStringVersion()
{
    QString major;
    major.setNum(VPG_MAJOR_NUMBER);
    QString minor;
    minor.setNum(VPG_MINOR_NUMBER);
    QString revision;
    revision.setNum(VPG_REVISION_NUMBER);

    QString reference = major + "." + minor + "." + revision;

    QCOMPARE(VpgVersion::getCurrentVersion().getStringVersion(), reference);
}

void VpgVersionTest::testGetStringPlatform()
{
    QString currentPlatform = "32 bits";
    if(QSysInfo::buildCpuArchitecture() == "x86_64")
    {
        currentPlatform = "64 bits";
    }

    QCOMPARE(VpgVersion::getCurrentVersion().getStringPlatform(), currentPlatform);
}

void VpgVersionTest::testGetStringVersionWithPlatform()
{
    QString major;
    major.setNum(VPG_MAJOR_NUMBER);
    QString minor;
    minor.setNum(VPG_MINOR_NUMBER);
    QString revision;
    revision.setNum(VPG_REVISION_NUMBER);

    QString currentPlatform = "32 bits";
    if(QSysInfo::buildCpuArchitecture() == "x86_64")
    {
        currentPlatform = "64 bits";
    }

    QString reference = major + "." + minor + "." + revision + " - " + currentPlatform;

    QCOMPARE(VpgVersion::getCurrentVersion().getStringVersionWithPlatform(), reference);
}

void VpgVersionTest::testLessOperator()
{
    VpgVersion version123(1,2,3);

    // Equality.
    QCOMPARE(version123 < version123, false);

    // Major number
    VpgVersion version023(0,2,3);
    QCOMPARE(version023 < version123, true);
    QCOMPARE(version123 < version023, false);
    VpgVersion version223(2,2,3);
    QCOMPARE(version223 < version123, false);
    QCOMPARE(version123 < version223, true);

    // Minor number
    VpgVersion version113(1,1,3);
    QCOMPARE(version113 < version123, true);
    QCOMPARE(version123 < version113, false);
    VpgVersion version153(1,5,3);
    QCOMPARE(version153 < version123, false);
    QCOMPARE(version123 < version153, true);

    // Revision number
    VpgVersion version121(1,2,1);
    QCOMPARE(version121 < version123, true);
    QCOMPARE(version123 < version121, false);
    VpgVersion version124(1,2,4);
    QCOMPARE(version124 < version123, false);
    QCOMPARE(version123 < version124, true);
}

QTEST_APPLESS_MAIN(VpgVersionTest)

#include "vpgversion_test.moc"
