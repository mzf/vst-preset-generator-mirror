include(../vpgtests_commons.pri)

QT       -= gui

TARGET = vpgversion_test

HEADERS += ../../vpgversion.h

SOURCES += vpgversion_test.cpp \
           ../../vpgversion.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
