/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstpreset_test.cpp
  Author: Francois Mazen
  Date: 19/07/16 20:12
  Description: a test class for the VSTPreset class
-------------------------------------------------------------------------------
*/

#include "../../vstpreset.h"
#include "../../vstplugin.h"
#include "../../vsthost.h"
#include "../../endiannessconverter.h"
#include "../../vstsdk.h"
#include "../../vpgrandom.h"

#include <QtTest>

#ifdef WIN64
    #define MOCKPLUGIN_FILENAME "mockplugin_win64.dll"
    #define MOCKCHUNKPLUGIN_FILENAME "mockchunkplugin_win64.dll"
#elif WIN32
    #define MOCKPLUGIN_FILENAME "mockplugin_win32.dll"
    #define MOCKCHUNKPLUGIN_FILENAME "mockchunkplugin_win32.dll"
#elif __x86_64
    #define MOCKPLUGIN_FILENAME "libmockplugin_linux.so"
    #define MOCKCHUNKPLUGIN_FILENAME "libmockchunkplugin_linux.so"
#endif

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

#define PRESET_NAME "this is the preset name"

class VSTPresetTest : public QObject
{
    Q_OBJECT

public:
    VSTPresetTest();

private:
    QFile readOnlyFile;
    QFileInfo testPresetFile;
    QSharedPointer<VpgRandom> vpgRandom;
    QSharedPointer<VSTHost> vstHost;
    bool isSlotCalled;

    void removeWrittenFiles();
    void resetIsSlotCalled();
    void loadVSTPluginAndVSTPreset(const QString& pluginFilePath, QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<VSTPreset> &vstPreset);
    void loadVSTPluginAndVSTPresetForMockPlugin(QSharedPointer<VSTPlugin>& vstPlugin, QSharedPointer<VSTPreset>& vstPreset);
    void loadVSTPluginAndVSTPresetForMockChunkPlugin(QSharedPointer<VSTPlugin>& vstPlugin, QSharedPointer<VSTPreset>& vstPreset);
    void checkRawProgramFile(QFile &presetFile, const QSharedPointer<VSTPlugin>& vstPlugin);
    void checkRawChunkProgramFile(QFile &presetFile, const QSharedPointer<VSTPlugin>& vstPlugin);

public slots:
    void slotCalled();

private Q_SLOTS:

    void initTestCase();
    void cleanupTestCase();

    void cleanup();

    void testInvalidFilename();

    void testWriteProgramToFile();
    void testWriteBankToFile();

    void testWriteChunkProgramToFile();

    void testWriteChunkBankToFile();

    void testAccessors();
};


VSTPresetTest::VSTPresetTest():
    testPresetFile("program_test_preset.fxp"),
    isSlotCalled(false)
{
    this->testPresetFile.setCaching(false);
    vpgRandom.reset(new VpgRandom(1234567));
    vstHost.reset(new VSTHost());
}

void VSTPresetTest::initTestCase()
{
    this->readOnlyFile.close();

    //Create a read only file.
    QString readOnlyFilename("readonlyfile");
    this->readOnlyFile.setFileName(readOnlyFilename);
    QVERIFY(this->readOnlyFile.open(QIODevice::WriteOnly));
    this->readOnlyFile.write("test");
    this->readOnlyFile.close();
    QVERIFY(this->readOnlyFile.setPermissions(QFileDevice::ReadOwner));

    removeWrittenFiles();
}

void VSTPresetTest::removeWrittenFiles()
{
    // Check that the files that will be generated don't exist.
    if(this->testPresetFile.exists())
    {
        QVERIFY(QFile::remove(this->testPresetFile.absoluteFilePath()));
    }
}

void VSTPresetTest::slotCalled()
{
    isSlotCalled = true;
}

void VSTPresetTest::resetIsSlotCalled()
{
    isSlotCalled = false;
}

void VSTPresetTest::cleanupTestCase()
{
    // Remove read only file.
    QVERIFY(this->readOnlyFile.setPermissions(QFileDevice::WriteOwner));
    QVERIFY(this->readOnlyFile.remove());

    removeWrittenFiles();
}

void VSTPresetTest::cleanup()
{
    removeWrittenFiles();
}

void VSTPresetTest::loadVSTPluginAndVSTPreset(const QString& pluginFilePath, QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<VSTPreset> &vstPreset)
{
    VSTHost::ERROR_CODE loadingErrorCode = vstHost->getOrLoadPlugin(pluginFilePath, vstPlugin);
    QCOMPARE(loadingErrorCode, VSTHost::ERROR_NO_ERROR);
    QCOMPARE(vstPlugin.isNull(), false);

    vstPreset.reset(new VSTPreset(vstPlugin, PRESET_NAME));
    QCOMPARE(vstPreset.isNull(), false);
    QVector<QSharedPointer<Parameter> > & parameters = vstPreset->getParametersForModification();
    QCOMPARE(parameters.size(), 3);
    parameters[0]->setRandomMode(Parameter::RandomMode_Constant);
    parameters[0]->setConstantValue(1.0);
    parameters[1]->setRandomMode(Parameter::RandomMode_Constant);
    parameters[1]->setConstantValue(0.5);
    parameters[2]->setRandomMode(Parameter::RandomMode_Constant);
    parameters[2]->setConstantValue(0.0);

    connect(vstPreset.data(), SIGNAL(dataChanged()), this, SLOT(slotCalled()));
    QCOMPARE(isSlotCalled, false);
}

void VSTPresetTest::loadVSTPluginAndVSTPresetForMockChunkPlugin(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<VSTPreset> &vstPreset)
{
    QDir mockPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo plugInFileInfo(mockPluginDir, MOCKCHUNKPLUGIN_FILENAME);
    QVERIFY(plugInFileInfo.exists());

    loadVSTPluginAndVSTPreset(plugInFileInfo.absoluteFilePath(), vstPlugin, vstPreset);
}

void VSTPresetTest::loadVSTPluginAndVSTPresetForMockPlugin(QSharedPointer<VSTPlugin> &vstPlugin, QSharedPointer<VSTPreset> &vstPreset)
{
    QDir mockPluginDir(STRINGIZE_VALUE_OF(MOCKPLUGINS_FOLDER));
    QFileInfo plugInFileInfo(mockPluginDir, MOCKPLUGIN_FILENAME);
    QVERIFY(plugInFileInfo.exists());

    loadVSTPluginAndVSTPreset(plugInFileInfo.absoluteFilePath(), vstPlugin, vstPreset);
}

void VSTPresetTest::checkRawProgramFile(QFile &presetFile, const QSharedPointer<VSTPlugin> &vstPlugin)
{
    int32_t chunkMagic = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(chunkMagic, CCONST('C','c','n','K'));

    int32_t byteSize = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(byteSize, 60);

    int32_t fxMagic = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxMagic, CCONST('F','x','C','k'));

    int32_t version = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(version, 1);

    int32_t fxID = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxID, CCONST('M','C','K','1'));

    int32_t fxVersion = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxVersion, vstPlugin->getFxVersion());

    int32_t numParams = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(numParams, vstPlugin->getNumberParams());

    QString prgName(presetFile.read(28).data());
    QVERIFY(prgName.startsWith("VPG"));

    float param1IntValue = EndiannessConverter::floatFromBigEndian(presetFile.read(4));
    QCOMPARE(1.0f, param1IntValue);

    float param2IntValue = EndiannessConverter::floatFromBigEndian(presetFile.read(4));
    QCOMPARE(0.5f, param2IntValue);

    float param3IntValue = EndiannessConverter::floatFromBigEndian(presetFile.read(4));
    QCOMPARE(0.0f, param3IntValue);
}

void VSTPresetTest::checkRawChunkProgramFile(QFile &presetFile, const QSharedPointer<VSTPlugin> &vstPlugin)
{
    QString referenceChunk = "PROGRAM;1;0.5;0";

    int32_t chunkMagic = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(chunkMagic, CCONST('C','c','n','K'));

    int32_t referenceSize = 4*5 + 28 + 4 + referenceChunk.size();
    int32_t byteSize = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(byteSize, referenceSize);

    int32_t fxMagic = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxMagic, CCONST('F','P','C','h'));

    int32_t version = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(version, 1);

    int32_t fxID = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxID, CCONST('M','C','K','2'));

    int32_t fxVersion = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxVersion, vstPlugin->getFxVersion());

    int32_t numParams = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(numParams, vstPlugin->getNumberParams());

    QString prgName(presetFile.read(28).data());
    QVERIFY(prgName.startsWith("VPG"));

    int32_t chunkSize = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(chunkSize, referenceChunk.size());

    QByteArray chunk = presetFile.read(referenceChunk.size());
    QCOMPARE(chunk.size(), referenceChunk.size());
    QCOMPARE(QString(chunk), referenceChunk);
}

void VSTPresetTest::testInvalidFilename()
{
    QFileInfo fileInfo(this->readOnlyFile);

    QSharedPointer<VSTPlugin> vstPlugin;
    QSharedPointer<VSTPreset> vstPreset;
    loadVSTPluginAndVSTPresetForMockPlugin(vstPlugin, vstPreset);

    QCOMPARE(vstPreset->writeProgramToFile(fileInfo.absoluteFilePath(), vpgRandom), VSTPreset::ERROR_COULD_NOT_CREATE_FILE);
    QCOMPARE(vstPreset->writeProgramChunkToFile(fileInfo.absoluteFilePath(), vpgRandom), VSTPreset::ERROR_COULD_NOT_CREATE_FILE);
    QCOMPARE(vstPreset->writeBankToFile(fileInfo.absoluteFilePath(), 10, vpgRandom), VSTPreset::ERROR_COULD_NOT_CREATE_FILE);
    QCOMPARE(vstPreset->writeBankChunkToFile(fileInfo.absoluteFilePath(), 10, vpgRandom), VSTPreset::ERROR_COULD_NOT_CREATE_FILE);
}

void VSTPresetTest::testWriteProgramToFile()
{
    QSharedPointer<VSTPlugin> vstPlugin;
    QSharedPointer<VSTPreset> vstPreset;
    loadVSTPluginAndVSTPresetForMockPlugin(vstPlugin, vstPreset);

    // Write the preset file.
    QCOMPARE(this->testPresetFile.exists(), false);

    VSTPreset::ERROR_CODE errorCode = vstPreset->writeProgramToFile(this->testPresetFile.absoluteFilePath(), vpgRandom);

    QCOMPARE(errorCode, VSTPreset::ERROR_NO_ERROR);
    QCOMPARE(this->testPresetFile.exists(), true);

    // Check preset file content.
    QFile presetFile(this->testPresetFile.absoluteFilePath());
    QVERIFY(presetFile.open(QIODevice::ReadOnly));

    checkRawProgramFile(presetFile, vstPlugin);

    presetFile.close();
}

void VSTPresetTest::testWriteBankToFile()
{
    QSharedPointer<VSTPlugin> vstPlugin;
    QSharedPointer<VSTPreset> vstPreset;
    loadVSTPluginAndVSTPresetForMockPlugin(vstPlugin, vstPreset);

    // Write the preset file.
    QCOMPARE(this->testPresetFile.exists(), false);

    int programCountPerBank = 5;
    VSTPreset::ERROR_CODE errorCode = vstPreset->writeBankToFile(this->testPresetFile.absoluteFilePath(), programCountPerBank, vpgRandom);

    QCOMPARE(errorCode, VSTPreset::ERROR_NO_ERROR);
    QCOMPARE(this->testPresetFile.exists(), true);

    // Check preset file content.
    QFile presetFile(this->testPresetFile.absoluteFilePath());
    QVERIFY(presetFile.open(QIODevice::ReadOnly));

    int32_t chunkMagic = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(chunkMagic, CCONST('C','c','n','K'));

    int32_t byteSize = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(byteSize, 5*4+128+68*programCountPerBank); // 448

    int32_t fxMagic = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxMagic, CCONST('F','x','B','k'));

    int32_t version = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(version, 1);

    int32_t fxID = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxID, CCONST('M','C','K','1'));

    int32_t fxVersion = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxVersion, vstPlugin->getFxVersion());

    int32_t numPrograms = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(numPrograms, programCountPerBank);

    int32_t currentProgram = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(currentProgram, 1);

    // Ignore next 124 bytes (reserved for future use).
    presetFile.read(124);

    // Check each program.
    for(int programIndex = 0; programIndex < programCountPerBank; programIndex++)
    {
        checkRawProgramFile(presetFile, vstPlugin);
    }

    presetFile.close();
}

void VSTPresetTest::testWriteChunkProgramToFile()
{
    QSharedPointer<VSTPlugin> vstPlugin;
    QSharedPointer<VSTPreset> vstPreset;
    loadVSTPluginAndVSTPresetForMockChunkPlugin(vstPlugin, vstPreset);

    // Write the preset file.
    QCOMPARE(this->testPresetFile.exists(), false);

    VSTPreset::ERROR_CODE errorCode = vstPreset->writeProgramChunkToFile(this->testPresetFile.absoluteFilePath(), vpgRandom);

    QCOMPARE(errorCode, VSTPreset::ERROR_NO_ERROR);
    QCOMPARE(this->testPresetFile.exists(), true);

    // Check preset file content.
    QFile presetFile(this->testPresetFile.absoluteFilePath());
    QVERIFY(presetFile.open(QIODevice::ReadOnly));

    checkRawChunkProgramFile(presetFile, vstPlugin);

    presetFile.close();
}

void VSTPresetTest::testWriteChunkBankToFile()
{
    QSharedPointer<VSTPlugin> vstPlugin;
    QSharedPointer<VSTPreset> vstPreset;
    loadVSTPluginAndVSTPresetForMockChunkPlugin(vstPlugin, vstPreset);

    // Write the preset file.
    QCOMPARE(this->testPresetFile.exists(), false);

    int programCountPerBank = 5;
    VSTPreset::ERROR_CODE errorCode = vstPreset->writeBankChunkToFile(this->testPresetFile.absoluteFilePath(), programCountPerBank, vpgRandom);

    QCOMPARE(errorCode, VSTPreset::ERROR_NO_ERROR);
    QCOMPARE(this->testPresetFile.exists(), true);

    // Check preset file content.
    QFile presetFile(this->testPresetFile.absoluteFilePath());
    QVERIFY(presetFile.open(QIODevice::ReadOnly));

    int32_t chunkMagic = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(chunkMagic, CCONST('C','c','n','K'));

    QString referenceChunk = "BANK;1;0.5;0";
    int32_t byteSize = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(byteSize, 5*4 + 128 + 4 + referenceChunk.size());

    int32_t fxMagic = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxMagic, CCONST('F','B','C','h'));

    int32_t version = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(version, 1);

    int32_t fxID = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxID, CCONST('M','C','K','2'));

    int32_t fxVersion = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(fxVersion, vstPlugin->getFxVersion());

    int32_t numPrograms = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(numPrograms, std::min(programCountPerBank, vstPlugin->getNumberPrograms()));

    int32_t currentProgram = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(currentProgram, 1);

    // Ignore next 124 bytes (reserved for future use).
    presetFile.read(124);

    // Check program chunk.
    int32_t chunkSize = EndiannessConverter::fromBigEndian(presetFile.read(4));
    QCOMPARE(chunkSize, referenceChunk.size());

    QByteArray chunk = presetFile.read(referenceChunk.size());
    QCOMPARE(chunk.size(), referenceChunk.size());
    QCOMPARE(QString(chunk), referenceChunk);

    presetFile.close();

}

void VSTPresetTest::testAccessors()
{
    QSharedPointer<VSTPlugin> vstPlugin;
    QSharedPointer<VSTPreset> vstPreset;
    loadVSTPluginAndVSTPresetForMockChunkPlugin(vstPlugin, vstPreset);

    resetIsSlotCalled();
    QCOMPARE(vstPreset->getName().toLatin1().data(), PRESET_NAME);
    QCOMPARE(vstPreset->getParameters().size(), 3);
    QCOMPARE(vstPreset->getParametersForModification().size(), 3);
    QCOMPARE(vstPreset->getVSTPlugin().data(), vstPlugin.data());
    QCOMPARE(isSlotCalled, false);

    vstPreset->setName("new name");
    QCOMPARE(isSlotCalled, true);
    QCOMPARE(vstPreset->getName().toLatin1().data(), "new name");

    resetIsSlotCalled();
    vstPreset->getParametersForModification()[0]->setConstantValue(0.5);
    QCOMPARE(isSlotCalled, true);
}

QTEST_GUILESS_MAIN(VSTPresetTest)

#include "vstpreset_test.moc"
