include(../vpgtests_commons.pri)

QT       -= gui
QT += widgets

TARGET = vstpreset_test

HEADERS += ../../vstpreset.h \
           ../../vsthost.h \
           ../../endiannessconverter.h \
           ../../parameter.h \
           ../../vpgrandom.h \
           ../../vstplugin.h

SOURCES += vstpreset_test.cpp \
           ../../vstpreset.cpp \
           ../../vsthost.cpp \
           ../../endiannessconverter.cpp \
           ../../parameter.cpp \
           ../../vpgrandom.cpp \
           ../../vstplugin.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
