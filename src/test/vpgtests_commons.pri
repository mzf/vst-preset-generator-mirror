
QT       += testlib
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += testcase

TEMPLATE = app

# Disable symbol __cdecl for linux platform because of issue in VST SDK.
linux*:QMAKE_CXXFLAGS += -D__cdecl=\"\"

# Get version from the vpg.pro file.
VPG_MAJOR_NUMBER = $$fromfile("../vpg.pro", VPG_MAJOR_NUMBER)
DEFINES+= VPG_MAJOR_NUMBER=$$VPG_MAJOR_NUMBER

VPG_MINOR_NUMBER = $$fromfile("../vpg.pro", VPG_MINOR_NUMBER)
DEFINES+= VPG_MINOR_NUMBER=$$VPG_MINOR_NUMBER

VPG_REVISION_NUMBER = $$fromfile("../vpg.pro", VPG_REVISION_NUMBER)
DEFINES+= VPG_REVISION_NUMBER=$$VPG_REVISION_NUMBER

# Get Mock Plugins binaries folder.
MOCKPLUGINSFOLDER = $$PWD/mockplugin/bin
message($$MOCKPLUGINSFOLDER)
DEFINES += MOCKPLUGINS_FOLDER=\"$$MOCKPLUGINSFOLDER\"

# VST SDK
exists(../vst2.x/aeffectx.h) {
    message("using VST SDK")
} else {
    message("VST SDK not found, using VeSTige header as substitute")
    DEFINES += USE_VESTIGE
}

