include(../vpgtests_commons.pri)

QT -= gui

TARGET = vpgrandom_test


HEADERS += ../../vpgrandom.h

SOURCES += vpgrandom_test.cpp \
           ../../vpgrandom.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
