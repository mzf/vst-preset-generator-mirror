/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgrandom_test.cpp
  Author: Francois Mazen
  Date: 14/11/13 20:10
  Description: a test class for the VpgRandom class
-------------------------------------------------------------------------------
*/

#include "../../vpgrandom.h"

#include <QtTest>

class VpgRandomTest : public QObject
{
    Q_OBJECT

public:
    VpgRandomTest();

private Q_SLOTS:

    void init();
    void cleanup();

    void testUniformGenerationBounds();
    void testUniformGenerationMean();
    void testUniformGenerationUniformity();

    void testNormalGenerationMean();
    void testNormalGenerationShape();

private:
    VpgRandom* vpgRandom;
};

VpgRandomTest::VpgRandomTest():
    vpgRandom(NULL)
{
}

void VpgRandomTest::init()
{
    vpgRandom = new VpgRandom(255);
}

void VpgRandomTest::cleanup()
{
    delete vpgRandom;
    vpgRandom = NULL;
}

void VpgRandomTest::testUniformGenerationBounds()
{
    for(int index = 0; index < 100; index++)
    {
        float value = vpgRandom->getUniformValue(10.0, 20.0);
        QVERIFY2(value >= 10.0, "The random value must be greater or equal than 10.0");
        QVERIFY2(value <= 20.0, "The random value must be less or equal than 20.0");
    }

    for(int index = 0; index < 100; index++)
    {
        float value = vpgRandom->getUniformValue(-20.0, -10.0);
        QVERIFY2(value >= -20.0, "The random value must be greater or equal than -20.0");
        QVERIFY2(value <= -10.0, "The random value must be less or equal than -10.0");
    }

    float fixedValue = vpgRandom->getUniformValue(3.0, 3.0);
    QVERIFY2(fabs(fixedValue - 3.0) < 1e-6, "The random value must be 3.0");

    for(int index = 0; index < 100; index++)
    {
        float value = vpgRandom->getUniformValue(0.0, -10.0);
        QVERIFY2(value >= -10.0, "Inverted input: the random value must be greater or equal than -10.0");
        QVERIFY2(value <= 0.0, "Inverted input: the random value must be less or equal than 0.0");
    }

    for(int index = 0; index < 100; index++)
    {
        float value = vpgRandom->getUniformValue(2.0, 0.0);
        QVERIFY2(value >= 0.0, "Inverted input: the random value must be greater or equal than 0.0");
        QVERIFY2(value <= 2.0, "Inverted input: the random value must be less or equal than 2.0");
    }
}

void VpgRandomTest::testUniformGenerationMean()
{
    // Check that mean of the generator between 10.0 and 20.0 is 15.0

    unsigned int maxIteration = 10000;
    float total = 0.0;
    for(unsigned int index = 0; index < maxIteration; index++)
    {
        float value = vpgRandom->getUniformValue(10.0, 20.0);
        total += value;
    }

    float mean = total / float(maxIteration);
    QVERIFY2(fabs(mean - 15.0f) < 0.1, "The mean of uniform generator must be 15.0" );
}

void VpgRandomTest::testUniformGenerationUniformity()
{
    //Count number of value in each slice between 0 and 9 and check that probability are equals to 0.1.

    unsigned int valuePerSlice[10];
    for(int index = 0; index < 10; index++)
    {
        valuePerSlice[index] = 0;
    }

    unsigned int maxIteration = 100000;
    for(unsigned int index = 0; index < maxIteration; index++)
    {
        float value = vpgRandom->getUniformValue(0.0, 10.0);
        unsigned int sliceIndex = floor(value);
        if(sliceIndex > 9)
        {
            sliceIndex = 9;
        }

        valuePerSlice[sliceIndex]++;
    }

    for(int index = 0; index < 10; index++)
    {
        float result = float(valuePerSlice[index]) / float(maxIteration);
        QVERIFY2(fabs(result - 0.1) < 1e-2, "Probability not equal to 0.1");
    }
}

void VpgRandomTest::testNormalGenerationMean()
{
    // Check that the mean of the generator is right.

    unsigned int maxIteration = 10000;
    float total = 0.0f;
    for(unsigned int index = 0; index < maxIteration; index++)
    {
        float value = vpgRandom->getNormalValue(0.2, 1);
        total += value;
    }

    float mean = total / float(maxIteration);
    QVERIFY2(fabs(mean - 0.2f) < 0.01, "The mean of normal generation must be 0.2");
}

void VpgRandomTest::testNormalGenerationShape()
{
    // Check that the shape is a bell curve.

    unsigned int maxIteration = 10000;
    unsigned int valuePerSlice[10];
    for(int index = 0; index < 10; index++)
    {
        valuePerSlice[index] = 0;
    }

    for(unsigned int index = 0; index < maxIteration; index++)
    {
        float value = vpgRandom->getNormalValue(0.5, 0.2);
        unsigned int sliceIndex = floor(value * 10);
        if(sliceIndex <= 9)
        {
            valuePerSlice[sliceIndex]++;
        }
    }

    QVERIFY2(valuePerSlice[0] < valuePerSlice[1], "value per slice 0 must be less than slice 1");
    QVERIFY2(valuePerSlice[1] < valuePerSlice[2], "value per slice 1 must be less than slice 2");
    QVERIFY2(valuePerSlice[2] < valuePerSlice[3], "value per slice 2 must be less than slice 3");
    QVERIFY2(valuePerSlice[3] < valuePerSlice[4], "value per slice 3 must be less than slice 4");

    QVERIFY2(valuePerSlice[5] > valuePerSlice[6], "value per slice 5 must be greater than slice 6");
    QVERIFY2(valuePerSlice[6] > valuePerSlice[7], "value per slice 6 must be greater than slice 7");
    QVERIFY2(valuePerSlice[7] > valuePerSlice[8], "value per slice 7 must be greater than slice 8");
    QVERIFY2(valuePerSlice[8] > valuePerSlice[9], "value per slice 8 must be greater than slice 9");
}

QTEST_APPLESS_MAIN(VpgRandomTest)

#include "vpgrandom_test.moc"
