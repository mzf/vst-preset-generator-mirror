/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: mockplugin.cpp
  Author: Francois Mazen
  Date: 20/03/16 12:07
  Description: mockplugin to test vpg. Based on the Again sample from the SDK.
-------------------------------------------------------------------------------
*/

#include "mockplugin.h"

#include <sstream>
#include <vector>

#ifdef MOCK_USE_CHUNK
    #define MOCK_PLUGIN_NAME "VPG Mock Chunk Plugin"
    #define MOCK_PLUGIN_ID CCONST('M','C','K','2')
    #define MOCK_PROGRAMS_ARE_CHUNK true
#else
    #define MOCK_PLUGIN_NAME "VPG Mock Plugin"
    #define MOCK_PLUGIN_ID CCONST('M','C','K','1')
    #define MOCK_PROGRAMS_ARE_CHUNK false
#endif

//-------------------------------------------------------------------------------------------------------
AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
{
    return new MockPlugin (audioMaster);
}

//-------------------------------------------------------------------------------------------------------
MockPlugin::MockPlugin (audioMasterCallback audioMaster)
: AudioEffectX (audioMaster, 2, 3)	// 2 programs, 3 parameters
{
	setNumInputs (2);		// stereo in
	setNumOutputs (2);		// stereo out
    setUniqueID (MOCK_PLUGIN_ID);	// identify
	canProcessReplacing ();	// supports replacing output
	canDoubleReplacing ();	// supports double precision processing
    programsAreChunks(MOCK_PROGRAMS_ARE_CHUNK);

    param1 = 1.f;			// default to 0 dB
    param2 = 0.5f;
    param3 = 0.0f;
	vst_strncpy (programName, "Default", kVstMaxProgNameLen);	// default program name
}

//-------------------------------------------------------------------------------------------------------
MockPlugin::~MockPlugin ()
{
	// nothing to do here
}

//-------------------------------------------------------------------------------------------------------
void MockPlugin::setProgramName (char* name)
{
	vst_strncpy (programName, name, kVstMaxProgNameLen);
}

//-----------------------------------------------------------------------------------------
void MockPlugin::getProgramName (char* name)
{
	vst_strncpy (name, programName, kVstMaxProgNameLen);
}

//-----------------------------------------------------------------------------------------
void MockPlugin::setParameter (VstInt32 index, float value)
{
    switch(index)
    {
    case 0:
        param1 = value;
        break;
    case 1:
        param2 = value;
        break;
    case 2:
        param3 = value;
        break;
    default:
        break;
    }
}

//-----------------------------------------------------------------------------------------
float MockPlugin::getParameter (VstInt32 index)
{
    float value = 1234.0f;
    switch(index)
    {
    case 0:
        value = param1;
        break;
    case 1:
        value = param2;
        break;
    case 2:
        value = param3;
        break;
    }

    return value;
}

//-----------------------------------------------------------------------------------------
void MockPlugin::getParameterName (VstInt32 index, char* label)
{
    switch(index)
    {
    case 0:
        vst_strncpy (label, "Param1", kVstMaxParamStrLen);
        break;
    case 1:
        vst_strncpy (label, "Param2", kVstMaxParamStrLen);
        break;
    case 2:
        vst_strncpy (label, "Param3", kVstMaxParamStrLen);
        break;
    default:
        vst_strncpy (label, "ERROR", kVstMaxParamStrLen);
        break;
    }
}

//-----------------------------------------------------------------------------------------
void MockPlugin::getParameterDisplay (VstInt32 index, char* text)
{
    switch(index)
    {
    case 0:
        dB2string (param1, text, kVstMaxParamStrLen);
        break;
    case 1:
        dB2string (param2, text, kVstMaxParamStrLen);
        break;
    case 2:
        dB2string (param3, text, kVstMaxParamStrLen);
        break;
    default:
        dB2string (1234.0, text, kVstMaxParamStrLen);
        break;
    }
}

//-----------------------------------------------------------------------------------------
void MockPlugin::getParameterLabel (VstInt32 index, char* label)
{
    switch(index)
    {
    case 0:
        vst_strncpy (label, "Label1", kVstMaxParamStrLen);
        break;
    case 1:
        vst_strncpy (label, "Label2", kVstMaxParamStrLen);
        break;
    case 2:
        vst_strncpy (label, "Label3", kVstMaxParamStrLen);
        break;
    default:
        vst_strncpy (label, "ERROR", kVstMaxParamStrLen);
        break;
    }
}

//------------------------------------------------------------------------
bool MockPlugin::getEffectName (char* name)
{
    vst_strncpy (name, MOCK_PLUGIN_NAME, kVstMaxEffectNameLen);
	return true;
}

//------------------------------------------------------------------------
bool MockPlugin::getProductString (char* text)
{
    vst_strncpy (text, MOCK_PLUGIN_NAME, kVstMaxProductStrLen);
	return true;
}

//------------------------------------------------------------------------
bool MockPlugin::getVendorString (char* text)
{
    vst_strncpy (text, "VST Preset Generator", kVstMaxVendorStrLen);
	return true;
}

//-----------------------------------------------------------------------------------------
VstInt32 MockPlugin::getVendorVersion ()
{ 
	return 1000; 
}

#ifdef MOCK_USE_CHUNK
//-----------------------------------------------------------------------------------------
VstInt32 MockPlugin::getChunk(void **data, bool isPreset)
{
    ///< Host stores plug-in state. Returns the size in bytes of the chunk (plug-in allocates the data array)

    //serialize the parameters values.
    std::stringstream values;
    if(isPreset)
    {
        values << "PROGRAM;";
    }
    else
    {
        values << "BANK;";
    }
    values << getParameter(0);
    values << ";";
    values << getParameter(1);
    values << ";";
    values << getParameter(2);

    this->parameterStates.clear();
    this->parameterStates = values.str();

    (*data) = const_cast<char*>(this->parameterStates.data());

    return this->parameterStates.size();
}
#endif

#ifdef MOCK_USE_CHUNK
//-----------------------------------------------------------------------------------------
std::vector<std::string> splitString(const std::string& inputString, const char delimiter)
{
    std::vector<std::string> splittedStrings;

    size_t previousDelimiterPosition = 0;
    size_t delimiterPosition = inputString.find(delimiter);
    while (delimiterPosition != std::string::npos)
    {
        splittedStrings.push_back(inputString.substr(previousDelimiterPosition, delimiterPosition - previousDelimiterPosition));
        previousDelimiterPosition = delimiterPosition + 1;
        delimiterPosition = inputString.find(delimiter, previousDelimiterPosition);
    }

    // The last one.
    std::string lastString = inputString.substr(previousDelimiterPosition, inputString.size());
    if (!lastString.empty())
    {
        splittedStrings.push_back(lastString);
    }

    return splittedStrings;
}
#endif

#ifdef MOCK_USE_CHUNK
//-----------------------------------------------------------------------------------------
VstInt32 MockPlugin::setChunk(void * data, VstInt32 byteSize, bool isPreset)
{
    ///< Host restores plug-in state

    std::string rawData((char*)data, byteSize);
    std::vector<std::string> splittedStrings = splitString(rawData, ';');
    if(splittedStrings.size() >= 4)
    {
        std::string& header = splittedStrings[0];
        if((isPreset && header == "PROGRAM") ||
           (!isPreset && header == "BANK"))
        {
            std::setlocale(LC_ALL, "C"); // Force atof to use classic locale.
            setParameter(0, atof(splittedStrings[1].c_str()));
            setParameter(1, atof(splittedStrings[2].c_str()));
            setParameter(2, atof(splittedStrings[3].c_str()));
        }
    }

    return 0;
}
#endif

//-----------------------------------------------------------------------------------------
void MockPlugin::processReplacing (float** /*inputs*/, float** /*outputs*/, VstInt32 /*sampleFrames*/)
{
    // Nothing to do here.
}

//-----------------------------------------------------------------------------------------
void MockPlugin::processDoubleReplacing (double** /*inputs*/, double** /*outputs*/, VstInt32 /*sampleFrames*/)
{
    // Nothing to do here.
}
