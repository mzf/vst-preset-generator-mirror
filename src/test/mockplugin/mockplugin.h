/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: mockplugin.h
  Author: Francois Mazen
  Date: 20/03/16 12:02
  Description: mockplugin to test vpg. Based on the Again sample from the SDK.
-------------------------------------------------------------------------------
*/

#ifndef __mockplugin__
#define __mockplugin__

#include "public.sdk/source/vst2.x/audioeffectx.h"

#include <string>

//-------------------------------------------------------------------------------------------------------
class MockPlugin : public AudioEffectX
{
public:
    MockPlugin (audioMasterCallback audioMaster);
    ~MockPlugin ();

	// Processing
	virtual void processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames);
	virtual void processDoubleReplacing (double** inputs, double** outputs, VstInt32 sampleFrames);

	// Program
	virtual void setProgramName (char* name);
	virtual void getProgramName (char* name);

	// Parameters
	virtual void setParameter (VstInt32 index, float value);
	virtual float getParameter (VstInt32 index);
	virtual void getParameterLabel (VstInt32 index, char* label);
	virtual void getParameterDisplay (VstInt32 index, char* text);
	virtual void getParameterName (VstInt32 index, char* text);

	virtual bool getEffectName (char* name);
	virtual bool getVendorString (char* text);
	virtual bool getProductString (char* text);
	virtual VstInt32 getVendorVersion ();

    // Chunk
#ifdef MOCK_USE_CHUNK
    VstInt32 getChunk (void** data, bool isPreset);
    VstInt32 setChunk (void* data, VstInt32 byteSize, bool isPreset);
#endif

protected:
    float param1;
    float param2;
    float param3;
	char programName[kVstMaxProgNameLen+1];

private:
    std::string parameterStates;
};

#endif //__mockplugin__
