include(mockplugincommon.pri)

# Target name of the plugin.
win32 {
    !contains(QMAKE_HOST.arch, x86_64) {
        message("win32 - 32 bits")
        TARGET = mockplugin_win32

    } else {
        message("win32 - 64 bits")
        TARGET = mockplugin_win64
    }
}
linux*: TARGET = mockplugin_linux
