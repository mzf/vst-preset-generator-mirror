include(mockplugincommon.pri)

# Target name of the plugin.
win32 {
    !contains(QMAKE_HOST.arch, x86_64) {
        message("win32 - 32 bits")
        TARGET = mockchunkplugin_win32

    } else {
        message("win32 - 64 bits")
        TARGET = mockchunkplugin_win64
    }
}
linux*: TARGET = mockchunkplugin_linux

# Specify that the plugin will use chunk for save/load presets.
DEFINES+= MOCK_USE_CHUNK=1
