TEMPLATE = lib
CONFIG += dll
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += .


SOURCES += mockplugin.cpp \
           public.sdk/source/vst2.x/audioeffect.cpp \
           public.sdk/source/vst2.x/audioeffectx.cpp \
           public.sdk/source/vst2.x/vstplugmain.cpp

HEADERS += mockplugin.h \
           public.sdk/source/vst2.x/aeffeditor.h \
           public.sdk/source/vst2.x/audioeffect.h \
           public.sdk/source/vst2.x/audioeffectx.h

QMAKE_LFLAGS += -static-libgcc -static-libstdc++

win32: QMAKE_LFLAGS += ../mockplugin/mockplugin.def -static -lpthread

DESTDIR = ../mockplugin/bin

# Disable symbol __cdecl for linux platform because of issue in VST SDK.
linux*:QMAKE_CXXFLAGS += -D__cdecl=\"\"

DISTFILES += \
    mockplugincommon.pri
