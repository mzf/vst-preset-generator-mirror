/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: maindialog.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: the MainDialog class, the core...
-------------------------------------------------------------------------------
*/

#include "maindialog.h"

#include "actionbutton.h"
#include "endiannessconverter.h"
#include "filetypedialog.h"
#include "parametertablemodel.h"
#include "parametervaluedelegate.h"
#include "pluginpage.h"
#include "randommodedelegate.h"
#include "vpgfile.h"
#include "vpgrandom.h"
#include "vpgsettings.h"
#include "vpgversion.h"
#include "vsthost.h"
#include "vstlistitem.h"
#include "vstlistmodel.h"
#include "vstlistview.h"
#include "vstpreset.h"
#include "vstpresetreader.h"

#include <QtWidgets>
#include <QSharedPointer>

#define VPG_LOGO ":/images/vpg.png" //logo filename
#define TOOL_BAR_ICON_SIZE 32

//-----------------------------------------------------------------------------
//MainDialog Class
//-----------------------------------------------------------------------------

//constructor
MainDialog::MainDialog(VpgSettings &settings) : QMainWindow(),
    vstListView(NULL),
    stackedPagesWidget(NULL),
    defaultPageIndex(0),
    pluginPageIndex(0),
    presetPageIndex(0),
    vstListModel(NULL),
    parameterTableView(NULL),
    vpgSettings(settings)
{
    //Qt objects------------------------------------------   

        //VST list
    vstListView = new VSTListView(this);
    vstListModel = new VSTListModel(this);
    vstListView->setModel(vstListModel);

        //VST host
    vstHost.reset(new VSTHost());

        //VST Preset reader.
    vstPresetReader.reset(new VSTPresetReader(vstHost));

        //table for parameters display
    parameterTableView = createParameterTableView(this);

    // Random number generator.
    QTime midnight(0, 0, 0);
    vpgRandom.reset(new VpgRandom(midnight.secsTo(QTime::currentTime())));


    //Menus-------------------------------------------------
        //VST Menu
    QAction* importVstAction = new QAction(QIcon(":/images/import.png"),tr("Load &VST Plugin..."),this);
    QAction* openVstListAction = new QAction(QIcon(":/images/open.png"),tr("&Open VST Plugin List..."), this);
    QAction* saveVstListAction = new QAction(QIcon(":/images/save.png"),tr("&Save VST Plugin List..."), this);
    QAction* newVstListAction = new QAction(QIcon(":/images/new.png"),tr("&New VST Plugin List"), this);

    const QIcon exitIcon = QIcon::fromTheme("application-exit");
    QAction* exitAction = new QAction(exitIcon, tr("E&xit"), this);//, &QWidget::close);
    exitAction->setShortcuts(QKeySequence::Quit);
    exitAction->setStatusTip(tr("Exit the application"));

    QMenu* vstMenu = menuBar()->addMenu(tr("&VST"));
    vstMenu->addAction(importVstAction);
    vstMenu->addSeparator();
    vstMenu->addAction(newVstListAction);
    vstMenu->addAction(openVstListAction);
    vstMenu->addAction(saveVstListAction);
    vstMenu->addSeparator();
    vstMenu->addAction(exitAction);

        //About
    QAction *aboutAction = new QAction(QIcon(":/images/vpg.png"),tr("&About VST Preset Generator"), this);
    QAction *aboutQtAction = new QAction(tr("About &Qt"), this);
    QMenu *aboutMenu = menuBar()->addMenu(tr("&About"));
    aboutMenu->addAction(aboutAction);
    aboutMenu->addAction(aboutQtAction);

    //Fill layout------------------------------------------

        //containers:
    QSplitter *mainContainer = new QSplitter(this);
    QWidget *leftContainer = new QWidget(mainContainer);

    QVBoxLayout *leftLayout = new QVBoxLayout(leftContainer);
    leftLayout->setContentsMargins(11,0,11,11);
    QToolBar* vstListToolBar = new QToolBar(tr("VST Plugin List"), this);
    vstListToolBar->addAction(importVstAction);
    vstListToolBar->addSeparator();
    vstListToolBar->addAction(newVstListAction);
    vstListToolBar->addAction(openVstListAction);
    vstListToolBar->addAction(saveVstListAction);
    vstListToolBar->setIconSize(QSize(TOOL_BAR_ICON_SIZE,TOOL_BAR_ICON_SIZE));
    leftLayout->addWidget(vstListToolBar);
    leftLayout->addWidget(vstListView);
    leftContainer->setLayout(leftLayout);

        //right layout
    stackedPagesWidget = createStackedPages(this,
                                            parameterTableView,
                                            importVstAction,
                                            openVstListAction,
                                            pluginPageIndex,
                                            presetPageIndex,
                                            defaultPageIndex);

        //global layout
    mainContainer->addWidget(leftContainer);
    mainContainer->addWidget(stackedPagesWidget);
    mainContainer->setChildrenCollapsible(false);
    QList<int> widgetWidths;
    widgetWidths.append(200);
    widgetWidths.append(400);
    mainContainer->setSizes(widgetWidths);
    setCentralWidget(mainContainer);

    //Qt connexions------------------------------------------

        //list interaction
    connect(vstListView, SIGNAL(currentIndexHasChanged( const QModelIndex &)),
            this, SLOT(onVSTListItemSelected(const QModelIndex &)));
    connect(vstListView, SIGNAL(addNewPresetActionTriggered()), this, SLOT(createNewPresetForCurrentPlugin()));
    connect(vstListView, SIGNAL(removePluginActionTriggered()), this, SLOT(removeCurrentPluginFromList()));
    connect(vstListView, SIGNAL(removePresetActionTriggered()), this, SLOT(removeCurrentPresetFromList()));

        //menu connections
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(showCopyright()));
    connect(aboutQtAction, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(openVstListAction, SIGNAL(triggered()), this, SLOT(openVstList()));
    connect(saveVstListAction, SIGNAL(triggered()), this, SLOT(saveVstList()));
    connect(newVstListAction, SIGNAL(triggered()), this, SLOT(newVstList()));
    connect(importVstAction, SIGNAL(triggered()), this, SLOT(loadVSTPluginFile()));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));

    //Application Settings -----------------------------------
    QString title(tr("VST Preset Generator V"));
    title += VpgVersion::getCurrentVersion().getStringVersionWithPlatform();
    setWindowTitle(title);
    resize(700,500);
    setWindowIcon(QIcon(":/images/vpg_icon.png"));

    //enable to drop file on the application
    setAcceptDrops(true);
}

//-----------------------------------------------------------------------------
MainDialog::~MainDialog()
{
}

//-----------------------------------------------------------------------------
QTableView* MainDialog::createParameterTableView(QWidget* parent) const
{
    QTableView* newParameterTableView = new QTableView(parent);

    ParameterValueDelegate* paramValueDelegate = new ParameterValueDelegate(parent);
    RandomModeDelegate* randomModeDelegate = new RandomModeDelegate(parent);

    newParameterTableView->setItemDelegateForColumn(1,randomModeDelegate);
    newParameterTableView->setItemDelegateForColumn(2,paramValueDelegate);
    newParameterTableView->setItemDelegateForColumn(3,paramValueDelegate);
    newParameterTableView->setEditTriggers(QAbstractItemView::AllEditTriggers);

    return newParameterTableView;
}

//-----------------------------------------------------------------------------
QWidget* MainDialog::createDefaultPage(QWidget* parent, QAction *openVstListAction, QAction *importVstAction) const
{
    QWidget* defaultPage = new QWidget(parent);

    QLabel* welcomeLabel = new QLabel(tr("<h2>Welcome to VST Preset Generator!</h2>\n\nPlease click an action below to start:"), parent);
    welcomeLabel->setAlignment(Qt::AlignCenter);

    ActionButton* importPluginButton = new ActionButton(parent, importVstAction);
    importPluginButton->setIconSize(QSize(TOOL_BAR_ICON_SIZE, TOOL_BAR_ICON_SIZE));
    importPluginButton->setMinimumHeight(50);

    ActionButton* openVSTListButton = new ActionButton(parent, openVstListAction);
    openVSTListButton->setIconSize(QSize(TOOL_BAR_ICON_SIZE, TOOL_BAR_ICON_SIZE));
    openVSTListButton->setMinimumHeight(50);

    QVBoxLayout* defaultPageLayout = new QVBoxLayout(defaultPage);
    defaultPageLayout->setAlignment(Qt::AlignHCenter);

    defaultPageLayout->addStretch();
    defaultPageLayout->addWidget(welcomeLabel);
    defaultPageLayout->addSpacing(30);
    defaultPageLayout->addWidget(importPluginButton);
    defaultPageLayout->addWidget(openVSTListButton);
    defaultPageLayout->addStretch();

    defaultPage->setLayout(defaultPageLayout);

    return defaultPage;
}

//-----------------------------------------------------------------------------
QWidget* MainDialog::createPresetPage(QWidget* parent, QTableView* tableView) const
{
    QWidget* presetPage = new QWidget(parent);

    // Button/Action to generate presets.
    QAction* generatePresetAction = new QAction(QIcon(":/images/generate.png"),tr("&Generate a preset file!"), parent);
    connect(generatePresetAction, SIGNAL(triggered()), this, SLOT(generatePreset()));

    ActionButton* generateButton = new ActionButton(parent, generatePresetAction);
    generateButton->setIconSize (QSize(TOOL_BAR_ICON_SIZE, TOOL_BAR_ICON_SIZE));
    generateButton->setEnabled(true);

    // Actions to change random methods.
    QAction* setUniformRandomAction = new QAction(QIcon(":/images/uniform.png"),tr("Set &uniform random method to all parameters"), parent);
    connect(setUniformRandomAction, SIGNAL(triggered()), this, SLOT(setGlobalRandomMethodUniform()));

    QAction* setNormalRandomAction = new QAction(QIcon(":/images/normal.png"),tr("Set &normal random method to all parameters"), parent);
    connect(setNormalRandomAction, SIGNAL(triggered()), this, SLOT(setGlobalRandomMethodNormal()));

    QAction* setConstantRandomAction = new QAction(QIcon(":/images/constant.png"),tr("Set &constant values to all parameters"), parent);
    connect(setConstantRandomAction, SIGNAL(triggered()), this, SLOT(setGlobalRandomMethodConstant()));

    QAction* setTotalRandomAction = new QAction(QIcon(":/images/total_random.png"),tr("Set &total random values"), parent);
    connect(setTotalRandomAction, SIGNAL(triggered()), this, SLOT(setGlobalRandomMethodTotal()));

    // Fill layout.
    QVBoxLayout* presetPageLayout = new QVBoxLayout(presetPage);
    presetPageLayout->setContentsMargins(11,0,11,11);
    QToolBar* randomMethodToolBar = new QToolBar(tr("Random methods"), parent);
    randomMethodToolBar->addAction(setUniformRandomAction);
    randomMethodToolBar->addAction(setNormalRandomAction);
    randomMethodToolBar->addAction(setConstantRandomAction);
    randomMethodToolBar->addAction(setTotalRandomAction);
    randomMethodToolBar->addSeparator();
    randomMethodToolBar->addWidget(generateButton);
    randomMethodToolBar->setIconSize(QSize(TOOL_BAR_ICON_SIZE,TOOL_BAR_ICON_SIZE));
    presetPageLayout->addWidget(randomMethodToolBar);
    presetPageLayout->addWidget(tableView);

    // Create page.
    presetPage->setLayout(presetPageLayout);

    return presetPage;
}

//-----------------------------------------------------------------------------
QStackedWidget* MainDialog::createStackedPages(QWidget* parent,
                                               QTableView* tableView,
                                               QAction* importVstAction,
                                               QAction* openVstListAction,
                                               int& newPluginPageIndex,
                                               int& newPresetPageIndex,
                                               int& newDefaultPageIndex) const
{
    QStackedWidget* newStackedPagesWidget = new QStackedWidget(parent);

    PluginPage* pluginPage = new PluginPage(newStackedPagesWidget);
    connect(pluginPage, SIGNAL(createPresetButtonClicked()), this, SLOT(createNewPresetForCurrentPlugin()));
    connect(pluginPage, SIGNAL(removePluginButtonClicked()), this, SLOT(removeCurrentPluginFromList()));
    connect(pluginPage, SIGNAL(openPresetButtonClicked()), this, SLOT(loadVSTPresetFile()));
    QWidget* presetPage = createPresetPage(newStackedPagesWidget, tableView);
    QWidget* defaultPage = createDefaultPage(newStackedPagesWidget, openVstListAction, importVstAction);

    newPluginPageIndex = newStackedPagesWidget->addWidget(pluginPage);
    newPresetPageIndex = newStackedPagesWidget->addWidget(presetPage);
    newDefaultPageIndex = newStackedPagesWidget->addWidget(defaultPage);
    newStackedPagesWidget->setCurrentIndex(newDefaultPageIndex);

    return newStackedPagesWidget;
}

//-----------------------------------------------------------------------------
//Dropping functions from Qt's patterns
//-----------------------------------------------------------------------------
void MainDialog::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
    {
        event->acceptProposedAction();
    }
}

//-----------------------------------------------------------------------------
void MainDialog::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasUrls())
    {
        QList<QUrl> urlList = event->mimeData()->urls();
        //QMessageBox::critical(this, "debug",urlList[0].path());
        //if there is many file, we only grab the first:
        QString fileName = urlList[0].path();

        if(fileName != NULL)
        {
            fileName.remove(0,1); //remove the "/" at the begining of the url
            if(isVSTPluginFileName(fileName))
            {
                loadParamFromVST(fileName);
            }
            else
            {
                openPreset(fileName);
            }

            event->acceptProposedAction();
        }
    }
}

//-----------------------------------------------------------------------------
void MainDialog::openPreset(QString filename)
{
    QList<QSharedPointer<VSTPreset> > newVstPresets;
    VSTPresetReader::ERROR_CODE errorCode = vstPresetReader->createVSTPresetsFromFile(filename, newVstPresets);

    if(errorCode != VSTPresetReader::ERROR_NO_ERROR)
    {
        QString errorMessage = tr("Unknown error!");
        switch(errorCode)
        {
        case VSTPresetReader::ERROR_CANNOT_OPEN_FILE:
            errorMessage = tr("Could not read the file.");
            break;
        case VSTPresetReader::ERROR_INVALID_FILE:
            errorMessage = tr("This is not a valid VST preset file.");
            break;
        case VSTPresetReader::ERROR_UNKNOWN_PLUGIN:
            errorMessage = tr("Could not find the plugin, please load the plugin first.");
            break;
        case VSTPresetReader::ERROR_WRONG_PLUGIN_VERSION:
            errorMessage = tr("Wrong version of the plugin, please load the corresponding plugin version.");
            break;
        case VSTPresetReader::ERROR_END_OF_FILE:
            errorMessage = tr("End of file reached. The file is incomplete.");
            break;
        case VSTPresetReader::ERROR_NO_ERROR:
            // Can't happen.
            break;
        }

        QMessageBox::critical(this, tr("Opening Preset File"), errorMessage);
    }
    else
    {
        QSharedPointer<VSTPreset> lastAddedPreset;

        for(QList<QSharedPointer<VSTPreset> >::const_iterator vstPresetIterator = newVstPresets.begin();
            vstPresetIterator != newVstPresets.end();
            ++vstPresetIterator)
        {
            if(!vstPresetIterator->isNull())
            {
                vstListModel->addPreset(*vstPresetIterator);
                lastAddedPreset = *vstPresetIterator;
            }
        }

        // Save preset folder.
        QFileInfo info(filename);
        this->vpgSettings.setVSTPresetsFolder(info.absolutePath());

        if(!lastAddedPreset.isNull())
        {
            vstListView->selectPresetItem(lastAddedPreset);
        }
    }
}

//-----------------------------------------------------------------------------
void MainDialog::generatePreset()
{
    // Get the current preset in from the model.
    ParameterTableModel* parameterTableModel = dynamic_cast<ParameterTableModel*>(parameterTableView->model());
    if(parameterTableModel != NULL)
    {
        const QSharedPointer<VSTPreset>& vstPreset = parameterTableModel->getVSTPreset();
        const QSharedPointer<VSTPlugin>& vstPlugin = vstPreset->getVSTPlugin();

        //get the type of the file

        int maxProgramPerBank = 100;
        bool isBankFileEnabled = vstPlugin->getNumberPrograms() > 0;

        FileTypeDialog fileTypeDialog(this, isBankFileEnabled, maxProgramPerBank);
        FileTypeDialog::FILE_TYPE fileType = (FileTypeDialog::FILE_TYPE)fileTypeDialog.exec();

        if(fileType != FileTypeDialog::FILE_TYPE_INVALID) //Cancel Button
        {
            int programCount = 1;
            bool isPresetTypeBank = false;
            if(fileType == FileTypeDialog::FILE_TYPE_BANK)
            {
                isPresetTypeBank = true;
                programCount = fileTypeDialog.getProgramPerBankValue();
            }

            //get the name of file
            QString fileName;
            if(isPresetTypeBank)
            {
                fileName = QFileDialog::getSaveFileName(this,
                                                  tr("Generate Preset Bank"),
                                                  this->vpgSettings.getGeneratedPresetFolder(),
                                                  tr("Preset Bank (*.fxb)"));
            }
            else
            {
                fileName = QFileDialog::getSaveFileName(this,
                                                  tr("Generate Preset Program"),
                                                  this->vpgSettings.getGeneratedPresetFolder(),
                                                  tr("Preset Program (*.fxp)"));
            }

            if(fileName != NULL) //cancel button
            {
                // Generate the content of the file.
                VSTPreset::ERROR_CODE presetErrorCode = VSTPreset::ERROR_NO_ERROR;

                if(vstPlugin->useChunk())
                {
                    if(isPresetTypeBank)
                    {
                        presetErrorCode = vstPreset->writeBankChunkToFile(fileName, programCount, vpgRandom);
                    }
                    else
                    {
                        presetErrorCode = vstPreset->writeProgramChunkToFile(fileName, vpgRandom);
                    }
                }
                else //non-chunk generation
                {
                    if(isPresetTypeBank)
                    {
                        presetErrorCode = vstPreset->writeBankToFile(fileName, programCount, vpgRandom);
                    }
                    else
                    {
                        presetErrorCode = vstPreset->writeProgramToFile(fileName, vpgRandom);
                    }
                }

                // Display error/success messages.
                if(presetErrorCode == VSTPreset::ERROR_NO_ERROR)
                {
                    QFileInfo info(fileName);
                    this->vpgSettings.setGeneratedPresetFolder(info.absolutePath());

                    //Operation success
                    QString success(tr("The preset file:\n%1\nhas been created. Enjoy!").arg(fileName));
                    QMessageBox::information(this, tr("Saving Preset File"), success);
                }
                else if (presetErrorCode == VSTPreset::ERROR_COULD_NOT_CREATE_FILE)
                {
                    QMessageBox::critical(this, tr("Saving Preset File"),
                                                tr("Could not create the file"));
                }
                else if(presetErrorCode == VSTPreset::ERROR_VST_HOST_INVALID_PROGRAM_NUMBER)
                {
                    QMessageBox::critical(this, tr("Generating Preset File"),
                                                tr("Can't generate Bank file for this VST plug-in."
                                                   "\nPlease try with a Program file."));
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------
bool MainDialog::saveVstList()
{
    bool isSuccess = true;

    //get the name of the file
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save VST Plugin List..."),
                                                          this->vpgSettings.getVpgFilesFolder(),
                                                          tr("VST Preset Generator file (*.vpg)"));
    if(fileName != NULL)
    {
        //save the folder for future use
        QFileInfo info(fileName);
        this->vpgSettings.setVpgFilesFolder(info.absolutePath());

        VpgFile::ERROR_CODE errorCode = VpgFile::WriteModelToFile(fileName, *vstListModel);
        if(errorCode != VpgFile::ERROR_NO_ERROR)
        {
            QString errorMessage;
            switch(errorCode)
            {
            case VpgFile::ERROR_COULD_NOT_OPEN_FILE:
                //error while opening
                errorMessage = tr("Error opening file: %1").arg(fileName);
                break;
            default:
                errorMessage = tr("Can't write the vpg file.");
                break;
            }

            QMessageBox::critical(this, tr("Saving VST Plugin List"), errorMessage);
        }

        vstListModel->setNeedSaving(false);
    }
    else
    {
        isSuccess = false;
    }

    return isSuccess;
}

//-----------------------------------------------------------------------------
bool MainDialog::saveVstListIfNeeded()
{
    bool shouldContinue = true;

    if(!vstListModel->isListEmpty() && !vstListModel->areDataSaved())
    {
        //ask for saving the actual list
        QMessageBox::StandardButton clickedButton = QMessageBox::question(this,
                                                                          tr("Save VST Plugin List..."),
                                                                          tr("Do you want to save current VST Plugin list?"),
                                                                          QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);

        if(clickedButton == QMessageBox::Cancel)
        {
            //quit the function
            shouldContinue = false;
        }
        else if(clickedButton == QMessageBox::Save)
        {
            shouldContinue = saveVstList();
        }
    }

    return shouldContinue;
}

//-----------------------------------------------------------------------------
QString MainDialog::getCheckedPluginFilePath(const VpgFile::Plugin &plugin)
{
    QFileInfo fileInfo(plugin.filePath);

    if(!fileInfo.exists())
    {
        QString warningMessage = tr("The file path for plugin named \"%1\" is not valid. Please select the plugin file in the following dialog.").arg(plugin.name);
        if(!plugin.filePath.isEmpty())
        {
            warningMessage.append(tr("\n\nOriginal file path:\n%1").arg(plugin.filePath));
        }

        QMessageBox::warning(this, tr("Invalid plugin file path"), warningMessage);

        QString newFilePath = getVSTFilePathFromDialog();
        if(newFilePath != NULL)
        {
            fileInfo.setFile(newFilePath);
        }
    }

    return fileInfo.filePath();
}

//-----------------------------------------------------------------------------
QString MainDialog::getVSTFilePathFromDialog()
{
    // Create file filters.
    QString windowsFilesFilter(tr("VST Plugin (*.dll)"));
    QString linuxFilesFilter(tr("Linux VST Plugin (*.so)"));
    QString allFilesFilter(tr("Any File (*.*)"));

    QString filesFilter = windowsFilesFilter + ";;" + linuxFilesFilter + ";;" + allFilesFilter;

    // Select the default filter depending on the operating system.
    QString selectedFilter = windowsFilesFilter;
    if(QSysInfo::windowsVersion() == QSysInfo::WV_None)
    {
        selectedFilter = linuxFilesFilter;
    }

    // Get the name of the file
    return  QFileDialog::getOpenFileName(this,
                                         tr("Load a VST Plugin"),
                                         this->vpgSettings.getVSTPluginsFolder(),
                                         filesFilter,
                                         &selectedFilter);
}

//-----------------------------------------------------------------------------
QString MainDialog::getPresetFilePathFromDialog()
{
    // Create file filters.
    QString presetFilesFilter(tr("Preset File (*.fxp *.fxb)"));
    QString allFilesFilter(tr("Any File (*.*)"));

    QString filesFilter = presetFilesFilter + ";;" +  allFilesFilter;

    // Get the name of the file
    return  QFileDialog::getOpenFileName(this,
                                         tr("Load a VST Preset"),
                                         this->vpgSettings.getVSTPresetsFolder(),
                                         filesFilter,
                                         &presetFilesFilter);
}

//-----------------------------------------------------------------------------
void MainDialog::resetVstListModelWithPlugins(const QList<VpgFile::Plugin>& extractedPlugins, QSharedPointer<VSTPreset>& firstLoadedPreset)
{
    vstListModel->beginModelChangeWithoutRefresh();
    vstListModel->clearList();

    // Try to load the plugins.
    for(QList<VpgFile::Plugin>::const_iterator pluginIterator = extractedPlugins.begin();
        pluginIterator != extractedPlugins.end();
        ++pluginIterator)
    {
        QString pluginFilePath = getCheckedPluginFilePath(*pluginIterator);

        QSharedPointer<VSTPlugin> newPlugin;
        bool isLoadingSuccess = loadVST(pluginFilePath, newPlugin);
        if(isLoadingSuccess && !newPlugin.isNull())
        {
            if(newPlugin->getFxId() == pluginIterator->id &&
               newPlugin->getFxVersion() == pluginIterator->version )
            {
                vstListModel->addPlugin(newPlugin);

                // Create the plugin's presets.
                for(QList<VpgFile::Preset>::const_iterator presetIterator = pluginIterator->presets.begin();
                    presetIterator != pluginIterator->presets.end();
                    ++presetIterator)
                {
                    QSharedPointer<VSTPreset> newPreset(new VSTPreset(newPlugin, presetIterator->name));

                    // Fill preset parameters' values.
                    if(newPreset->getParameters().size() == presetIterator->parameters.size())
                    {
                        QVector<QSharedPointer<Parameter> >& newParameters = newPreset->getParametersForModification();
                        for(int parameterIndex = 0; parameterIndex < newParameters.size(); ++parameterIndex)
                        {
                            newParameters[parameterIndex]->copyValuesFromOtherParameter(presetIterator->parameters[parameterIndex]);
                        }

                        // Finaly add the preset to the model.
                        vstListModel->addPreset(newPreset);
                        if(firstLoadedPreset.isNull())
                        {
                            firstLoadedPreset = newPreset;
                        }
                    }
                    else
                    {
                        QMessageBox::warning(this, tr("Loading VPG File"), tr("The parameter count of the preset named \"%1\" does not match (plugin \"%2\"). It won't be added to the list.").arg(presetIterator->name).arg(pluginIterator->name));
                    }
                }
            }
            else
            {
                QMessageBox::warning(this, tr("Loading VPG File"), tr("The ID of the plugin named \"%1\" does not match. It won't be added to the list.").arg(pluginIterator->name));
            }
        }
        else
        {
            QMessageBox::warning(this, tr("Loading VPG File"), tr("Could not load the plugin named \"%1\". It won't be added to the list.").arg(pluginIterator->name));
        }
    }

    vstListModel->endModelChangeWithoutRefresh();
}

//-----------------------------------------------------------------------------
void MainDialog::openVstList()
{
    bool shouldContinue = saveVstListIfNeeded();

    if(shouldContinue)
    {
        //get the name of the file
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open VST Plugin List"),
                                                              this->vpgSettings.getVpgFilesFolder(),
                                                              tr("VST Preset Generator file (*.vpg)"));
        if(fileName != NULL)
        {
            //save the folder for future use
            QFileInfo info(fileName);
            this->vpgSettings.setVpgFilesFolder(info.absolutePath());

            QSharedPointer<VSTPreset> firstLoadedPreset(NULL);
            QList<VpgFile::Plugin> extractedPlugins;
            VpgFile::ERROR_CODE errorCode = VpgFile::ExtractPluginsFromFile(fileName, extractedPlugins);
            if(errorCode != VpgFile::ERROR_NO_ERROR)
            {
                QString errorMessage;
                switch(errorCode)
                {
                case VpgFile::ERROR_COULD_NOT_OPEN_FILE:
                    errorMessage = tr("Could not open the file: %1").arg(fileName);
                    break;
                case VpgFile::ERROR_COULD_NOT_PARSE_FILE:
                    errorMessage = tr("Could not parse the file: %1").arg(fileName);
                    break;
                case VpgFile::ERROR_INVALID_VPG_FILE:
                    errorMessage = tr("The file is not a valid vpg file: %1").arg(fileName);
                    break;
                case VpgFile::ERROR_UNKNOWN_XML_TAG:
                    errorMessage = tr("Unknown XML tag in file: %1").arg(fileName);
                    break;
                case VpgFile::ERROR_INVALID_XML_ELEMENT:
                    errorMessage = tr("Invalid XML element in file: %1").arg(fileName);
                    break;
                case VpgFile::ERROR_NO_ERROR:
                    break;
                }
                QMessageBox::information(this, tr("Opening VST Plugin List"), errorMessage);
            }
            else
            {
                resetVstListModelWithPlugins(extractedPlugins, firstLoadedPreset);
            }

            vstListModel->setNeedSaving(false);

            // Select the first preset of the first plugin.
            if(!firstLoadedPreset.isNull())
            {
                vstListView->selectPresetItem(firstLoadedPreset);
            }
            else
            {
                stackedPagesWidget->setCurrentIndex(defaultPageIndex);
            }
        }
    }
}

//-----------------------------------------------------------------------------
void MainDialog::newVstList()
{
    if(!vstListModel->isListEmpty())
    {
        bool shouldContinue = saveVstListIfNeeded();

        if(shouldContinue)
        {
            //clear the list
            vstListModel->clearList();
            stackedPagesWidget->setCurrentIndex(defaultPageIndex);
        }
    }
}

//-----------------------------------------------------------------------------
void MainDialog::showCopyright()
{
    //show copyright information (GNU GPL)

    QMessageBox messageBox(this);
    messageBox.setWindowTitle(tr("About the VST Preset Generator "));

    QString text = tr("<h2>VST Preset Generator ");
    text += VpgVersion::getCurrentVersion().getStringVersionWithPlatform();
    text += tr("</h2>"
               "<p>Copyright &copy; 2007-2017 Fran&ccedil;ois Mazen"
               "<p>Please visit official website for more informations:"
               "<p><a href='http://vst-preset-generator.org/'>http://vst-preset-generator.org/</a>"
               "<p>This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version."
               "<p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
               "<p>You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
               "<p>VST is a registered trademark of Steinberg Soft- und Hardware, GmbH.");
    text += tr("<p>Special thanks to: Selfik, AzralXt, Spacedad, and... YOU for using this tool!");
    text += tr("<p>English translation: ");
    text += tr("Translator");

    messageBox.setText(text);

    QImage logo(VPG_LOGO);
    QPixmap pixMap = QPixmap::fromImage(logo);
    if (!pixMap.isNull())
    {
        messageBox.setIconPixmap(pixMap);
    }
    messageBox.addButton(QMessageBox::Ok);
    messageBox.exec();
}

//-----------------------------------------------------------------------------
void MainDialog::closeEvent(QCloseEvent* event)
{
    bool mustQuitApplication = true;
    if(vstListModel->areDataSaved() == false && vstListModel->isListEmpty() == false)
    {
        QMessageBox::StandardButton buttonClicked = QMessageBox::question(this,
                                                                          tr("Closing..."),
                                                                          tr("Do you want to save changes you made to your VST list?"),
                                                                          QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);

        if(buttonClicked == QMessageBox::Save)
        {
            saveVstList();
        }
        else if(buttonClicked == QMessageBox::Cancel)
        {
            //quit the function, but not the application
            event->ignore();
            mustQuitApplication = false;
        }
    }

    if(mustQuitApplication)
    {
        //quit the application
        event->accept();
    }
}

//-----------------------------------------------------------------------------
void MainDialog::loadVSTPluginFile()
{
    QString fileName = getVSTFilePathFromDialog();

    if(fileName != NULL)
    {
        loadParamFromVST(fileName);
    }
}

//-----------------------------------------------------------------------------
void MainDialog::loadVSTPresetFile()
{
    QString fileName = getPresetFilePathFromDialog();

    if(fileName != NULL)
    {
        openPreset(fileName);
    }
}

//-----------------------------------------------------------------------------
void MainDialog::addNewRandomPreset(const QSharedPointer<VSTPlugin>& vstPlugin)
{
    QSet<QString> existingPresetNames = vstListModel->getUniquePluginPresetNames(vstPlugin);

    // Generate a new unique name.
    unsigned int presetIndex = 0;
    QString baseName = tr("New ");
    QString newPresetName;
    do
    {
        ++presetIndex;
        newPresetName.setNum(presetIndex);
        newPresetName.prepend(baseName);
    }
    while(existingPresetNames.contains(newPresetName) && presetIndex < 9999);

    QSharedPointer<VSTPreset> vstPreset(new VSTPreset(vstPlugin, newPresetName));
    vstListModel->addPreset(vstPreset);
    vstListView->selectPresetItem(vstPreset);
}

//-----------------------------------------------------------------------------
void MainDialog::loadParamFromVST(const QString& fileName)
{
    QSharedPointer<VSTPlugin> vstPlugin;
    if(loadVST(fileName, vstPlugin) && !vstPlugin.isNull())
    {
        //add the new VST to our list:
        vstListModel->addPlugin(vstPlugin);

        if(!vstListModel->doesExistPluginPresets(vstPlugin))
        {
            // Create a default random preset.
            addNewRandomPreset(vstPlugin);
        }
        else
        {
            // Select the first preset.
            QSharedPointer<VSTPreset> vstPreset = vstListModel->getPluginPresets(vstPlugin).first()->getVstPreset();
            if(!vstPreset.isNull())
            {
                vstListView->selectPresetItem(vstPreset);
            }
            else
            {
                stackedPagesWidget->setCurrentIndex(defaultPageIndex);
            }
        }
    }
}


//-----------------------------------------------------------------------------
bool MainDialog::loadVST(const QString& fileName, QSharedPointer<VSTPlugin>& vstPlugin)
{
    bool isLoadingSuccessful = true;
    if(fileName != NULL)
    {
        QFileInfo info(fileName);
        this->vpgSettings.setVSTPluginsFolder(info.absolutePath());

        vstPlugin.reset();
        VSTHost::ERROR_CODE code = vstHost->getOrLoadPlugin(fileName, vstPlugin);
        if(code != VSTHost::ERROR_NO_ERROR)
        {
            //error while opening
            QString message = tr("Error: ");
            switch(code)
            {
            case VSTHost::ERROR_PLUGIN_MAIN:
                message += tr("VST Plugin main entry not found");
                break;

            case VSTHost::ERROR_PLUGIN_INSTANCE:
                message += tr("failed to create effect instance");
                break;

            case VSTHost::ERROR_PLUGIN_VALID:
                message += tr("it's not a valid VST plugin");
                break;

            case VSTHost::ERROR_PLUGIN_LOAD:
            default:
                message += tr("failed to load VST Plugin library\n%1").arg(fileName);
                break;
            }

            QMessageBox::critical(this, tr("Loading plugin VST"), message);
            isLoadingSuccessful = false;
        }
    }

    return isLoadingSuccessful;
}

//-----------------------------------------------------------------------------
bool MainDialog::isVSTPluginFileName(const QString &fileName) const
{
    return fileName.endsWith (QString("dll")) || fileName.endsWith (QString("so"));
}

//-----------------------------------------------------------------------------
void MainDialog::loadPresetInParameterTable(const QSharedPointer<VSTPreset>& vstPreset)
{
    QAbstractItemModel* oldModel = parameterTableView->model();

    ParameterTableModel* newModel = new ParameterTableModel(vstPreset);
    parameterTableView->setModel(newModel);
    parameterTableView->resizeColumnsToContents();
    parameterTableView->resizeRowsToContents();
    connect(newModel, SIGNAL(dataChanged( const QModelIndex &, const QModelIndex & )), parameterTableView, SLOT(resizeColumnsToContents()));

    delete oldModel;
}

//-----------------------------------------------------------------------------
void MainDialog::createNewPresetForCurrentPlugin()
{
    PluginPage* pluginPage = dynamic_cast<PluginPage*>(stackedPagesWidget->widget(pluginPageIndex));
    if(pluginPage)
    {
        const QSharedPointer<VSTPlugin>& vstPlugin = pluginPage->getVSTPlugin();
        if(!vstPlugin.isNull())
        {
            addNewRandomPreset(vstPlugin);
        }
    }
}

//-----------------------------------------------------------------------------
void MainDialog::removeCurrentPluginFromList()
{
    PluginPage* pluginPage = dynamic_cast<PluginPage*>(stackedPagesWidget->widget(pluginPageIndex));
    if(pluginPage)
    {
        const QSharedPointer<VSTPlugin>& vstPlugin = pluginPage->getVSTPlugin();
        if(!vstPlugin.isNull())
        {
            QMessageBox::StandardButton buttonClicked = QMessageBox::question(this,
                                                                              tr("Remove Plugin"),
                                                                              tr("Are you sure you want to remove the plugin named \"%1\"?").arg(vstPlugin->getName()),
                                                                              QMessageBox::Yes | QMessageBox::No);
            if(buttonClicked == QMessageBox::Yes)
            {
                vstListModel->removePlugin(vstPlugin);
                stackedPagesWidget->setCurrentIndex(defaultPageIndex);
            }
        }
    }
}

//-----------------------------------------------------------------------------
void MainDialog::removeCurrentPresetFromList()
{
    QModelIndex currentIndex = vstListView->currentIndex();
    if(currentIndex.isValid())
    {
        VSTListItem* item = static_cast<VSTListItem*>(currentIndex.internalPointer());
        if(item != NULL)
        {
            if(item->getType() == VSTListItem::PRESET_TYPE)
            {
                const QSharedPointer<VSTPreset>& vstPreset = item->getVstPreset();
                if(!vstPreset.isNull())
                {
                    QMessageBox::StandardButton buttonClicked = QMessageBox::question(this,
                                                                                      tr("Remove Preset"),
                                                                                      tr("Are you sure you want to remove the preset named \"%1\"?").arg(vstPreset->getName()),
                                                                                      QMessageBox::Yes | QMessageBox::No);
                    if(buttonClicked == QMessageBox::Yes)
                    {
                        vstListModel->removePreset(vstPreset);
                        stackedPagesWidget->setCurrentIndex(defaultPageIndex);
                    }
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------
void MainDialog::setGlobalRandomMethod(Parameter::RandomMode method)
{
    QMessageBox::StandardButton buttonClicked = QMessageBox::question(this,
                                                                      tr("Setting a random method"),
                                                                      tr("If you change the random method of all parameters, your old settings will be lost.\nDo you want to continue?"),
                                                                      QMessageBox::Yes | QMessageBox::No);

    if(buttonClicked == QMessageBox::Yes)
    {
        ParameterTableModel* parameterTableModel = dynamic_cast<ParameterTableModel*>(parameterTableView->model());
        if(parameterTableModel != NULL)
        {
            parameterTableModel->ChangeRandomMethodForAll(method, vpgRandom);
            parameterTableView->resizeColumnsToContents ();
        }
    }
}

//-----------------------------------------------------------------------------
void MainDialog::setGlobalRandomMethodConstant()
{
    setGlobalRandomMethod(Parameter::RandomMode_Constant);
}

//-----------------------------------------------------------------------------
void MainDialog::setGlobalRandomMethodNormal()
{
    setGlobalRandomMethod(Parameter::RandomMode_Normal);
}

//-----------------------------------------------------------------------------
void MainDialog::setGlobalRandomMethodTotal()
{
    setGlobalRandomMethod(Parameter::RandomMode_Random);
}

//-----------------------------------------------------------------------------
void MainDialog::onVSTListItemSelected(const QModelIndex &index)
{
    if(index.isValid())
    {
        VSTListItem* item = static_cast<VSTListItem*>(index.internalPointer());
        if(item != NULL)
        {
            if(item->getType() == VSTListItem::VST_TYPE)
            {
                // Click on a VST.
                const QSharedPointer<VSTPlugin>& vstPlugin = item->getVSTPlugin();
                if(!vstPlugin.isNull())
                {
                    PluginPage* pluginPage = dynamic_cast<PluginPage*>(stackedPagesWidget->widget(pluginPageIndex));
                    if(pluginPage)
                    {
                        pluginPage->setVSTPlugin(vstPlugin);
                    }
                }

                stackedPagesWidget->setCurrentIndex(pluginPageIndex);
            }
            else if(item->getType() == VSTListItem::PRESET_TYPE)
            {
                // Click on a preset.
                QSharedPointer<VSTPreset> vstPreset = item->getVstPreset();
                if(!vstPreset.isNull())
                {
                    loadPresetInParameterTable(vstPreset);

                    stackedPagesWidget->setCurrentIndex(presetPageIndex);
                }
            }
        }
    }
    else
    {
        stackedPagesWidget->setCurrentIndex(defaultPageIndex);
    }
}

//-----------------------------------------------------------------------------
void MainDialog::setGlobalRandomMethodUniform()
{
    setGlobalRandomMethod(Parameter::RandomMode_Uniform);
}

