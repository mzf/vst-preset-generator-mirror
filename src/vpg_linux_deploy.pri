# Install on linux:
linux* {
    VPG_DIR = /usr/bin
    message(install dir: $$VPG_DIR)
    vpg_install.path = $$VPG_DIR
    vpg_install.files = $${OUT_PWD}/$${TARGET}
    INSTALLS += vpg_install
}
