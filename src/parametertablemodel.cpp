/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: parametertablemodel.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: ParameterTableModel class contains data about parameters (type of
               random generation, values...) and interface with the table view.
-------------------------------------------------------------------------------
*/

#include "parametertablemodel.h"
#include "vpgrandom.h"
#include "vstplugin.h"

#include <QtWidgets>
#include <QSharedPointer>

//-----------------------------------------------------------------------------
ParameterTableModel::ParameterTableModel(const QSharedPointer<VSTPreset> &vstPreset, QObject *parent) :
    QAbstractTableModel(parent),
    vstPreset(vstPreset)
{

}

ParameterTableModel::~ParameterTableModel()
{
}

//-----------------------------------------------------------------------------
int ParameterTableModel::rowCount(const QModelIndex& /*parent*/) const
{
    return vstPreset->getParameters().size();

}

//-----------------------------------------------------------------------------
int ParameterTableModel::columnCount(const QModelIndex& /*parent*/) const
{
    return COLUMN_COUNT;
}

//-----------------------------------------------------------------------------
QVariant ParameterTableModel::data(const QModelIndex &index, int role) const
{
    QVariant returnedData;

    if (index.isValid())
    {
        int parameterIndex = index.row();
        const QSharedPointer<Parameter>& parameter = vstPreset->getParameters().at(parameterIndex);

        if(role == Qt::DisplayRole)
        {
            //Name column
            if(index.column() == COLUMN_NAME)
            {
                returnedData = parameter->getName();
            }

            //Random method column
            else if(index.column() == COLUMN_RANDOM_METHOD)
            {
                if(parameter->getRandomMode() == Parameter::RandomMode_Uniform)
                {
                    returnedData = tr("uniform");
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Normal)
                {
                    returnedData = tr("normal");
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Constant)
                {
                    returnedData = tr("constant");
                }
            }

            //Val 1 column
            else if(index.column() == COLUMN_RANDOM_VALUE_1)
            {
                if(parameter->getRandomMode() == Parameter::RandomMode_Uniform)
                {
                    returnedData = tr("min: %1").arg(getParameterDisplay(parameterIndex, parameter->getUniformMinValue()));
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Normal)
                {
                    returnedData = tr("mid: %1").arg(getParameterDisplay(parameterIndex, parameter->getNormalMiddleValue()));
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Constant)
                {
                    returnedData = tr("val: %1").arg(getParameterDisplay(parameterIndex, parameter->getConstantValue()));
                }
            }

            //Val 2 column
            else if(index.column() == COLUMN_RANDOM_VALUE_2)
            {
                if(parameter->getRandomMode() == Parameter::RandomMode_Uniform)
                {
                    returnedData = tr("max: %1").arg(getParameterDisplay(parameterIndex, parameter->getUniformMaxValue()));
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Normal)
                {
                    QString stringBuffer;
                    stringBuffer.setNum(parameter->getNormalStandardDeviation());
                    returnedData = tr("dev: %1").arg(stringBuffer);
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Constant)
                {
                    returnedData = " ";
                }
            }
        }
        else if (role == Qt::DecorationRole)
        {
            if(index.column() == COLUMN_RANDOM_METHOD)
            {
                if(parameter->getRandomMode() == Parameter::RandomMode_Uniform)
                {
                    returnedData = QIcon(":/images/uniform.png");
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Normal)
                {
                    returnedData = QIcon(":/images/normal.png");
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Constant)
                {
                    returnedData = QIcon(":/images/constant.png");
                }
            }
        }
        else if (role == Qt::EditRole)
        {
            if(index.column() == COLUMN_RANDOM_METHOD)
            {
                returnedData = parameter->getRandomMode();
            }
            else if(index.column() == COLUMN_RANDOM_VALUE_1)
            {
                if(parameter->getRandomMode() == Parameter::RandomMode_Uniform)
                {
                    returnedData = parameter->getUniformMinValue();
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Normal)
                {
                    returnedData = parameter->getNormalMiddleValue();
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Constant)
                {
                    returnedData = parameter->getConstantValue();
                }
            }
            else if(index.column() == COLUMN_RANDOM_VALUE_2)
            {
                if(parameter->getRandomMode() == Parameter::RandomMode_Uniform)
                {
                    returnedData = parameter->getUniformMaxValue();
                }
                else if(parameter->getRandomMode() == Parameter::RandomMode_Normal)
                {
                    returnedData = parameter->getNormalStandardDeviation();
                }
            }
        }
    }
    return returnedData;
}

//-----------------------------------------------------------------------------
QVariant ParameterTableModel::headerData(int section, Qt::Orientation orientation,
                                  int role) const
{
    QVariant returnedData;

    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            switch(section)
            {
            case COLUMN_NAME:
                returnedData = tr("name");
                break;
            case COLUMN_RANDOM_METHOD:
                returnedData = tr("random mode");
                break;
            case COLUMN_RANDOM_VALUE_1:
                returnedData = tr("val 1");
                break;
            case COLUMN_RANDOM_VALUE_2:
                returnedData = tr("val 2");
                break;
            }
        }
        else
        {
            returnedData = section + 1;
        }
    }

    return returnedData;
}

//-----------------------------------------------------------------------------
Qt::ItemFlags ParameterTableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags returnedFlags = QAbstractItemModel::flags(index);

    if (!index.isValid())
    {
        returnedFlags = Qt::ItemIsEnabled;
    }
    else
    {
        //the first column can not be edited
        //neither the last column in constant mode

        if(index.column() == COLUMN_RANDOM_METHOD ||
           index.column() == COLUMN_RANDOM_VALUE_1 ||
           (index.column() == COLUMN_RANDOM_VALUE_2 && (vstPreset->getParameters().at(index.row())->getRandomMode() != Parameter::RandomMode_Constant)))
        {
            returnedFlags = QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
        }
    }

    return returnedFlags;
}

//-----------------------------------------------------------------------------
bool ParameterTableModel::setData(const QModelIndex &modelIndex,
                           const QVariant &value, int role)
{
    bool isSuccess = false;

    if (modelIndex.isValid() && role == Qt::EditRole)
    {
        QSharedPointer<Parameter> parameter = vstPreset->getParametersForModification()[modelIndex.row()];

        //Random mode column
        if((modelIndex.column() == COLUMN_RANDOM_METHOD) &&
           ((Parameter::RandomMode)value.toInt() < Parameter::RandomMode_RandomModeMax))
        {
            parameter->setRandomMode((Parameter::RandomMode)value.toInt());
        }

        //Val 1 column
        if(modelIndex.column() == COLUMN_RANDOM_VALUE_1)
        {
            switch(parameter->getRandomMode())
            {
            case Parameter::RandomMode_Uniform:
                if(value.toDouble() > parameter->getUniformMaxValue())
                { //avoid min > max
                    parameter->setUniformMinValue(parameter->getUniformMaxValue());
                }
                else
                {
                    parameter->setUniformMinValue(value.toDouble());
                }
                break;
            case Parameter::RandomMode_Normal:
                parameter->setNormalMiddleValue(value.toDouble());
                break;
            case Parameter::RandomMode_Constant:
                parameter->setConstantValue(value.toDouble());
                break;
            default :
                break;
            }
        }

        //Val 2 column
        if(modelIndex.column() == COLUMN_RANDOM_VALUE_2)
        {
            switch(parameter->getRandomMode())
            {
            case Parameter::RandomMode_Uniform:
                if(value.toDouble() < parameter->getUniformMinValue())
                {//avoid max < min
                    parameter->setUniformMaxValue(parameter->getUniformMinValue());
                }
                else
                {
                    parameter->setUniformMaxValue(value.toDouble());
                }
                break;
            case Parameter::RandomMode_Normal:
                parameter->setNormalStandardDeviation(value.toDouble());
                break;
            default:
                break;
            }
        }

        //Update the model
        vstPreset->getParametersForModification().replace(modelIndex.row(), parameter);
        emit dataChanged(modelIndex, index(modelIndex.row(), COLUMN_COUNT - 1));
        isSuccess = true;
    }

    return isSuccess;
}

//-----------------------------------------------------------------------------
void ParameterTableModel::beginModelChangeWithoutRefresh()
{
    beginResetModel();
}

//-----------------------------------------------------------------------------
void ParameterTableModel::endModelChangeWithoutRefresh()
{
    endResetModel();
}

//-----------------------------------------------------------------------------
void ParameterTableModel::ChangeRandomMethodForAll(Parameter::RandomMode method, const QSharedPointer<VpgRandom>& vpgRandom)
{
    QVector<QSharedPointer<Parameter> >& parameters = vstPreset->getParametersForModification();
    for(QVector<QSharedPointer<Parameter> >::iterator parameterIterator = parameters.begin();
        parameterIterator != parameters.end();
        ++parameterIterator)
    {
        QSharedPointer<Parameter> parameter = *parameterIterator;
        switch(method)
        {
            case Parameter::RandomMode_Uniform:
            case Parameter::RandomMode_Normal :
            case Parameter::RandomMode_Constant :
                parameter->setRandomMode(method);
                break;
            case Parameter::RandomMode_Random :
                parameter->setRandomMode((Parameter::RandomMode)(int)vpgRandom->getUniformValue(0, Parameter::RandomMode_RandomModeMax));
                parameter->setUniformMinValue(vpgRandom->getUniformValue(0,1));
                parameter->setUniformMaxValue(vpgRandom->getUniformValue(parameter->getUniformMinValue(), 1));
                parameter->setNormalMiddleValue(vpgRandom->getUniformValue(0,1));
                parameter->setNormalStandardDeviation(vpgRandom->getUniformValue(0,0.2));
                parameter->setConstantValue(vpgRandom->getUniformValue(0,1));
            break;
            default :
                break;
        }
    }

    emit dataChanged(index(0, 0), index(parameters.size() - 1, COLUMN_COUNT - 1));
}

//-----------------------------------------------------------------------------
const QSharedPointer<VSTPreset> &ParameterTableModel::getVSTPreset() const
{
    return vstPreset;
}

//-----------------------------------------------------------------------------
bool ParameterTableModel::doesIndexUseParameterDisplay(const QModelIndex &index) const
{
    bool useParameterDisplay = false;

    // Only uniform min/max, constant val and normal mid use real plugin display.
    Parameter::RandomMode randomMode = vstPreset->getParameters()[index.row()]->getRandomMode();
    if(index.column() == COLUMN_RANDOM_VALUE_1 ||
       ((index.column() == COLUMN_RANDOM_VALUE_2) && ((randomMode == Parameter::RandomMode_Constant) ||
                                                      (randomMode == Parameter::RandomMode_Uniform))))
    {
        useParameterDisplay = true;
    }

    return useParameterDisplay;
}

//-----------------------------------------------------------------------------
QString ParameterTableModel::getParameterDisplay(int parameterIndex, float parameterValue) const
{
    vstPreset->getVSTPlugin()->setParameter(parameterIndex, parameterValue);
    QString display = vstPreset->getVSTPlugin()->getParameterDisplay(parameterIndex);

    if(display.isEmpty())
    {
        display.setNum(parameterValue);
    }

    return display;
}
