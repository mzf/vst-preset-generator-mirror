/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstlistview.cpp
  Author: Francois Mazen
  Date: 27/02/17 18:25
  Description: the VSTListView is a custom QTreeView.
-------------------------------------------------------------------------------
*/

#include "vstlistview.h"

#include "presetitemnamedelegate.h"
#include "vstlistitem.h"
#include "vstlistmodel.h"
#include "vstplugin.h"
#include "vstpreset.h"

#include <QHeaderView>
#include <QMenu>
#include <QMouseEvent>
#include <QStyledItemDelegate>

//-----------------------------------------------------------------------------
VSTListView::VSTListView(QWidget *parent) :
    QTreeView(parent)
{
    header()->hide();
    setExpandsOnDoubleClick(false);
    setItemsExpandable(false);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setItemDelegate(new PresetItemNameDelegate(this));
    setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::EditKeyPressed);
}

//-----------------------------------------------------------------------------
VSTListView::~VSTListView()
{

}

//-----------------------------------------------------------------------------
void VSTListView::selectPresetItem(const QSharedPointer<VSTPreset>& vstPreset)
{
    if(!vstPreset.isNull())
    {
        VSTListModel* vstListModel = dynamic_cast<VSTListModel*>(model());
        if(vstListModel != NULL)
        {
            QModelIndex presetModelIndex = vstListModel->getPresetIndex(vstPreset);
            if(presetModelIndex.isValid())
            {
                QModelIndex parentModelIndex = model()->parent(presetModelIndex);
                if(parentModelIndex.isValid())
                {
                    collapseAll();
                    expand(parentModelIndex);
                    setCurrentIndex(presetModelIndex);
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------
void VSTListView::contextMenuEvent(QContextMenuEvent *event)
{
    QModelIndex selectedIndex = currentIndex();
    if(selectedIndex.isValid())
    {
        VSTListItem* item = static_cast<VSTListItem*>(selectedIndex.internalPointer());
        if(item->getType() == VSTListItem::VST_TYPE)
        {
            // Click on a VST.
            QMenu menu(this);

            QAction* addNewPresetAction = menu.addAction(QIcon(":/images/edit_add.png"), tr("Add new random preset"));
            connect(addNewPresetAction, SIGNAL(triggered()), this, SIGNAL(addNewPresetActionTriggered()));

            QAction* removePluginAction = menu.addAction(QIcon(":/images/button_cancel.png"), tr("Remove plugin \"%1\"").arg(item->getVSTPlugin()->getName()));
            connect(removePluginAction, SIGNAL(triggered()), this, SIGNAL(removePluginActionTriggered()));

            menu.exec(event->globalPos());
        }
        else if(item->getType() == VSTListItem::PRESET_TYPE)
        {
            // Click on a Preset.
            QMenu menu(this);

            QAction* renamePresetAction = menu.addAction(QIcon(":/images/edit.png"), tr("Rename..."));
            connect(renamePresetAction, SIGNAL(triggered()), this, SLOT(editSelectedItem()));

            QAction* removePresetAction = menu.addAction(QIcon(":/images/button_cancel.png"), tr("Remove preset \"%1\"").arg(item->getVstPreset()->getName()));
            connect(removePresetAction, SIGNAL(triggered()), this, SIGNAL(removePresetActionTriggered()));

            menu.exec(event->globalPos());
        }
    }
}

//-----------------------------------------------------------------------------
void VSTListView::editSelectedItem()
{
    QModelIndex selectedIndex = currentIndex();
    if(selectedIndex.isValid())
    {
        edit(selectedIndex);
    }
}

//-----------------------------------------------------------------------------
void VSTListView::currentChanged(const QModelIndex &current, const QModelIndex &previous)
{
    QTreeView::currentChanged(current, previous);

    VSTListItem* item = static_cast<VSTListItem*>(current.internalPointer());
    if(item != NULL)
    {
        if(item->getType() == VSTListItem::VST_TYPE)
        {
            // Click on a VST.
            collapseAll();
            expand(current);
        }
    }

    emit currentIndexHasChanged(current);
}

//-----------------------------------------------------------------------------
void VSTListView::mousePressEvent(QMouseEvent *event)
{
    // QTreeView::mousePressEvent highlights many items when the mouse is
    // pressed and that we collapse/expand items.
    // So we manualy select the index to avoid this behavior.

    QModelIndex clickedIndex = indexAt(event->pos());
    if(clickedIndex.isValid())
    {
        setCurrentIndex(clickedIndex);
    }
}

//-----------------------------------------------------------------------------
void VSTListView::mouseDoubleClickEvent(QMouseEvent* /*event*/)
{
    edit(currentIndex());
}


