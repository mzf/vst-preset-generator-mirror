/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: actionbutton.cpp
  Author: Francois Mazen
  Date: 16/03/17 19:25
  Description: A QPushButton linked to an action, inspired by
               https://wiki.qt.io/PushButton_Based_On_Action
-------------------------------------------------------------------------------
*/

#include "actionbutton.h"

#include <QAction>

//-----------------------------------------------------------------------------
ActionButton::ActionButton(QWidget *parent, QAction *action) :
    QPushButton(parent),
    action(NULL)
{
    setAction(action);
}

//-----------------------------------------------------------------------------
void ActionButton::setAction(QAction *newAction)
{
    // if I've got already an action associated to the button
    // remove all connections
    if ( action && action != newAction )
    {
        disconnect(action, &QAction::changed, this, &ActionButton::updateButtonStatusFromAction);
        disconnect(this, &ActionButton::clicked, action, &QAction::trigger);
    }

    // store the action
    action = newAction;

    // configure the button
    updateButtonStatusFromAction();

    // connect the action and the button
    // so that when the action is changed the
    // button is changed too!
    connect(action, &QAction::changed, this, &ActionButton::updateButtonStatusFromAction);

    // connect the button to the slot that forwards the
    // signal to the action
    connect(this, &ActionButton::clicked, action, &QAction::trigger);
}

//-----------------------------------------------------------------------------
void ActionButton::updateButtonStatusFromAction()
{
    if(action != NULL)
    {
        setText(action->text());
        setStatusTip(action->statusTip());
        setToolTip(action->toolTip());
        setIcon(action->icon());
        setEnabled(action->isEnabled());
        setCheckable(action->isCheckable());
        setChecked(action->isChecked());
    }
}

