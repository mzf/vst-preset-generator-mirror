/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: actionbutton.h
  Author: Francois Mazen
  Date: 16/03/17 19:25
  Description: A QPushButton linked to an action, inspired by
               https://wiki.qt.io/PushButton_Based_On_Action
-------------------------------------------------------------------------------
*/

#ifndef ACTIONBUTTON_H
#define ACTIONBUTTON_H

#include <QPushButton>


class ActionButton : public QPushButton
{
    Q_OBJECT

public:
    ActionButton(QWidget* parent, QAction* action);

private:
    void setAction(QAction* newAction);
    QAction* action;

public slots:
    void updateButtonStatusFromAction();
};

#endif // ACTIONBUTTON_H
