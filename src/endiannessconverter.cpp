/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: endiannessconverter.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: some tools because of big-endian/little-endian notation in the
               VST preset files (fxb, fxp). See Steinberg VST SDK for more
               informations
-------------------------------------------------------------------------------
*/

#include "endiannessconverter.h"

//-----------------------------------------------------------------------------
int32_t EndiannessConverter::fromBigEndian(const QByteArray& bigEndianInputValue)
{
    return changeEndianness(*((int32_t*)bigEndianInputValue.data()));
}

//-----------------------------------------------------------------------------
float EndiannessConverter::floatFromBigEndian(const QByteArray &bigEndianInputValue)
{
    int32_t integer = fromBigEndian(bigEndianInputValue);
    float f = 0.f;
    memcpy(&f, &integer, sizeof(f));
    return f;
}

//-----------------------------------------------------------------------------
QByteArray EndiannessConverter::toBigEndian(const int32_t& littleEndianInputValue)
{
    int32_t result = changeEndianness(littleEndianInputValue);
    QByteArray bigEndianOutputValue((const char*)&result, 4);
    return bigEndianOutputValue;
}

//-----------------------------------------------------------------------------
QByteArray EndiannessConverter::toBigEndian(const float &littleEndianInputValue)
{
    int32_t integerValue = 0;
    memcpy(&integerValue, &littleEndianInputValue, 4);
    return toBigEndian(integerValue);
}

//-----------------------------------------------------------------------------
QString EndiannessConverter::getMultiCharFromBigEndian(int32_t bigEndianInputValue)
{
    QString multiChar;
    char* pt = (char*)&bigEndianInputValue;
    multiChar += pt[3];
    multiChar += pt[2];
    multiChar += pt[1];
    multiChar += pt[0];
    //multiChar += '\0';
    return multiChar;
}

//-----------------------------------------------------------------------------
int32_t EndiannessConverter::changeEndianness(const int32_t &value)
{
    int32_t result = 0;
    result |= (value & 0x000000FF) << 24;
    result |= (value & 0x0000FF00) << 8;
    result |= (value & 0x00FF0000) >> 8;
    result |= (value & 0xFF000000) >> 24;
    return result;
}
