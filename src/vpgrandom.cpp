/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgrandom.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: the VpgRandom class provides random modules for different
               types of random generation (uniform, normal, constant, ...)
               The random functions come from:
               "Random Number Generator for non-uniform distributions" 
               Authors : Weili Chen, Zixuan Ma                      
-------------------------------------------------------------------------------
*/

#include "vpgrandom.h"


//-----------------------------------------------------------------------------
VpgRandom::VpgRandom(int seed)
{
    srand(seed);
}

//-----------------------------------------------------------------------------
float VpgRandom::getNormalValue(float mu, float sigma) const
{
      return (float)(normal(mu,sigma));
}

//-----------------------------------------------------------------------------
float VpgRandom::getUniformValue(float min, float max) const
{
      return (((float)(uniform())) * (max - min) + min);
}


//-----------------------------------------------------------------------------
/* 
 * Generates a Bernoulli random variable 
 * p is the probability of sucess
 * This generator uses the Accept/Reject method
 */
int VpgRandom::bernoulli(double p) {
  double u = uniform();

  if(u < p)
    return 1;
  else 
    return 0;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a Binomial random variable 
 * p is the probability of sucess
 * n is the number of trials
 * This generator uses the Accept/Reject method
 */
int VpgRandom::binomial(double p, int n)
{
  if( n < 0 ) {
    return -1;
  }
  
  double u = uniform();
  double sum = 0;
  int x = 0;
  double temp = C(0, n) * pow(p, 0) * pow(1 - p, n);
  for(; sum < u; x++) {
    sum += temp;
    temp = temp * p / (1 - p) * (n - x) / (x + 1);
  }
  return x - 1;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a Negative-Binomial random variable 
 * p is the probability of sucess
 * r is the (number of successes + 1)
 * This function assumes r is an integer
 * This generator uses the Accept/Reject method
 */
int VpgRandom::negativeBinomial(double p, int r) 
{
  if( r < 1 ) {
    return -1;
  }
  
  double u = uniform();
  double sum = 0;
  int x = 0;
  for(; sum < u; x ++) {
    sum += C(r - 1, x) * pow(p, r) * pow(1 - p, x);;
  }
  return x - 1;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a Poisson random variable 
 * lambda is the mean of the distibution
 * This generator uses the Accept/Reject method
 */
int VpgRandom::poisson(int lambda)
{
  if( lambda < 1 ) {
    return -1;
  }
  int x = 0;
  double u = uniform();
  double sum = 0;
  double temp = exp(-lambda) * pow(lambda, 0) / factor(0);
  for(; sum < u; x++) {
    sum += temp;
    temp = temp * lambda / (x + 1);
  }
  return x - 1;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a Geometric random variable 
 * p is the probability of success
 * This generator uses the Accept/Reject method
 */
int VpgRandom::geometric(double p)
{
  double u = uniform();
  double sum = 0;
  int x = 0;
  for(; sum < u; x ++) {
    sum += p * pow(1 - p, x);
  }
  return x - 1;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a Uniform random variable between 0 and 1
 * This generator is part of the C library
 */
double VpgRandom::uniform() const
{
  int r = rand();
  return (double)r / RAND_MAX;
}

//-----------------------------------------------------------------------------
/* 
 * Generates an Exponential random variable 
 * lambda is the rate parameter
 * This generator uses the Inverse Transform  method
 */
double VpgRandom::exponential(double lambda) const
{
  return -log(uniform()) / lambda;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a two-parameter Weibull random variable 
 * k is the shape parameter
 * lambda is the scale parameter
 * This generator uses the Inverse Transform  method
 */
double VpgRandom::weibull(double k, double lambda)
{
  return pow(-log(uniform()), 1/k) * lambda;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a Normal random variable 
 * mu is the mean of the distribution
 * sigma is the stdev of the distribution
 * This generator uses the Covolution method on N(0,1)
 */
double VpgRandom::normal(double mu, double sigma) const
{
  return sigma * normalhelper() + mu;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a Log-normal random variable 
 * mu is the mean of the log of the variable
 * sigma is the stdev of the log of the variable
 * This generator uses the Convolution method
 */
double VpgRandom::lognormal(double mu, double sigma) {
  return exp(sigma * normalhelper() + mu);
}

//-----------------------------------------------------------------------------
/* 
 * Generates a Chi-squared random variable 
 * dof is the degree of freedom
 * This generator uses the Convolution method
 */
double VpgRandom::chisquare(int dof)
{
  if( dof < 1 ) {
    return 0;
  }
  
  int i = 0;
  double chi = 0;
  for(i = 0; i < dof; i ++) {
    chi += pow(normalhelper(), 2);
  }
  return chi;
}

//-----------------------------------------------------------------------------
/* 
 * Generates a t-distribution random variable 
 * dof is the degree of freedom
 * This generator uses the Convolution method
 */
double VpgRandom::t(int dof) {
  return normalhelper() / sqrt(chisquare(dof) / dof);
}

//-----------------------------------------------------------------------------
/* 
 * Generates a F random variable 
 * m is the degree of freedom of first chi-square distribution
 * n is the degree of freedom of second chi-square distribution
 * This generator uses the Convolution method
 */
double VpgRandom::F(int m, int n){
  return (chisquare(m) / m) /(chisquare(n) / n);
}

//-----------------------------------------------------------------------------
/* 
 * Generates an Erlang random variable 
 * k is the shape parameter
 * rate is the rate parameter
 * This generator uses the Composition method
 */
double VpgRandom::erlang(int k, double rate) {
  
  if (k < 0) return 0;

  int i = 0;
  double erl = 0;
 
  for (i = 0; i < k; i++) {
    erl += -log(uniform());
  }
  
  return erl / rate;
}

//-----------------------------------------------------------------------------

/* 
 * HELPER FUNCTIONS
 * The functions below are helper functions for the random variable generators.
 */


//-----------------------------------------------------------------------------
/* Generates a N(0, 1) variable
 * This generator uses the Accept/Reject method
 */
double VpgRandom::normalhelper() const
{
  double c = sqrt( 2 * M_E / M_PI );
  double t = exponential( 1 );
  
  while( t > (sqrt(2 / M_PI) * exp(t - t * t / 2) / c)) {
    t = exponential( 1 );
  }
  
  if(rand() % 2 == 0 ) t = -1 * t;
  
  return 2 * t;
}

//-----------------------------------------------------------------------------
/* Calculates m permutate n */
int VpgRandom::P(int m, int n)
{
  int ret = 1, i = 0;
  if( m > n || m < 0 || n < 0 ) {
    return 0;
  }
  
  for(i = n; i > n - m; i --) {
    ret *= i;
  }
  return ret;
}

//-----------------------------------------------------------------------------
/* Calculates n factorial */
int VpgRandom::factor(int n)
{
  if( n < 0 ) {
    return 0;
  }
  else if( n == 0 ) {
    return 1;
  }
  else {
    int prod = 1;
    for (int i = 1; i <= n; i++)
      prod = prod * i;

    return prod;
  }
}

//-----------------------------------------------------------------------------
/* Calculates m choose n */
int VpgRandom::C(int m, int n)
{
  //int ret = 1;
  if( m > n ) {
    return 0;
  }
  
  return P(m, n) / factor(m);
}

//-----------------------------------------------------------------------------
