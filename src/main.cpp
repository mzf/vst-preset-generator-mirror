/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: main.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: main file
-------------------------------------------------------------------------------
*/


#include <QApplication>
#include <QTranslator>

#include "maindialog.h"
#include "vpglanguagechooser.h"
#include "vpgsettings.h"


int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(vpg);

    QApplication app(argc, argv);

    VpgSettings vpgSettings;
    VpgLanguageChooser vpgLanguageChooser;

    //if language is not set
    if(!vpgSettings.hasTranslationFilePath())
    {
        if(vpgLanguageChooser.exec() == VpgLanguageChooser::RETURN_CODE_LANGUAGE_CHOOSEN)
        {
            vpgSettings.setTranslationFilePath(vpgLanguageChooser.getChoosenLanguageFilePath());
        }
    }

    QTranslator translator(qApp);
    translator.load(vpgSettings.getTranslationFilePath());
    qApp->installTranslator(&translator);

    MainDialog mainDialog(vpgSettings);
    mainDialog.show();

    return app.exec();
}
