/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstlistview.h
  Author: Francois Mazen
  Date: 27/02/17 18:25
  Description: header file of the VSTListView class
-------------------------------------------------------------------------------
*/

#ifndef VSTLISTVIEW_H
#define VSTLISTVIEW_H

#include <QTreeView>

class VSTPreset;

class VSTListView : public QTreeView
{
    Q_OBJECT
public:
    VSTListView(QWidget *parent);
    virtual ~VSTListView();

    void selectPresetItem(const QSharedPointer<VSTPreset> &vstPreset);


signals:
    void currentIndexHasChanged(const QModelIndex& currentIndex);
    void addNewPresetActionTriggered();
    void removePluginActionTriggered();
    void removePresetActionTriggered();

protected:
    virtual void currentChanged(const QModelIndex & current, const QModelIndex & previous) Q_DECL_OVERRIDE;
    virtual void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void contextMenuEvent(QContextMenuEvent *event) Q_DECL_OVERRIDE;

private slots:
    void editSelectedItem();
};

#endif // VSTLISTVIEW_H
