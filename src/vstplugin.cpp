/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgfile.h
  Author: Francois Mazen
  Date: 20/02/17 19:57
  Description: VST Plugin abstraction.
-------------------------------------------------------------------------------
*/

#include "vstplugin.h"
#include "vstsdk.h"

// A define for string memory allocation.
#define NAME_LENGTH 64

//-----------------------------------------------------------------------------
VSTPlugin::VSTPlugin(const QString& filePath, AEffect* audioEffect) :
    filePath(filePath),
    audioEffect(audioEffect)
{
    Q_ASSERT(audioEffect != NULL);
}

//-----------------------------------------------------------------------------
void VSTPlugin::sendCloseEvent()
{
    if(audioEffect)
    {
        audioEffect->dispatcher(audioEffect, effClose, 0, 0, 0, 0);
        audioEffect = NULL;
    }
}

//-----------------------------------------------------------------------------
int32_t VSTPlugin::getFxId() const
{
    int32_t fxId = 0;
    if(audioEffect != NULL)
    {
        fxId = audioEffect->uniqueID;
    }
    return fxId;
}

//-----------------------------------------------------------------------------
int32_t VSTPlugin::getFxVersion() const
{
    int32_t fxVersion = 0;
    if(audioEffect != NULL)
    {
        fxVersion = audioEffect->dispatcher(audioEffect, effGetVendorVersion, 0, 0, 0, 0);
    }
    return fxVersion;
}

//-----------------------------------------------------------------------------
int32_t VSTPlugin::getNumberParams() const
{
    int32_t numberParams = 0;
    if(audioEffect != NULL)
    {
        numberParams = audioEffect->numParams;
    }
    return numberParams;
}

//-----------------------------------------------------------------------------
int32_t VSTPlugin::getNumberPrograms() const
{
    int32_t numberPrograms = 0;
    if(audioEffect != NULL)
    {
        numberPrograms = audioEffect->numPrograms;
    }
    return numberPrograms;
}

//-----------------------------------------------------------------------------
QString VSTPlugin::getName() const
{
    QString name;
    if (audioEffect != NULL)
    {
        //The buffer length is normaly kVstMaxEffectNameLen (= 32 chars)
        // but some plugin set longer names.
        char buffer[NAME_LENGTH + 1];
        for(size_t charIndex = 0; charIndex <= NAME_LENGTH; ++charIndex)
        {
            buffer[charIndex] = '\0';
        }

        audioEffect->dispatcher(audioEffect,effGetEffectName,0,0,buffer,0.0f);

        buffer[NAME_LENGTH] = '\0';
        name = buffer;
    }
    return name;
}

//-----------------------------------------------------------------------------
QString VSTPlugin::getParameterName(int32_t index) const
{
    QString name;

    if (audioEffect != NULL)
    {
        //effGetParamName,
        ///< [ptr]: char buffer for parameter name, limited to #kVstMaxParamStrLen
        //@see AudioEffect::getParameterName

        //The buffer length is normaly kVstMaxParamStrLen (= 8 chars)
        // but some plugin set longer names.
        char buffer[NAME_LENGTH + 1];
        for(size_t charIndex = 0; charIndex <= NAME_LENGTH; ++charIndex)
        {
            buffer[charIndex] = '\0';
        }

        audioEffect->dispatcher(audioEffect,effGetParamName,index,0,buffer,0.0f);

        buffer[NAME_LENGTH] = '\0';
        name = buffer;
    }
    return name;

}

//-----------------------------------------------------------------------------
QString VSTPlugin::getParameterDisplay(int32_t index) const
{
    QString name;

    if (audioEffect != NULL)
    {
        // The buffer length is normaly kVstMaxParamStrLen (= 8 chars)
        char buffer[NAME_LENGTH + 1];
        memset(buffer, '\0', NAME_LENGTH + 1);
        audioEffect->dispatcher(audioEffect,effGetParamDisplay,index,0,buffer,0.0f);
        buffer[NAME_LENGTH] = '\0';
        name = buffer;

        // Append parameter label.
        memset(buffer, '\0', NAME_LENGTH + 1);
        audioEffect->dispatcher(audioEffect,effGetParamLabel,index,0,buffer,0.0f);
        buffer[NAME_LENGTH] = '\0';

        QString label = buffer;
        if(!label.isEmpty())
        {
            name += " ";
            name += label;
        }
    }
    return name;
}


//-----------------------------------------------------------------------------
bool VSTPlugin::useChunk() const
{
    bool isChunkUsed = false;
    if (audioEffect != NULL && (audioEffect->flags & effFlagsProgramChunks) != 0)
    {
        isChunkUsed = true;
    }
    return isChunkUsed;
}

//-----------------------------------------------------------------------------
const QString &VSTPlugin::getFilePath() const
{
    return filePath;
}

//-----------------------------------------------------------------------------
void VSTPlugin::setParameter(long index, float value) const
{
    if (audioEffect != NULL)
    {
        audioEffect->setParameter(audioEffect, index, value);
    }
}

//-----------------------------------------------------------------------------
float VSTPlugin::getParameterValue(long index) const
{
    float value = 0.f;
    if (audioEffect != NULL)
    {
        value = audioEffect->getParameter(audioEffect, index);
    }

    return value;
}

//-----------------------------------------------------------------------------
int32_t VSTPlugin::getProgramChunk(void** chunk) const
{
    int32_t chunkSize = 0;

    if (audioEffect != NULL)
    {
        ///effGetChunk:
        ///  [ptr]: void** for chunk data address
        ///  [index]: 0 for bank, 1 for program  @see AudioEffect::getChunk

        //(AEffect* effect, VstInt32 opcode, VstInt32 index, VstIntPtr value, void* ptr, float opt)
        chunkSize = audioEffect->dispatcher(audioEffect,effGetChunk,1,0,chunk,0.0f);
    }

    return chunkSize;
}

//-----------------------------------------------------------------------------
int32_t VSTPlugin::getBankChunk(void** chunk) const
{
    int32_t chunkSize = 0;

    if (audioEffect != NULL)
    {
        ///effGetChunk:
        ///  [ptr]: void** for chunk data address
        ///  [index]: 0 for bank, 1 for program  @see AudioEffect::getChunk

        //(AEffect* effect, VstInt32 opcode, VstInt32 index, VstIntPtr value, void* ptr, float opt)
        chunkSize = audioEffect->dispatcher(audioEffect,effGetChunk,0,0,chunk,0.0f);
    }

    return chunkSize;
}

//-----------------------------------------------------------------------------
void VSTPlugin::setProgramChunk(void *chunk, int32_t chunkSize) const
{
    if(chunk != NULL && chunkSize > 0)
    {
        audioEffect->dispatcher(audioEffect, effSetChunk, 1, chunkSize, chunk, 0.f);
    }
}

//-----------------------------------------------------------------------------
void VSTPlugin::setBankChunk(void *chunk, int32_t chunkSize) const
{
    if(chunk != NULL && chunkSize > 0)
    {
        audioEffect->dispatcher(audioEffect, effSetChunk, 0, chunkSize, chunk, 0.f);
    }
}

//-----------------------------------------------------------------------------
void VSTPlugin::setCurrentProgramName(const char* name) const
{
    if (audioEffect != NULL)
    {
        audioEffect->dispatcher(audioEffect,effSetProgramName,0,0,(void*)name,0.0f);
    }
}

//-----------------------------------------------------------------------------
QString VSTPlugin::getCurrentProgramName() const
{
    QString name;
    if (audioEffect != NULL)
    {
        char buffer[NAME_LENGTH + 1];
        memset(buffer, '\0', NAME_LENGTH + 1);
        audioEffect->dispatcher(audioEffect,effGetProgramName,0,0,(void*)buffer,0.0f);
        buffer[NAME_LENGTH] = '\0';
        name = buffer;
    }

    return name;
}

//-----------------------------------------------------------------------------
void VSTPlugin::setProgram(long num) const
{
    if (audioEffect != NULL)
    {
        audioEffect->dispatcher(audioEffect,effSetProgram,0,num,0,0.0f);
    }
}
