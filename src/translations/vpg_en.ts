<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>FileTypeDialog</name>
    <message>
        <location filename="../filetypedialog.cpp" line="34"/>
        <location filename="../filetypedialog.cpp" line="36"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="37"/>
        <source>Number of programs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="39"/>
        <source>program preset file (.fxp)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="40"/>
        <source>bank preset file (.fxb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="41"/>
        <source>&amp;Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="42"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <location filename="../maindialog.cpp" line="101"/>
        <source>&amp;VST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="250"/>
        <source>Set &amp;uniform random method to all parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="253"/>
        <source>Set &amp;normal random method to all parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="256"/>
        <source>Set &amp;constant values to all parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="259"/>
        <source>Set &amp;total random values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="97"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="99"/>
        <source>Exit the application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="111"/>
        <source>&amp;About VST Preset Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="112"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="113"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="125"/>
        <source>VST Plugin List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="210"/>
        <source>&lt;h2&gt;Welcome to VST Preset Generator!&lt;/h2&gt;

Please click an action below to start:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="242"/>
        <source>&amp;Generate a preset file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="265"/>
        <source>Random methods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="354"/>
        <source>Unknown error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="358"/>
        <source>Could not read the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="361"/>
        <source>This is not a valid VST preset file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="370"/>
        <source>End of file reached. The file is incomplete.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="377"/>
        <source>Opening Preset File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="364"/>
        <source>Could not find the plugin, please load the plugin first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="367"/>
        <source>Wrong version of the plugin, please load the corresponding plugin version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="510"/>
        <location filename="../maindialog.cpp" line="556"/>
        <source>Save VST Plugin List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="527"/>
        <source>Error opening file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="530"/>
        <source>Can&apos;t write the vpg file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Opaque Chunk method : please import informations from the VST file (.dll)</source>
        <translation type="obsolete">Opaque Chunk method: please import informations from the VST file (.dll)</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="438"/>
        <source>Generate Preset Bank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="440"/>
        <source>Preset Bank (*.fxb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="445"/>
        <source>Generate Preset Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="447"/>
        <source>Preset Program (*.fxp)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="485"/>
        <source>The preset file:
%1
has been created. Enjoy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="486"/>
        <location filename="../maindialog.cpp" line="490"/>
        <source>Saving Preset File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="491"/>
        <source>Could not create the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="495"/>
        <source>Generating Preset File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="534"/>
        <source>Saving VST Plugin List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="557"/>
        <source>Do you want to save current VST Plugin list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="584"/>
        <source>

Original file path:
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="714"/>
        <source>Open VST Plugin List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="749"/>
        <source>Opening VST Plugin List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="806"/>
        <source>&lt;p&gt;English translation: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="828"/>
        <source>Closing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="829"/>
        <source>Do you want to save changes you made to your VST list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="793"/>
        <source>About the VST Preset Generator </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="795"/>
        <source>&lt;h2&gt;VST Preset Generator </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save your actual Vst list before close the application ?</source>
        <translation type="obsolete">Save your actual Vst list before close the application?</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="940"/>
        <source>Error: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="944"/>
        <source>VST Plugin main entry not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="948"/>
        <source>failed to create effect instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="952"/>
        <source>it&apos;s not a valid VST plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="961"/>
        <source>Loading plugin VST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1013"/>
        <source>Remove Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1014"/>
        <source>Are you sure you want to remove the plugin named &quot;%1&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1040"/>
        <source>Remove Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1041"/>
        <source>Are you sure you want to remove the preset named &quot;%1&quot;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1058"/>
        <source>Setting a random method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1059"/>
        <source>If you change the random method of all parameters, your old settings will be lost.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../vpglanguagechooser.cpp" line="110"/>
        <source>Language</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="807"/>
        <source>Translator</source>
        <translation>François Mazen</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="174"/>
        <source>VST Preset Generator V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="91"/>
        <source>Load &amp;VST Plugin...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="92"/>
        <source>&amp;Open VST Plugin List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="93"/>
        <source>&amp;Save VST Plugin List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="94"/>
        <source>&amp;New VST Plugin List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="496"/>
        <source>Can&apos;t generate Bank file for this VST plug-in.
Please try with a Program file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="512"/>
        <location filename="../maindialog.cpp" line="716"/>
        <source>VST Preset Generator file (*.vpg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="581"/>
        <source>The file path for plugin named &quot;%1&quot; is not valid. Please select the plugin file in the following dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="587"/>
        <source>Invalid plugin file path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="603"/>
        <source>VST Plugin (*.dll)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="604"/>
        <source>Linux VST Plugin (*.so)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="618"/>
        <source>Load a VST Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="628"/>
        <source>Preset File (*.fxp *.fxb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="635"/>
        <source>Load a VST Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="688"/>
        <location filename="../maindialog.cpp" line="694"/>
        <location filename="../maindialog.cpp" line="699"/>
        <source>Loading VPG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="688"/>
        <source>The parameter count of the preset named &quot;%1&quot; does not match (plugin &quot;%2&quot;). It won&apos;t be added to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="694"/>
        <source>The ID of the plugin named &quot;%1&quot; does not match. It won&apos;t be added to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="699"/>
        <source>Could not load the plugin named &quot;%1&quot;. It won&apos;t be added to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="732"/>
        <source>Could not open the file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="735"/>
        <source>Could not parse the file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="738"/>
        <source>The file is not a valid vpg file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="741"/>
        <source>Unknown XML tag in file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="744"/>
        <source>Invalid XML element in file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="797"/>
        <source>&lt;/h2&gt;&lt;p&gt;Copyright &amp;copy; 2007-2017 Fran&amp;ccedil;ois Mazen&lt;p&gt;Please visit official website for more informations:&lt;p&gt;&lt;a href=&apos;http://vst-preset-generator.org/&apos;&gt;http://vst-preset-generator.org/&lt;/a&gt;&lt;p&gt;This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;p&gt;You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.&lt;p&gt;VST is a registered trademark of Steinberg Soft- und Hardware, GmbH.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="880"/>
        <source>New </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="957"/>
        <source>failed to load VST Plugin library
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="805"/>
        <source>&lt;p&gt;Special thanks to: Selfik, AzralXt, Spacedad, and... YOU for using this tool!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="605"/>
        <location filename="../maindialog.cpp" line="629"/>
        <source>Any File (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;Translation in English: </source>
        <translation type="obsolete">&lt;p&gt;Original texts: </translation>
    </message>
</context>
<context>
    <name>ParameterTableModel</name>
    <message>
        <location filename="../parametertablemodel.cpp" line="83"/>
        <source>uniform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="87"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="91"/>
        <source>constant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="100"/>
        <source>min: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="104"/>
        <source>mid: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="108"/>
        <source>val: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="117"/>
        <source>max: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="123"/>
        <source>dev: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="199"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="202"/>
        <source>random mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="205"/>
        <source>val 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="208"/>
        <source>val 2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PluginPage</name>
    <message>
        <location filename="../pluginpage.cpp" line="49"/>
        <source>Plugin Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="55"/>
        <source>Add new random preset for this plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="58"/>
        <source>Remove this plugin from the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="61"/>
        <source>Load Preset...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="135"/>
        <source>No plug-in loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="141"/>
        <source>name: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="144"/>
        <source>VST ID: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="149"/>
        <source>version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="154"/>
        <source>number of parameters: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="166"/>
        <source>preset type: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="169"/>
        <source>file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="160"/>
        <source>chunk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="164"/>
        <source>parameters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RandomModeDelegate</name>
    <message>
        <location filename="../randommodedelegate.cpp" line="43"/>
        <source>uniform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../randommodedelegate.cpp" line="44"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../randommodedelegate.cpp" line="45"/>
        <source>constant</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VSTListView</name>
    <message>
        <location filename="../vstlistview.cpp" line="93"/>
        <source>Add new random preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../vstlistview.cpp" line="96"/>
        <source>Remove plugin &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../vstlistview.cpp" line="106"/>
        <source>Rename...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../vstlistview.cpp" line="109"/>
        <source>Remove preset &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VSTPreset</name>
    <message>
        <location filename="../vstpreset.cpp" line="55"/>
        <source>default %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VpgLanguageChooser</name>
    <message>
        <location filename="../vpglanguagechooser.cpp" line="36"/>
        <source>Please choose your language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../vpglanguagechooser.cpp" line="56"/>
        <source>Language Chooser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
