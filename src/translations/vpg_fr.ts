<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>FileTypeDialog</name>
    <message>
        <source>File type</source>
        <translation type="obsolete">Type de fichier</translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="34"/>
        <location filename="../filetypedialog.cpp" line="36"/>
        <source>File Type</source>
        <translation>Type de fichier</translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="37"/>
        <source>Number of programs:</source>
        <translation>Nombre de programmes :</translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="39"/>
        <source>program preset file (.fxp)</source>
        <translation>preset simple de type &quot;program&quot; (.fxp)</translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="40"/>
        <source>bank preset file (.fxb)</source>
        <translation>preset de type &quot;bank&quot;, contenant plusieurs programmes (.fxb)</translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="41"/>
        <source>&amp;Next</source>
        <translation>&amp;Suivant</translation>
    </message>
    <message>
        <location filename="../filetypedialog.cpp" line="42"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>&amp;Generate a preset file !</source>
        <translation type="obsolete">&amp;Générer un fichier de presets !</translation>
    </message>
    <message>
        <source>No header loaded</source>
        <translation type="obsolete">Aucune information chargée</translation>
    </message>
    <message>
        <source>VST List</source>
        <translation type="obsolete">Liste de VST</translation>
    </message>
    <message>
        <source>Import informations about &amp;VST Plugin</source>
        <translation type="obsolete">Importer les informations d&apos;un plugin &amp;VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="101"/>
        <source>&amp;VST</source>
        <translation>&amp;VST</translation>
    </message>
    <message>
        <source>&amp;Open VST List...</source>
        <translation type="obsolete">&amp;Ouvrir une liste...</translation>
    </message>
    <message>
        <source>&amp;Save VST List...</source>
        <translation type="obsolete">&amp;Sauvegarder la liste...</translation>
    </message>
    <message>
        <source>&amp;New VST List</source>
        <translation type="obsolete">&amp;Nouvelle liste de VST</translation>
    </message>
    <message>
        <source>&amp;VST List</source>
        <translation type="obsolete">&amp;Liste de VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="250"/>
        <source>Set &amp;uniform random method to all parameters</source>
        <translation>Génération aléatoire &amp;uniforme à tous</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="253"/>
        <source>Set &amp;normal random method to all parameters</source>
        <translation>Génération aléatoire &amp;normale à tous</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="256"/>
        <source>Set &amp;constant values to all parameters</source>
        <translation>Valeurs &amp;constantes à tous</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="259"/>
        <source>Set &amp;total random values</source>
        <translation>&amp;Valeurs aléatoires à tous</translation>
    </message>
    <message>
        <source>&amp;Random methods</source>
        <translation type="obsolete">&amp;Méthodes aléatoires</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="111"/>
        <source>&amp;About VST Preset Generator</source>
        <translation>&amp;A propos de VST Preset Generator</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="112"/>
        <source>About &amp;Qt</source>
        <translation>A propos de &amp;Qt</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="113"/>
        <source>&amp;About</source>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <source>Vst list</source>
        <translation type="obsolete">Liste des VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="242"/>
        <source>&amp;Generate a preset file!</source>
        <translation>&amp;Générer un fichier de presets !</translation>
    </message>
    <message>
        <source>Import Vst</source>
        <translation type="obsolete">Importer un VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="265"/>
        <source>Random methods</source>
        <translation>Méthodes aléatoires</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="377"/>
        <source>Opening Preset File</source>
        <translation>Ouvrir un preset</translation>
    </message>
    <message>
        <source>Could not read the file</source>
        <translation type="obsolete">Impossible de lire le fichier</translation>
    </message>
    <message>
        <source>This is not a valid Vst preset file</source>
        <translation type="obsolete">Ce n&apos;est pas un fichier de preset valide</translation>
    </message>
    <message>
        <source>Loading Preset File</source>
        <translation type="obsolete">Chargement d&apos;un fichier de preset</translation>
    </message>
    <message>
        <source>Opaque Chunk method : please import informations from the VST file (.dll)</source>
        <translation type="obsolete">Méthode &quot;opaque chunk&quot; : veuillez importer les informations directement à partir du fichier VST (.dll)</translation>
    </message>
    <message>
        <source>Name of the Vst</source>
        <translation type="obsolete">Nom du VST</translation>
    </message>
    <message>
        <source>Please enter the name of your Vst:</source>
        <translation type="obsolete">Veuillez entrer le nom du VST :</translation>
    </message>
    <message>
        <source>My Vst</source>
        <translation type="obsolete">Mon VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="97"/>
        <source>E&amp;xit</source>
        <translation>S&amp;ortir</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="99"/>
        <source>Exit the application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="125"/>
        <source>VST Plugin List</source>
        <translation>Liste de plugin VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="210"/>
        <source>&lt;h2&gt;Welcome to VST Preset Generator!&lt;/h2&gt;

Please click an action below to start:</source>
        <translation>&lt;h2&gt;Bienvenu dans VST Preset Generator !&lt;/h2&gt;

Veuillez cliquer sur une action ci-dessous pour commencer :</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="354"/>
        <source>Unknown error!</source>
        <translation>Erreur inconnue !</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="358"/>
        <source>Could not read the file.</source>
        <translation>Impossible de lire le fichier.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="361"/>
        <source>This is not a valid VST preset file.</source>
        <translation>Ce n&apos;est pas un fichier de preset VST valide.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="364"/>
        <source>Could not find the plugin, please load the plugin first.</source>
        <translation>Impossible de trouver le plugin. Veuillez d&apos;abord charger le plugin.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="367"/>
        <source>Wrong version of the plugin, please load the corresponding plugin version.</source>
        <translation>Mauvaise version du plugin. Veuillez charger la version du plugin qui correspond au fichier.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="370"/>
        <source>End of file reached. The file is incomplete.</source>
        <translation>Fin du fichier atteinte. Le fichier semble incomplet.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="438"/>
        <source>Generate Preset Bank</source>
        <translation>Générer un preset de type &quot;bank&quot;</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="440"/>
        <source>Preset Bank (*.fxb)</source>
        <translation>Preset Bank (*.fxb)</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="445"/>
        <source>Generate Preset Program</source>
        <translation>Générer un preset de type &quot;program&quot;</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="447"/>
        <source>Preset Program (*.fxp)</source>
        <translation>Preset Program (*.fxp)</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="485"/>
        <source>The preset file:
%1
has been created. Enjoy!</source>
        <translation>Le fichier de preset :
%1
a été généré. Bonne musique !</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="486"/>
        <location filename="../maindialog.cpp" line="490"/>
        <source>Saving Preset File</source>
        <translation>Sauvegarder le fichier</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="491"/>
        <source>Could not create the file</source>
        <translation>Impossible de créer le fichier</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="495"/>
        <source>Generating Preset File</source>
        <translation>Génération du fichier</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="510"/>
        <location filename="../maindialog.cpp" line="556"/>
        <source>Save VST Plugin List...</source>
        <translation>Enregistrer la liste de plugin VST...</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="527"/>
        <source>Error opening file: %1</source>
        <translation>Impossible d&apos;ouvrir le fichier : %1</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="530"/>
        <source>Can&apos;t write the vpg file.</source>
        <translation>Impossible d&apos;écrire le fichier vpg.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="534"/>
        <source>Saving VST Plugin List</source>
        <translation>Enregistrement de la liste de plugin VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="557"/>
        <source>Do you want to save current VST Plugin list?</source>
        <translation>Voulez-vous enregistrer la liste de plugin VST ?</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="584"/>
        <source>

Original file path:
%1</source>
        <translation>

Chemin originel du fichier :
%1</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="880"/>
        <source>New </source>
        <translation>Nouveau </translation>
    </message>
    <message>
        <source>&lt;/h2&gt;&lt;p&gt;Copyright &amp;copy; 2007-2016 Fran&amp;ccedil;ois Mazen&lt;p&gt;Please visit official website for more informations:&lt;p&gt;&lt;a href=&apos;http://vst-preset-generator.org/&apos;&gt;http://vst-preset-generator.org/&lt;/a&gt;&lt;p&gt;This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;p&gt;You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.&lt;p&gt;VST is a registered trademark of Steinberg Soft- und Hardware, GmbH.</source>
        <translation type="obsolete">&lt;/h2&gt;&lt;p&gt;Copyright &amp;copy; 2007-2016 Fran&amp;ccedil;ois Mazen&lt;p&gt;Consultez le site web officiel pour plus d&apos;informations :&lt;p&gt;&lt;a href=&apos;http://vst-preset-generator.org/&apos;&gt;http://vst-preset-generator.org/&lt;/a&gt;&lt;p&gt;This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;p&gt;You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.&lt;p&gt;VST is a registered trademark of Steinberg Soft- und Hardware, GmbH.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="806"/>
        <source>&lt;p&gt;English translation: </source>
        <translation>&lt;p&gt;Traduction en Français : </translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="828"/>
        <source>Closing...</source>
        <translation>Fermeture...</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="829"/>
        <source>Do you want to save changes you made to your VST list?</source>
        <translation>Voulez-vous sauvegarder les changements dans votre liste de VST ?</translation>
    </message>
    <message>
        <source>VST File (*.dll *.fxp *.fxb);;Linux VST File (*.so);;Any File (*.*)</source>
        <translation type="obsolete">Plugin VST (*.dll *.fxp *.fxb);;Plugin VST Linux (*.so);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <source>Please load the VST file (.dll) corresponding to the specified choice</source>
        <translation type="obsolete">Veuillez charger le fichier VST correspondant à votre choix (fichier dll)</translation>
    </message>
    <message>
        <source>Load a Plugin VST</source>
        <translation type="obsolete">Charger un plugin VST</translation>
    </message>
    <message>
        <source>the VST file (.dll) don&apos;t have the same ID has the specified choice
Please try again.</source>
        <translation type="obsolete">le fichier VST (.dll) n&apos;a pas le même identifiant interne que votre choix
Veuillez en choisir un autre.</translation>
    </message>
    <message>
        <source>The preset file:
</source>
        <translation type="obsolete">Le fichier de presets :
</translation>
    </message>
    <message>
        <source>
has been created. Enjoy !</source>
        <translation type="obsolete">
a été créé avec succès !</translation>
    </message>
    <message>
        <source>Vst name: </source>
        <translation type="obsolete">Nom : </translation>
    </message>
    <message>
        <source>
Vst ID: </source>
        <translation type="obsolete">
Id : </translation>
    </message>
    <message>
        <source>
Vst Version: </source>
        <translation type="obsolete">
Version : </translation>
    </message>
    <message>
        <source>
number of parameters: </source>
        <translation type="obsolete">
nombre de paramètres : </translation>
    </message>
    <message>
        <source>
preset type: </source>
        <translation type="obsolete">
type de preset : </translation>
    </message>
    <message>
        <source>chunk</source>
        <translation type="obsolete">chunk</translation>
    </message>
    <message>
        <source>parameters</source>
        <translation type="obsolete">paramètres</translation>
    </message>
    <message>
        <source>unknow</source>
        <translation type="obsolete">inconnu</translation>
    </message>
    <message>
        <source>Save Vst List</source>
        <translation type="obsolete">Sauvegarder la liste</translation>
    </message>
    <message>
        <source>Vst Preset Generator file (*.vpg)</source>
        <translation type="obsolete">Fichier VST Preset Generator (*.vpg)</translation>
    </message>
    <message>
        <source>Open Vst List</source>
        <translation type="obsolete">Ouvrir une liste</translation>
    </message>
    <message>
        <source>New Vst List...</source>
        <translation type="obsolete">Nouvelle liste...</translation>
    </message>
    <message>
        <source>Save your actual Vst list before create a new one ?</source>
        <translation type="obsolete">Voulez-vous sauvegarder la liste de VST actuelle avant d&apos;en créer une nouvelle ?</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="793"/>
        <source>About the VST Preset Generator </source>
        <translation>A propos de VST Preset Generator </translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="795"/>
        <source>&lt;h2&gt;VST Preset Generator </source>
        <translation>&lt;h2&gt;VST Preset Generator </translation>
    </message>
    <message>
        <source>Close Program...</source>
        <translation type="obsolete">Fermer le programme...</translation>
    </message>
    <message>
        <source>Save your actual Vst list before close the application ?</source>
        <translation type="obsolete">Sauvegarder la liste actuelle de VST avant de fermer l&apos;application ?</translation>
    </message>
    <message>
        <source>VST File (*.dll *.fxp *.fxb)</source>
        <translation type="obsolete">Fichier VST (*.dll *.fxp *.fxb)</translation>
    </message>
    <message>
        <source>Linux VST File (*.so)</source>
        <translation type="obsolete">Fichier VST Linux (*.so)</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="605"/>
        <location filename="../maindialog.cpp" line="629"/>
        <source>Any File (*.*)</source>
        <translation>Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="714"/>
        <source>Open VST Plugin List</source>
        <translation>Ouvrir une liste de plugin VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="732"/>
        <source>Could not open the file: %1</source>
        <translation>Impossible d&apos;ouvrir le fichier : %1</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="735"/>
        <source>Could not parse the file: %1</source>
        <translation>Impossible d&apos;analyser le fichier : %1</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="738"/>
        <source>The file is not a valid vpg file: %1</source>
        <translation>Le fichier &quot;%1&quot; n&apos;est pas un fichier VPG valide</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="741"/>
        <source>Unknown XML tag in file: %1</source>
        <translation>Label XML inconnu dans le fichier : %1</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="744"/>
        <source>Invalid XML element in file: %1</source>
        <translation>Elément XML invalide dans le fichier : %1</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="749"/>
        <source>Opening VST Plugin List</source>
        <translation>Ouverture de liste de plugin VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="797"/>
        <source>&lt;/h2&gt;&lt;p&gt;Copyright &amp;copy; 2007-2017 Fran&amp;ccedil;ois Mazen&lt;p&gt;Please visit official website for more informations:&lt;p&gt;&lt;a href=&apos;http://vst-preset-generator.org/&apos;&gt;http://vst-preset-generator.org/&lt;/a&gt;&lt;p&gt;This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;p&gt;You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.&lt;p&gt;VST is a registered trademark of Steinberg Soft- und Hardware, GmbH.</source>
        <translation>&lt;/h2&gt;&lt;p&gt;Copyright &amp;copy; 2007-2017 Fran&amp;ccedil;ois Mazen&lt;p&gt;Consultez le site web officiel pour plus d&apos;informations :&lt;p&gt;&lt;a href=&apos;http://vst-preset-generator.org/&apos;&gt;http://vst-preset-generator.org/&lt;/a&gt;&lt;p&gt;This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;p&gt;You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.&lt;p&gt;VST is a registered trademark of Steinberg Soft- und Hardware, GmbH.</translation>
    </message>
    <message>
        <source>New Random Preset </source>
        <translation type="vanished">Nouveau preset aléatoire</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="940"/>
        <source>Error: </source>
        <translation>Erreur : </translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="944"/>
        <source>VST Plugin main entry not found</source>
        <translation>La fonction d&apos;entrée du plugin VST n&apos;a pas été trouvée</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="948"/>
        <source>failed to create effect instance</source>
        <translation>impossible de créer une instance du plugin</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="952"/>
        <source>it&apos;s not a valid VST plugin</source>
        <translation>ce n&apos;est pas un plugin VST valide</translation>
    </message>
    <message>
        <source>failed to load VST Plugin library</source>
        <translation type="obsolete">impossible de charger le plugin VST</translation>
    </message>
    <message>
        <source>
</source>
        <translation type="obsolete">
</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="961"/>
        <source>Loading plugin VST</source>
        <translation>Chargement d&apos;un plugin VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1013"/>
        <source>Remove Plugin</source>
        <translation>Suppression du plugin</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1014"/>
        <source>Are you sure you want to remove the plugin named &quot;%1&quot;?</source>
        <translation>Êtes vous sûr de vouloir supprimer le plugin &quot;%1&quot; ?</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1040"/>
        <source>Remove Preset</source>
        <translation>Supprimer le preset</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1041"/>
        <source>Are you sure you want to remove the preset named &quot;%1&quot;?</source>
        <translation>Êtes vous sûr de vouloir supprimer le preset &quot;%1&quot; ?</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1058"/>
        <source>Setting a random method</source>
        <translation>Assigner une méthode de génération aléatoire</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="1059"/>
        <source>If you change the random method of all parameters, your old settings will be lost.
Do you want to continue?</source>
        <translation>Si vous changez la méthode de génération aléatoire de tous les paramètres, vos anciens réglages seront perdu.
Voulez vous continuer ?</translation>
    </message>
    <message>
        <location filename="../vpglanguagechooser.cpp" line="110"/>
        <source>Language</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="807"/>
        <source>Translator</source>
        <translation>François Mazen</translation>
    </message>
    <message>
        <source>Import VST</source>
        <translation type="obsolete">Importer un VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="174"/>
        <source>VST Preset Generator V</source>
        <translation>VST Preset Generator V</translation>
    </message>
    <message>
        <source>This is not a valid VST preset file</source>
        <translation type="obsolete">Ce n&apos;est pas un fichier de preset valide</translation>
    </message>
    <message>
        <source>Name of the VST</source>
        <translation type="obsolete">Nom du VST</translation>
    </message>
    <message>
        <source>Please enter the name of your VST:</source>
        <translation type="obsolete">Veuillez entrer le nom du VST :</translation>
    </message>
    <message>
        <source>My VST</source>
        <translation type="obsolete">Mon VST</translation>
    </message>
    <message>
        <source>Invalid Name</source>
        <translation type="obsolete">Nom invalide</translation>
    </message>
    <message>
        <source>Please load the VST plugin file related to the selected preset.</source>
        <translation type="obsolete">Veuillez charger le fichier VST correspondant à votre choix (fichier dll).</translation>
    </message>
    <message>
        <source>VST Plugin (*.dll);;Linux VST Plugin (*.so)</source>
        <translation type="obsolete">Plugin VST (*.dll);;Plugin VST Linux (*.so)</translation>
    </message>
    <message>
        <source>The VST plugin file doesn&apos;t have the same ID has the selected preset.
Please try again.</source>
        <translation type="obsolete">Le fichier VST selectionné n&apos;a pas le même identifiant interne que votre choix.
Veuillez en choisir un autre.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="496"/>
        <source>Can&apos;t generate Bank file for this VST plug-in.
Please try with a Program file.</source>
        <translation>Impossible de générer un preset &quot;Bank&quot; pour ce VST.
Veuillez essayer avec un preset de type &quot;Program&quot;.</translation>
    </message>
    <message>
        <source>
has been created. Enjoy!</source>
        <translation type="obsolete">
a été créé avec succès !</translation>
    </message>
    <message>
        <source>VST name: </source>
        <translation type="obsolete">Nom : </translation>
    </message>
    <message>
        <source>
VST ID: </source>
        <translation type="obsolete">
Id : </translation>
    </message>
    <message>
        <source>
VST Version: </source>
        <translation type="obsolete">
Version : </translation>
    </message>
    <message>
        <source>unknown</source>
        <translation type="obsolete">inconnu</translation>
    </message>
    <message>
        <source>Save VST List</source>
        <translation type="obsolete">Sauvegarder la liste</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="512"/>
        <location filename="../maindialog.cpp" line="716"/>
        <source>VST Preset Generator file (*.vpg)</source>
        <translation>Fichier VST Preset Generator (*.vpg)</translation>
    </message>
    <message>
        <source>Error opening file: </source>
        <translation type="obsolete">Erreur à l&apos;ouverture du fichier : </translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="91"/>
        <source>Load &amp;VST Plugin...</source>
        <translation>Charger un plugin &amp;VST...</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="92"/>
        <source>&amp;Open VST Plugin List...</source>
        <translation>&amp;Ouvrir une liste de plugin VST...</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="93"/>
        <source>&amp;Save VST Plugin List...</source>
        <translation>&amp;Enregistrer la liste de plugin VST...</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="94"/>
        <source>&amp;New VST Plugin List</source>
        <translation>&amp;Nouvelle liste de plugin VST</translation>
    </message>
    <message>
        <source>Saving VST List</source>
        <translation type="obsolete">Sauvegarder la liste de VST</translation>
    </message>
    <message>
        <source>Do you want to save current VST list?</source>
        <translation type="obsolete">Voulez-vous enregistrer la liste de plugin VST ?</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="581"/>
        <source>The file path for plugin named &quot;%1&quot; is not valid. Please select the plugin file in the following dialog.</source>
        <translation>Le chemin du fichier pour le plugin &quot;%1&quot; n&apos;est pas valide. Veuillez sélectionner le fichier du plugin dans la fenêtre suivante.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="587"/>
        <source>Invalid plugin file path</source>
        <translation>Chemin du plugin invalide</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="603"/>
        <source>VST Plugin (*.dll)</source>
        <translation>Plugin VST (*.dll)</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="604"/>
        <source>Linux VST Plugin (*.so)</source>
        <translation>Plugin VST Linux (*.so)</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="618"/>
        <source>Load a VST Plugin</source>
        <translation>Charger un plugin VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="628"/>
        <source>Preset File (*.fxp *.fxb)</source>
        <translation>Fichier preset (*.fxp *.fxb)</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="635"/>
        <source>Load a VST Preset</source>
        <translation>Charger un preset VST</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="688"/>
        <location filename="../maindialog.cpp" line="694"/>
        <location filename="../maindialog.cpp" line="699"/>
        <source>Loading VPG File</source>
        <translation>Chargement d&apos;un fichier VPG</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="688"/>
        <source>The parameter count of the preset named &quot;%1&quot; does not match (plugin &quot;%2&quot;). It won&apos;t be added to the list.</source>
        <translation>Le nombre de paramètres du preset &quot;%1&quot; ne correspond pas (plugin &quot;%2&quot;). Le preset ne sera pas ajouté à la liste.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="694"/>
        <source>The ID of the plugin named &quot;%1&quot; does not match. It won&apos;t be added to the list.</source>
        <translation>L&apos;ID du plugin &quot;%1&quot; ne correspond pas. Le preset ne sera pas ajouté à la liste.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="699"/>
        <source>Could not load the plugin named &quot;%1&quot;. It won&apos;t be added to the list.</source>
        <translation>Impossible de charger le plugin &quot;%1&quot;. Il ne sera pas ajouté à la liste.</translation>
    </message>
    <message>
        <source>Open VST List</source>
        <translation type="obsolete">Ouvrir une liste</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="957"/>
        <source>failed to load VST Plugin library
%1</source>
        <translation>impossible de charger le plugin VST
%1</translation>
    </message>
    <message>
        <source>Could not open the file: </source>
        <translation type="obsolete">Impossible d&apos;ouvrir le fichier : </translation>
    </message>
    <message>
        <source>Could not parse the file: </source>
        <translation type="obsolete">Impossible d&apos;analyser le fichier : </translation>
    </message>
    <message>
        <source>The file is not a valid vpg file: </source>
        <translation type="obsolete">Le fichier sélectionné n&apos;est pas valide : </translation>
    </message>
    <message>
        <source>Unknown XML tag in file: </source>
        <translation type="obsolete">Label XML inconnu dans le fichier : </translation>
    </message>
    <message>
        <source>Invalid XML element in file: </source>
        <translation type="obsolete">Elément XML invalide dans le fichier : </translation>
    </message>
    <message>
        <source>Opening VST List</source>
        <translation type="obsolete">Ouvrir une liste de VST</translation>
    </message>
    <message>
        <source>New VST List...</source>
        <translation type="obsolete">Nouvelle liste...</translation>
    </message>
    <message>
        <source>Do you want to save changes to your current VST list?</source>
        <translation type="obsolete">Voulez-vous sauvegarder la liste de VST actuelle avant d&apos;en créer une nouvelle ?</translation>
    </message>
    <message>
        <source>&lt;/h2&gt;&lt;p&gt;Copyright &amp;copy; 2007 Fran&amp;ccedil;ois Mazen&lt;p&gt;Please visit Source Forge website for more informations:&lt;p&gt;&lt;a href=&apos;http://vst-preset-gen.sourceforge.net/&apos;&gt;http://vst-preset-gen.sourceforge.net/&lt;/a&gt;&lt;p&gt;This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;p&gt;You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.&lt;p&gt;VST is a registered trademark of Steinberg Soft- und Hardware, GmbH.</source>
        <translation type="obsolete">&lt;/h2&gt;&lt;p&gt;Copyright &amp;copy; 2007 Fran&amp;ccedil;ois Mazen&lt;p&gt;Please visit Source Forge website for more informations:&lt;p&gt;&lt;a href=&apos;http://vst-preset-gen.sourceforge.net/&apos;&gt;http://vst-preset-gen.sourceforge.net/&lt;/a&gt;&lt;p&gt;This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.&lt;p&gt;This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;p&gt;You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.&lt;p&gt;VST is a registered trademark of Steinberg Soft- und Hardware, GmbH.</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="805"/>
        <source>&lt;p&gt;Special thanks to: Selfik, AzralXt, Spacedad, and... YOU for using this tool!</source>
        <translation>&lt;p&gt;Un merci tout particulier à : Selfik, AzralXt, Spacedad, et... VOUS pour l’utilisation de cet outil !</translation>
    </message>
    <message>
        <source>&lt;p&gt;Translation in English: </source>
        <translation type="obsolete">&lt;p&gt;Traduction en Français : </translation>
    </message>
</context>
<context>
    <name>ParamManager</name>
    <message>
        <source>uniform</source>
        <translation type="obsolete">uniforme</translation>
    </message>
    <message>
        <source>normal</source>
        <translation type="obsolete">normale</translation>
    </message>
    <message>
        <source>constant</source>
        <translation type="obsolete">constante</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="obsolete">nom</translation>
    </message>
    <message>
        <source>random mode</source>
        <translation type="obsolete">méthode aléatoire</translation>
    </message>
    <message>
        <source>val 1</source>
        <translation type="obsolete">val 1</translation>
    </message>
    <message>
        <source>val 2</source>
        <translation type="obsolete">val 2</translation>
    </message>
    <message>
        <source>min: </source>
        <translation type="obsolete">min : </translation>
    </message>
    <message>
        <source>mid: </source>
        <translation type="obsolete">moy : </translation>
    </message>
    <message>
        <source>val: </source>
        <translation type="obsolete">val : </translation>
    </message>
    <message>
        <source>max: </source>
        <translation type="obsolete">max : </translation>
    </message>
    <message>
        <source>dev: </source>
        <translation type="obsolete">écart-type : </translation>
    </message>
</context>
<context>
    <name>ParameterTableModel</name>
    <message>
        <location filename="../parametertablemodel.cpp" line="83"/>
        <source>uniform</source>
        <translation>uniforme</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="87"/>
        <source>normal</source>
        <translation>normale</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="91"/>
        <source>constant</source>
        <translation>constante</translation>
    </message>
    <message>
        <source>min: </source>
        <translation type="obsolete">min : </translation>
    </message>
    <message>
        <source>mid: </source>
        <translation type="obsolete">moy : </translation>
    </message>
    <message>
        <source>val: </source>
        <translation type="obsolete">val : </translation>
    </message>
    <message>
        <source>max: </source>
        <translation type="obsolete">max : </translation>
    </message>
    <message>
        <source>dev: </source>
        <translation type="obsolete">écart-type : </translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="100"/>
        <source>min: %1</source>
        <translation>min : %1</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="104"/>
        <source>mid: %1</source>
        <translation>moy : %1</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="108"/>
        <source>val: %1</source>
        <translation>val : %1</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="117"/>
        <source>max: %1</source>
        <translation>max : %1</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="123"/>
        <source>dev: %1</source>
        <translation>écart-type : %1</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="199"/>
        <source>name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="202"/>
        <source>random mode</source>
        <translation>méthode aléatoire</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="205"/>
        <source>val 1</source>
        <translation>val 1</translation>
    </message>
    <message>
        <location filename="../parametertablemodel.cpp" line="208"/>
        <source>val 2</source>
        <translation>val 2</translation>
    </message>
</context>
<context>
    <name>PluginPage</name>
    <message>
        <location filename="../pluginpage.cpp" line="49"/>
        <source>Plugin Information</source>
        <translation>Informations sur le plugin</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="55"/>
        <source>Add new random preset for this plugin</source>
        <translation>Ajouter un nouveau preset aléatoire</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="58"/>
        <source>Remove this plugin from the list</source>
        <translation>Supprimer ce plugin de la liste</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="61"/>
        <source>Load Preset...</source>
        <translation>Charger un preset...</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="135"/>
        <source>No plug-in loaded</source>
        <translation>Pas de plugin chargé</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="141"/>
        <source>name: %1</source>
        <translation>nom : %1</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="144"/>
        <source>VST ID: %1</source>
        <translation>VST ID : %1</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="149"/>
        <source>version: %1</source>
        <translation>version : %1</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="154"/>
        <source>number of parameters: %1</source>
        <translation>nombre de paramètres : %1</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="166"/>
        <source>preset type: %1</source>
        <translation>type de preset : %1</translation>
    </message>
    <message>
        <source>name: %1
</source>
        <translation type="obsolete">nom : %1\n</translation>
    </message>
    <message>
        <source>version: %1
</source>
        <translation type="obsolete">version : %1\n</translation>
    </message>
    <message>
        <source>VST ID: %1
</source>
        <translation type="obsolete">VST ID : %1\n</translation>
    </message>
    <message>
        <source>number of parameters: %1
</source>
        <translation type="obsolete">nombre de paramètres : %1\n</translation>
    </message>
    <message>
        <source>preset type: %1
</source>
        <translation type="obsolete">type de preset : %1\n</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="169"/>
        <source>file: %1</source>
        <translation>fichier : %1</translation>
    </message>
    <message>
        <source>VST name: </source>
        <translation type="obsolete">Nom : </translation>
    </message>
    <message>
        <source>
VST ID: </source>
        <translation type="obsolete">
Id : </translation>
    </message>
    <message>
        <source>
VST Version: </source>
        <translation type="obsolete">
Version : </translation>
    </message>
    <message>
        <source>
number of parameters: </source>
        <translation type="obsolete">
nombre de paramètres : </translation>
    </message>
    <message>
        <source>
preset type: </source>
        <translation type="obsolete">
type de preset : </translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="160"/>
        <source>chunk</source>
        <translation>chunk</translation>
    </message>
    <message>
        <location filename="../pluginpage.cpp" line="164"/>
        <source>parameters</source>
        <translation>paramètres</translation>
    </message>
</context>
<context>
    <name>RandomModeDelegate</name>
    <message>
        <location filename="../randommodedelegate.cpp" line="43"/>
        <source>uniform</source>
        <translation>uniforme</translation>
    </message>
    <message>
        <location filename="../randommodedelegate.cpp" line="44"/>
        <source>normal</source>
        <translation>normale</translation>
    </message>
    <message>
        <location filename="../randommodedelegate.cpp" line="45"/>
        <source>constant</source>
        <translation>constante</translation>
    </message>
</context>
<context>
    <name>VSTListView</name>
    <message>
        <location filename="../vstlistview.cpp" line="93"/>
        <source>Add new random preset</source>
        <translation>Ajouter un nouveau preset aléatoire</translation>
    </message>
    <message>
        <location filename="../vstlistview.cpp" line="96"/>
        <source>Remove plugin &quot;%1&quot;</source>
        <translation>Supprimer le plugin &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../vstlistview.cpp" line="106"/>
        <source>Rename...</source>
        <translation>Renommer...</translation>
    </message>
    <message>
        <location filename="../vstlistview.cpp" line="109"/>
        <source>Remove preset &quot;%1&quot;</source>
        <translation>Supprimer le preset &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>VSTPreset</name>
    <message>
        <location filename="../vstpreset.cpp" line="55"/>
        <source>default %1</source>
        <translation>défaut %1</translation>
    </message>
</context>
<context>
    <name>VpgLanguageChooser</name>
    <message>
        <location filename="../vpglanguagechooser.cpp" line="36"/>
        <source>Please choose your language:</source>
        <translation>Veuillez choisir la langue appropriée:</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../vpglanguagechooser.cpp" line="56"/>
        <source>Language Chooser</source>
        <translation>Choix de la langue</translation>
    </message>
</context>
<context>
    <name>VpgXml</name>
    <message>
        <source>Could not opening the file: </source>
        <translation type="obsolete">Impossible d&apos;ouvrir le fichier : </translation>
    </message>
    <message>
        <source>Saving Vst List</source>
        <translation type="obsolete">Sauvegarder la liste de VST</translation>
    </message>
    <message>
        <source>Error opening file: </source>
        <translation type="obsolete">Erreur à l&apos;ouverture du fichier : </translation>
    </message>
    <message>
        <source>Opening Vst List</source>
        <translation type="obsolete">Ouvrir une liste de VST</translation>
    </message>
    <message>
        <source>Saving VST List</source>
        <translation type="obsolete">Sauvegarder la liste de VST</translation>
    </message>
    <message>
        <source>Could not open the file: </source>
        <translation type="obsolete">Impossible d&apos;ouvrir le fichier : </translation>
    </message>
    <message>
        <source>Opening VST List</source>
        <translation type="obsolete">Ouvrir une liste de VST</translation>
    </message>
    <message>
        <source>The file is not a valid vpg file: </source>
        <translation type="obsolete">Le fichier sélectionné n&apos;est pas valide : </translation>
    </message>
    <message>
        <source>Error detected in the file: </source>
        <translation type="obsolete">Le programme a détecté une erreur dans le fichier suivant : </translation>
    </message>
    <message>
        <source>The program has detected error in the file: </source>
        <translation type="obsolete">Le programme a détecté une erreur dans le fichier suivant : </translation>
    </message>
    <message>
        <source>The file: </source>
        <translation type="obsolete">Le fichier : </translation>
    </message>
    <message>
        <source> is not a valid vpg file</source>
        <translation type="obsolete"> n&apos;est pas un fichier valide vpg</translation>
    </message>
    <message>
        <source>The program has dectected error in the file: </source>
        <translation type="obsolete">Le programme a détecté une erreur dans le fichier suivant : </translation>
    </message>
</context>
</TS>
