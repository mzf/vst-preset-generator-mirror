/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpglanguagechooser.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: a simple dialog to choose the language of the software
-------------------------------------------------------------------------------
*/

#include "vpglanguagechooser.h"

#include <QtWidgets>


//-----------------------------------------------------------------------------
VpgLanguageChooser::VpgLanguageChooser(QWidget *parent)
     : QDialog(parent)
{
    languageLabel = new QLabel(tr("Please choose your language:"),this);
    languageComboBox = new QComboBox(this);
    qmFiles = findQmFiles();

    for (int i = 0; i < qmFiles.size(); ++i)
    {
        languageComboBox->addItem(getLanguageName(qmFiles[i]));
    }

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(onOkButtonClicked()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(onCancelButtonClicked()));

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(languageLabel);
    mainLayout->addWidget(languageComboBox);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);


    setWindowTitle(tr("Language Chooser"));
    this->layout()->setSizeConstraint(QLayout::SetFixedSize);
}

//-----------------------------------------------------------------------------
VpgLanguageChooser::~VpgLanguageChooser()
{
    delete languageComboBox;
    delete languageLabel;
    delete buttonBox;
}

//-----------------------------------------------------------------------------
QString VpgLanguageChooser::getChoosenLanguageFilePath() const
{
    return this->qmFiles[this->languageComboBox->currentIndex()];
}

//-----------------------------------------------------------------------------
void VpgLanguageChooser::onOkButtonClicked()
{
    //exit
    done(RETURN_CODE_LANGUAGE_CHOOSEN);
}

//-----------------------------------------------------------------------------
void VpgLanguageChooser::onCancelButtonClicked()
{
    done(RETURN_CODE_NO_LANGUAGE_CHOOSEN);
}

//-----------------------------------------------------------------------------
QStringList VpgLanguageChooser::findQmFiles()
{
    QDir dir(":/translations");
    QStringList fileNames = dir.entryList(QStringList("*.qm"), QDir::Files, QDir::Name);

    // Prepend dir path to the file names.
    QMutableStringListIterator fileNameIterator(fileNames);
    while (fileNameIterator.hasNext())
    {
        fileNameIterator.next();
        fileNameIterator.setValue(dir.filePath(fileNameIterator.value()));
    }

    return fileNames;
}

//-----------------------------------------------------------------------------
QString VpgLanguageChooser::getLanguageName(const QString &qmFile)
{
    QTranslator translator;
    translator.load(qmFile);

    return translator.translate("MainDialog", "Language");
}


