/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: presetitemnamedelegate.h
  Author: Francois Mazen
  Date: 11/04/17 19:33
  Description: Delegate to edit the preset name.
-------------------------------------------------------------------------------
*/

#ifndef PRESETITEMNAMEDELEGATE_H
#define PRESETITEMNAMEDELEGATE_H

#include <QStyledItemDelegate>

class PresetItemNameDelegate : public QStyledItemDelegate
{
public:
    PresetItemNameDelegate(QObject* parent);

    // From QStyledItemDelegate.
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
};

#endif // PRESETITEMNAMEDELEGATE_H
