/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgrandom.h
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: header file of the VpgRandom class
-------------------------------------------------------------------------------
*/
/* Random Number Generator for non-uniform distributions */
/* Authors : Weili Chen, Zixuan Ma                       */
/* Anyone can use it for any purposes                    */


#ifndef VPGRANDOM_H
#define VPGRANDOM_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


class VpgRandom
{
public:
       VpgRandom(int seed);

       float getNormalValue(float mu, float sigma) const;
       float getUniformValue(float min, float max) const;

private:
      //function for intern random calculation:
      
      /* Random Number Generator for non-uniform distributions */
      /* Authors : Weili Chen, Zixuan Ma                       */
      /* Anyone can use it for any purposes                    */

      /* Discrete random variable generators */
      int bernoulli(double p);
      int binomial(double p, int n);
      int negativeBinomial(double p, int r); 
      int poisson(int lambda);
      int geometric(double p);

      /* Continuous random variable generators */
      double uniform() const;
      double exponential(double lambda) const;
      double weibull(double k, double lambda);
      double normal(double mu, double sigma) const;
      double lognormal(double mu, double sigma);
      double chisquare(int dof);
      double t(int dof);
      double F(int m, int n);
      double erlang(int k, double rate);

      /* Helper functions for generators */
      int P(int m, int n);
      int C(int m, int n);
      int factor(int n);
      double normalhelper() const;
       
};



#endif //VPGRANDOM_H
