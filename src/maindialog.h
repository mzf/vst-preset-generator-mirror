/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: maindialog.h
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: MainDialog class header
-------------------------------------------------------------------------------
*/

#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QMainWindow>

#include "parameter.h"
#include "vpgfile.h"

class QLabel;
class QStackedWidget;
class QTableView;
class VpgRandom;
class VpgSettings;
class VSTHost;
class VSTListModel;
class VSTListView;
class VSTPlugin;
class VSTPreset;
class VSTPresetReader;

//-----------------------------------------------------------------------------
class MainDialog : public QMainWindow
{
    Q_OBJECT

public:
    MainDialog(VpgSettings& settings);
    ~MainDialog();
    void closeEvent(QCloseEvent* event);

private slots:
    void generatePreset();
    void openVstList();
    bool saveVstList();
    void newVstList();
    void showCopyright();

    void loadVSTPluginFile();
    void loadVSTPresetFile();
    void loadParamFromVST(const QString& fileName); 			//from dll file
    void openPreset(QString fileName);					//from fxp or fxb file
    void createNewPresetForCurrentPlugin();
    void removeCurrentPluginFromList();
    void removeCurrentPresetFromList();

    void setGlobalRandomMethod(Parameter::RandomMode method);
    void setGlobalRandomMethodUniform();
    void setGlobalRandomMethodNormal();
    void setGlobalRandomMethodConstant();
    void setGlobalRandomMethodTotal();

    void onVSTListItemSelected(const QModelIndex& index);

private:
    void dragEnterEvent(QDragEnterEvent *event);    //from Qt
    void dropEvent(QDropEvent *event);               //from Qt

    bool loadVST(const QString &fileName, QSharedPointer<VSTPlugin> &vstPlugin);
    bool isVSTPluginFileName(const QString& fileName) const;
    void loadPresetInParameterTable(const QSharedPointer<VSTPreset> &vstPreset);
    void addNewRandomPreset(const QSharedPointer<VSTPlugin> &vstPlugin);
    bool saveVstListIfNeeded();
    QString getCheckedPluginFilePath(const VpgFile::Plugin& plugin);
    QString getVSTFilePathFromDialog();
    QString getPresetFilePathFromDialog();
    void resetVstListModelWithPlugins(const QList<VpgFile::Plugin> &extractedPlugins, QSharedPointer<VSTPreset> &firstLoadedPreset);

    QTableView* createParameterTableView(QWidget *parent) const;
    QWidget* createDefaultPage(QWidget *parent, QAction *openVstListAction, QAction *importVstAction) const;
    QWidget* createPresetPage(QWidget *parent, QTableView* tableView) const;
    QStackedWidget* createStackedPages(QWidget *parent, QTableView *tableView, QAction *importVstAction, QAction *openVstListAction, int &newPluginPageIndex, int &newPresetPageIndex, int &newDefaultPageIndex) const;

    //list
    VSTListView* vstListView;

    //Pages
    QStackedWidget* stackedPagesWidget;
    int defaultPageIndex;
    int pluginPageIndex;
    int presetPageIndex;

    //Our object to read and save xml files
    VSTListModel *vstListModel;

    //VST host
    QSharedPointer<VSTHost> vstHost;

    //Table to display parameters values
    QTableView* parameterTableView;

    // Random number generator.
    QSharedPointer<VpgRandom> vpgRandom;

    //Object to save settings of the session
    VpgSettings& vpgSettings;

    //Preset reader.
    QSharedPointer<VSTPresetReader> vstPresetReader;
};

#endif //MAINDIALOG_H
