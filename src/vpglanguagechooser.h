/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpglanguagechooser.h
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: a simple dialog to choose the language of the software
-------------------------------------------------------------------------------
*/

#ifndef VPGLANGUAGECHOOSER_H
#define VPGLANGUAGECHOOSER_H



#include <QDialog>
#include <QStringList>

class QComboBox;
class QDialogButtonBox;
class QAbstractButton;
class QLabel;
class QStringList;

class VpgLanguageChooser : public QDialog
{
    Q_OBJECT

public:

    enum RETURN_CODE
    {
        RETURN_CODE_NO_LANGUAGE_CHOOSEN = QDialog::Rejected,
        RETURN_CODE_LANGUAGE_CHOOSEN = QDialog::Accepted
    };

    VpgLanguageChooser(QWidget *parent = 0);
    ~VpgLanguageChooser();
    QString getChoosenLanguageFilePath() const;

private:
    QStringList findQmFiles();
    QString getLanguageName(const QString &qmFile);

    QStringList qmFiles;

    QComboBox *languageComboBox;
    QLabel *languageLabel;
    QDialogButtonBox *buttonBox;

private slots:
    void onOkButtonClicked();
    void onCancelButtonClicked();
};

#endif //VPGLANGUAGECHOOSER_H

