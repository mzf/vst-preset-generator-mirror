/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstpreset.cpp
  Author: Francois Mazen
  Date: 18/07/16 19:55
  Description: Class for VST Preset writer.
-------------------------------------------------------------------------------
*/


#include "vstpreset.h"

#include "parametertablemodel.h"
#include "endiannessconverter.h"
#include "vstplugin.h"

#include <QFile>
#include <QSharedPointer>

//-----------------------------------------------------------------------------
VSTPreset::VSTPreset(const QSharedPointer<VSTPlugin> &vstPlugin, const QString &presetName) :
    vstPlugin(vstPlugin),
    name(presetName)
{
    for(int parameterIndex=0; parameterIndex < vstPlugin->getNumberParams(); parameterIndex++)
    {
        //if we have loaded the plugin (and not only a preset file)
        //we get the name of each parameter
        QSharedPointer<Parameter> parameter(new Parameter(vstPlugin->getParameterName(parameterIndex)));
        connect(parameter.data(), SIGNAL(dataChanged()), this, SLOT(parameterDataChanged()));
        parameters.append(parameter);
    }
}

//-----------------------------------------------------------------------------
VSTPreset::VSTPreset(const QSharedPointer<VSTPlugin> &vstPlugin, int32_t programIndex) :
    vstPlugin(vstPlugin),
    name(tr("default %1").arg(programIndex))
{
    if(programIndex < vstPlugin->getNumberPrograms())
    {
        // Load the parameter values from the program.
        vstPlugin->setProgram(programIndex);
        for(int parameterIndex=0; parameterIndex < vstPlugin->getNumberParams(); parameterIndex++)
        {
            //we get the name of each parameter
            QSharedPointer<Parameter> parameter(new Parameter(vstPlugin->getParameterName(parameterIndex)));
            connect(parameter.data(), SIGNAL(dataChanged()), this, SLOT(parameterDataChanged()));
            parameters.append(parameter);

            // Set the value from the plugin's state.
            parameter->setRandomMode(Parameter::RandomMode_Constant);
            parameter->setConstantValue(vstPlugin->getParameterValue(parameterIndex));

            name = vstPlugin->getCurrentProgramName();
        }
    }
}

//-----------------------------------------------------------------------------
VSTPreset::~VSTPreset()
{

}

//-----------------------------------------------------------------------------
VSTPreset::ERROR_CODE VSTPreset::createQFile(const QString &filename, QFile &file) const
{
    ERROR_CODE errorCode = ERROR_NO_ERROR;

    file.setFileName(filename);
    if (!file.open(QIODevice::WriteOnly))
    {
        errorCode = ERROR_COULD_NOT_CREATE_FILE;
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
VSTPreset::ERROR_CODE VSTPreset::writeRawProgram(QFile &file, int programNumber, const QSharedPointer<VpgRandom> &vpgRandom) const
{
    ERROR_CODE errorCode = ERROR_NO_ERROR;

    //cMagic
    QByteArray buffer = "CcnK";
    file.write(buffer);

    //size of byte * number of parameters
    qint32 presetSize = 4 * 5 + 28 + 4 * vstPlugin->getNumberParams();
    file.write(EndiannessConverter::toBigEndian(presetSize));

    //'FxCk' (regular) or 'FPCh' (opaque chunk) Program
    buffer = "FxCk";
    file.write(buffer);

    ///< format version (currently 1)
    qint32 formatVersion = 1;
    file.write(EndiannessConverter::toBigEndian(formatVersion));

    ///< fx unique ID
    file.write(EndiannessConverter::toBigEndian(vstPlugin->getFxId()));

    ///< fx version
    file.write(EndiannessConverter::toBigEndian(vstPlugin->getFxVersion()));

    //< number of parameters
    file.write(EndiannessConverter::toBigEndian(vstPlugin->getNumberParams()));

    ///< program name (null-terminated ASCII string) !!!![28]!!!!
    buffer.clear();
    buffer.setNum(programNumber);
    buffer.prepend("VPG ");
    file.write(buffer,28);

    //Float parameters
    for(qint32 parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex ++)
    {
        float parameterValue = parameters[parameterIndex]->computeRandomValue(vpgRandom);
        char* parameterPointer = (char*)&parameterValue;

        //inversion because little-endian notation
        buffer.clear();
        buffer.resize(4);
        buffer[3] = parameterPointer[0];
        buffer[2] = parameterPointer[1];
        buffer[1] = parameterPointer[2];
        buffer[0] = parameterPointer[3];

        file.write(buffer);
    }//params

    return errorCode;
}

//-----------------------------------------------------------------------------
VSTPreset::ERROR_CODE VSTPreset::writeProgramToFile(const QString& filename, const QSharedPointer<VpgRandom> &vpgRandom) const
{
    QFile file;
    ERROR_CODE errorCode = createQFile(filename, file);
    if(errorCode == ERROR_NO_ERROR)
    {
        errorCode = writeRawProgram(file, 1, vpgRandom);
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
VSTPreset::ERROR_CODE VSTPreset::writeProgramChunkToFile(const QString& filename, const QSharedPointer<VpgRandom> &vpgRandom) const
{
    QFile file;
    ERROR_CODE errorCode = createQFile(filename, file);
    if(errorCode == ERROR_NO_ERROR)
    {
        //first write the parameters to the plug-in
        //then get the program chunk
        for(qint32 parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex ++)
        {
            float parameterValue = parameters[parameterIndex]->computeRandomValue(vpgRandom);
            vstPlugin->setParameter(parameterIndex, parameterValue);
        }
        QString presetName;
        presetName.setNum(1);
        presetName.prepend("VPG ");
        vstPlugin->setCurrentProgramName(qPrintable(presetName));

        void* chunk = NULL;
        qint32 chunkSize = vstPlugin->getProgramChunk(&chunk);

        //cMagic
        QByteArray buffer = "CcnK";
        file.write(buffer);

        //size = 5 parameters + 28 for prgName + chunksize (4) + chunk
        qint32 presetSize = 4 * 5 + 28 + 4 + chunkSize;
        file.write(EndiannessConverter::toBigEndian(presetSize));

        //'FxCk' (regular) or 'FPCh' (opaque chunk) Program
        buffer = "FPCh";
        file.write(buffer);

        ///< format version (currently 1)
        qint32 formatVersion = 1;
        file.write(EndiannessConverter::toBigEndian(formatVersion));

        ///< fx unique ID
        file.write(EndiannessConverter::toBigEndian(vstPlugin->getFxId()));

        ///< fx version
        file.write(EndiannessConverter::toBigEndian(vstPlugin->getFxVersion()));

        //< number of parameters
        file.write(EndiannessConverter::toBigEndian(vstPlugin->getNumberParams()));

        ///< program name (null-terminated ASCII string) !!!![28]!!!!
        file.write(presetName.toLatin1(), 28);

        //chunk size and chunk
        file.write(EndiannessConverter::toBigEndian(chunkSize));
        file.write((char*)chunk, chunkSize);
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
VSTPreset::ERROR_CODE VSTPreset::writeBankToFile(const QString& filename, int programCountPerBank, const QSharedPointer<VpgRandom> &vpgRandom) const
{
    QFile file;
    ERROR_CODE errorCode = createQFile(filename, file);
    if(errorCode == ERROR_NO_ERROR)
    {
        //cMagic
        QByteArray buffer = "CcnK";
        file.write(buffer);

        //size = header size + number of program * program size
        qint32 presetSize = 4 * 5 + 128 + programCountPerBank * (4 * 7 + 28 + 4 * vstPlugin->getNumberParams());
        file.write(EndiannessConverter::toBigEndian(presetSize));

        ///< 'FxCk' (regular) or 'FPCh' (opaque chunk)
        buffer = "FxBk";
        file.write(buffer);

        ///< format version (currently 1)
        qint32 formatVersion = 1;
        file.write(EndiannessConverter::toBigEndian(formatVersion));

        ///< fx unique ID
        file.write(EndiannessConverter::toBigEndian(vstPlugin->getFxId()));

        ///< fx version
        file.write(EndiannessConverter::toBigEndian(vstPlugin->getFxVersion()));

        //number of programs
        file.write(EndiannessConverter::toBigEndian(programCountPerBank));

        //currentProgram = 1
        file.write(EndiannessConverter::toBigEndian(1));

        //future space for bank preset
        QByteArray blankArray(124, '\0');
        file.write(blankArray);

        //Write all programs
        for(int programIndex = 0; programIndex < programCountPerBank && (errorCode == ERROR_NO_ERROR); programIndex++)
        {
            errorCode = writeRawProgram(file, programIndex, vpgRandom);
        }//programs

    }

    return errorCode;
}

//-----------------------------------------------------------------------------
VSTPreset::ERROR_CODE VSTPreset::writeBankChunkToFile(const QString& filename, int programCountPerBank, const QSharedPointer<VpgRandom> &vpgRandom) const
{
    QFile file;
    ERROR_CODE errorCode = createQFile(filename, file);
    if(errorCode == ERROR_NO_ERROR)
    {
        qint32 finalProgramCount = std::min(programCountPerBank, vstPlugin->getNumberPrograms());
        if (finalProgramCount <= 0)
        {
            errorCode = ERROR_VST_HOST_INVALID_PROGRAM_NUMBER;
        }

        if(errorCode == ERROR_NO_ERROR)
        {
            //first, write the parameters to the plug-in
            //and get the bank chunk
            for(qint32 programIndex = 0; programIndex < finalProgramCount; programIndex++)
            {
                //select the program:
                vstPlugin->setProgram(programIndex);
                for(qint32 parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex ++)
                {
                    float parameterValue = parameters[parameterIndex]->computeRandomValue(vpgRandom);
                    vstPlugin->setParameter(parameterIndex, parameterValue);
                }
                QString name;
                name.setNum(programIndex);
                name.prepend("VPG ");
                vstPlugin->setCurrentProgramName(qPrintable(name));
            }

            //get the bank chunk
            void* chunk = NULL;
            qint32 chunkSize = vstPlugin->getBankChunk(&chunk);

            //cMagic
            QByteArray buffer = "CcnK";
            file.write(buffer);

            //size = 5 parameters + future (128 bytes) + chunkSize (4 bytes) + chunk
            qint32 presetSize = 4 * 5 + 128 + 4 + chunkSize;
            file.write(EndiannessConverter::toBigEndian(presetSize));

            //'FxBk' (regular) or 'FBCh' (opaque chunk) Program
            buffer = "FBCh";
            file.write(buffer);

            ///< format version (currently 1)
            qint32 formatVersion = 1;
            file.write(EndiannessConverter::toBigEndian(formatVersion));

            ///< fx unique ID
            file.write(EndiannessConverter::toBigEndian(vstPlugin->getFxId()));

            ///< fx version
            file.write(EndiannessConverter::toBigEndian(vstPlugin->getFxVersion()));

            // number of programs
            file.write(EndiannessConverter::toBigEndian(finalProgramCount));

            //currentProgram = 1
            file.write(EndiannessConverter::toBigEndian(1));

            //future space for bank preset
            QByteArray blankArray(124, '\0');
            file.write(blankArray);

            //chunk size and chunk
            file.write(EndiannessConverter::toBigEndian(chunkSize));
            file.write((char*)chunk, chunkSize);
        }

    }

    return errorCode;
}

//-----------------------------------------------------------------------------
const QVector<QSharedPointer<Parameter> >& VSTPreset::getParameters() const
{
    return parameters;
}

//-----------------------------------------------------------------------------
QVector<QSharedPointer<Parameter> > &VSTPreset::getParametersForModification()
{
    return parameters;
}

//-----------------------------------------------------------------------------
const QSharedPointer<VSTPlugin>& VSTPreset::getVSTPlugin() const
{
    return vstPlugin;
}

//-----------------------------------------------------------------------------
const QString &VSTPreset::getName() const
{
    return name;
}

//-----------------------------------------------------------------------------
void VSTPreset::setName(const QString& newName)
{
    if(!newName.isNull() && newName != "")
    {
        name = newName;
        emit dataChanged();
    }
}

//-----------------------------------------------------------------------------
void VSTPreset::parameterDataChanged()
{
    // Simply forward the event.
    emit dataChanged();
}
