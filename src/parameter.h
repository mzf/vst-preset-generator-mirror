/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: parameter.h
  Author: Francois Mazen
  Date: 25/05/16 22:01
  Description: Class for parameters.
-------------------------------------------------------------------------------
*/


#ifndef PARAMETER_H
#define PARAMETER_H

#include <QString>
#include <QObject>

class VpgRandom;

class Parameter : public QObject
{      
    Q_OBJECT

public:

    //random modes:
    enum RandomMode
    {
        RandomMode_Random = -1,
        RandomMode_Uniform = 0,
        RandomMode_Normal,
        RandomMode_Constant,
        RandomMode_RandomModeMax //always the last one.
    };

    Parameter();
    Parameter(const QString& name);
    virtual ~Parameter();

    const QString& getName() const;
    void setName(const QString& newName);

    RandomMode getRandomMode() const;
    void setRandomMode(RandomMode mode);

    float getUniformMinValue() const;
    void setUniformMinValue(float value);

    float getUniformMaxValue() const;
    void setUniformMaxValue(float value);

    float getNormalMiddleValue() const;
    void setNormalMiddleValue(float value);

    float getNormalStandardDeviation() const;
    void setNormalStandardDeviation(float value);

    float getConstantValue() const;
    void setConstantValue(float value);

    float computeRandomValue(const QSharedPointer<VpgRandom> &vpgRandom) const;

    void copyValuesFromOtherParameter(const QSharedPointer<Parameter> &other);

signals:
    void dataChanged();

private:
    QString name;
    RandomMode randomMode;
    float uniformMinValue;
    float uniformMaxValue;
    float normalStandardDeviation;
    float targetValue;
}; 

#endif //PARAMETER_H
