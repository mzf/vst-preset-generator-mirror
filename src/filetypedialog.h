/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: filetypedialog.h
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: a dialog class to choose the type of the preset file (fxp or fxb)
-------------------------------------------------------------------------------
*/

#ifndef FILETYPEDIALOG_H
#define FILETYPEDIALOG_H

#include <QDialog>

class QLabel;
class QRadioButton;
class QSpinBox;
class QPushButton;
class QGroupBox;

class FileTypeDialog : public QDialog
{
    Q_OBJECT

public:

    enum FILE_TYPE
    {
        FILE_TYPE_INVALID = -1,
        FILE_TYPE_PROGRAM = 0,
        FILE_TYPE_BANK = 1
    };

    FileTypeDialog(QWidget *parent, bool isBankFileEnabled, int maxProgramPerBank);

    int getProgramPerBankValue() const;

private slots:
    void ProcessOk();
    void ProcessCancel();

private:
    //Qt Objects
    QRadioButton *rbtnProgram;
    QRadioButton *rbtnBank;
    QLabel *lblNbProg;
    QSpinBox *spbxBank;
    QPushButton *btnOk;
    QPushButton *btnCancel;
    QGroupBox *gbFileType;
};



#endif //FILETYPEDIALOG_H
