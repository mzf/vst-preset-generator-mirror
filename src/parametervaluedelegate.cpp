/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: parametervaluedelegate.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: Object to edit a value in a table (see Qt's pattern)
-------------------------------------------------------------------------------
*/

#include "parametervaluedelegate.h"

#include "parametervalueeditor.h"
#include "parametertablemodel.h"
#include <QtWidgets>

//-----------------------------------------------------------------------------
//delegate float value class
//-----------------------------------------------------------------------------
ParameterValueDelegate::ParameterValueDelegate(QObject *parent) : QItemDelegate(parent)
{
}

//-----------------------------------------------------------------------------
QWidget *ParameterValueDelegate::createEditor(QWidget *parent,
     const QStyleOptionViewItem &/* option */,
     const QModelIndex &/* index */) const
{
    ParameterValueEditor* editor = new ParameterValueEditor(parent);
    connect(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));

    return editor;
}
 
//-----------------------------------------------------------------------------
void ParameterValueDelegate::setEditorData(QWidget *editor,
                                     const QModelIndex &index) const
{
    ParameterValueEditor* parameterValueEditor = dynamic_cast<ParameterValueEditor*>(editor);
    if(parameterValueEditor != NULL)
    {
        parameterValueEditor->setParameterIndex(index.row());

        const ParameterTableModel* parameterTableModel = dynamic_cast<const ParameterTableModel*>(index.model());
        if(parameterTableModel != NULL)
        {
            parameterValueEditor->setShouldUsePluginDisplay(parameterTableModel->doesIndexUseParameterDisplay(index));
            parameterValueEditor->setPlugin(parameterTableModel->getVSTPreset()->getVSTPlugin());
            parameterValueEditor->setValue(parameterTableModel->data(index, Qt::EditRole).toFloat());
        }
    }
}

//----------------------------------------------------------------------------- 
void ParameterValueDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                    const QModelIndex &index) const
{
    ParameterValueEditor* parameterValueEditor = dynamic_cast<ParameterValueEditor*>(editor);
    if(parameterValueEditor != NULL)
    {
        model->setData(index, parameterValueEditor->getValue());
    }
}

//-----------------------------------------------------------------------------
void ParameterValueDelegate::updateEditorGeometry(QWidget *editor,
     const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    QSize size = editor->sizeHint();
    size.setWidth(option.rect.width());
    editor->resize(size);

    // The editor is a popup and its position should be in global coordinates.
    QWidget* editorParent = editor->parentWidget();
    QPoint globalPosition = editorParent->mapToGlobal(QPoint(option.rect.x(), option.rect.y()));
    editor->move(globalPosition);
}

//-----------------------------------------------------------------------------
void ParameterValueDelegate::commitAndCloseEditor()
{
    ParameterValueEditor* editor = qobject_cast<ParameterValueEditor*>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
}
