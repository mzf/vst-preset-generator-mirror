/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgfile.cpp
  Author: Francois Mazen
  Date: 28/06/16 19:22
  Description: VpgFile class that represent the list of VST plugins and presets
               loaded by the user.
-------------------------------------------------------------------------------
*/

#include "vpgfile.h"

#include "vstlistmodel.h"
#include "vstlistitem.h"
#include "vstplugin.h"
#include "vstpreset.h"
#include "vstsdk.h"
#include "vpgversion.h"

#include <QtXml/QDomDocument>
#include <QFile>
#include <QTextStream>
#include <QSharedPointer>

//-----------------------------------------------------------------------------
VpgFile::ERROR_CODE VpgFile::ExtractPluginsFromFile(const QString& filename, QList<Plugin>& pluginFromFile)
{
    ERROR_CODE errorCode = ERROR_NO_ERROR;

    // VPG file format changelog
    // =========================
    // * 0.3.0
    //   - vst/fx_id is an integer instead of a string
    //   - vst/nb_param removed
    //   - vst/chunk removed
    //   - vst contains preset tags for each preset, and each preset contains param tags.
    //
    // * 0.2.7
    //   - versioning in vpg_vst_list tag: major_number, minor_number and revision_number.
    //



    //read the file
    QFile file(filename);
    bool isSuccess = file.open(QIODevice::ReadOnly);
    if (!isSuccess)
    {
        errorCode = ERROR_COULD_NOT_OPEN_FILE;
    }

    QDomDocument doc;
    if(isSuccess)
    {
        isSuccess = doc.setContent(&file);
        if(!isSuccess)
        {
            errorCode = ERROR_COULD_NOT_PARSE_FILE;
        }
        file.close();
    }

    QDomElement root;
    if(isSuccess)
    {
        //check data
        root = doc.documentElement();
        if( root.tagName() != "vpg_vst_list" )
        {
            isSuccess = false;
            errorCode = ERROR_INVALID_VPG_FILE;
        }
    }

    VpgVersion fileVersion(0,0,0);
    if(isSuccess)
    {
        // Read version. If no version it means the file version is before 0.2.7.
        unsigned int majorNumber = 0;
        unsigned int minorNumber = 0;
        unsigned int revisionNumber = 0;
        if(root.hasAttribute("major_number") &&
           root.hasAttribute("minor_number") &&
           root.hasAttribute("revision_number"))
        {
            majorNumber = root.attribute("major_number").toUInt();
            minorNumber = root.attribute("minor_number").toUInt();
            revisionNumber = root.attribute("revision_number").toUInt();
        }
        fileVersion = VpgVersion(majorNumber, minorNumber, revisionNumber);
    }

    if(isSuccess)
    {

        //fill the data:
        QDomNode node = root.firstChild();
        while(!node.isNull() && isSuccess)
        {
            QDomElement vstTag = node.toElement();
            if(!vstTag.isNull())
            {
                if(vstTag.tagName() == "vst")
                {
                    Plugin newPlugin;
                    errorCode = fillPluginFromXML(fileVersion, vstTag, newPlugin);
                    isSuccess = errorCode == ERROR_NO_ERROR;

                    if(isSuccess)
                    {
                        pluginFromFile.push_back(newPlugin);
                    }
                }
                else
                {
                  isSuccess = false;
                  errorCode = ERROR_UNKNOWN_XML_TAG;
                }
            }
            else
            {
               isSuccess = false; //check error while reading
               errorCode = ERROR_INVALID_XML_ELEMENT;
            }

            node = node.nextSibling();
        }
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
VpgFile::ERROR_CODE VpgFile::fillPluginFromXML(const VpgVersion& fileVersion, const QDomElement& vstTag, VpgFile::Plugin& newPlugin)
{
    ERROR_CODE errorCode = ERROR_NO_ERROR;
    bool isSuccess = true;

    if(vstTag.hasAttribute("name"))
    {
        newPlugin.name = vstTag.attribute("name");
    }

    if(vstTag.hasAttribute("fx_path"))
    {
        newPlugin.filePath = vstTag.attribute("fx_path");
    }

    if(vstTag.hasAttribute("fx_id"))
    {
        QString fxIdString = vstTag.attribute("fx_id");
        if(fileVersion < VpgVersion(0,3,0) && fxIdString.size() >= 4)
        {
            // Before 0.3.0 the id was the string representation.
            // Get the corresponding integer.
            newPlugin.id = CCONST(fxIdString[0].toLatin1(), fxIdString[1].toLatin1(), fxIdString[2].toLatin1(), fxIdString[3].toLatin1());
        }
        else
        {
            newPlugin.id = fxIdString.toInt();
        }
    }

    if(vstTag.hasAttribute("fx_version"))
    {
        newPlugin.version = vstTag.attribute("fx_version").toInt();
    }

    // Extract presets.
    QDomNode presetNode = vstTag.firstChild();
    while(!presetNode.isNull() && isSuccess)
    {
        QDomElement presetTag = presetNode.toElement();
        if(!presetTag.isNull() && presetTag.tagName() == "preset")
        {
            Preset newPreset;
            errorCode = fillPresetFromXML(presetTag, newPreset);
            isSuccess = errorCode == ERROR_NO_ERROR;
            if(isSuccess)
            {
                newPlugin.presets.push_back(newPreset);
            }
        }
        else
        {
          isSuccess = false;
          errorCode = ERROR_UNKNOWN_XML_TAG;
        }

        presetNode = presetNode.nextSibling();
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
VpgFile::ERROR_CODE VpgFile::fillPresetFromXML(const QDomElement& presetTag, Preset& newPreset)
{
    ERROR_CODE errorCode = ERROR_NO_ERROR;
    bool isSuccess = true;

    if(presetTag.hasAttribute("name"))
    {
        newPreset.name = presetTag.attribute("name");
    }

    QDomNode paramNode = presetTag.firstChild();
    while(!paramNode.isNull() && isSuccess)
    {
        QDomElement paramTag = paramNode.toElement();
        if(!paramTag.isNull() && paramTag.tagName() == "param")
        {
            QSharedPointer<Parameter> newParameter(new Parameter());
            fillParameterFromXML(paramTag, newParameter);
            newPreset.parameters.push_back(newParameter);
        }
        else
        {
          isSuccess = false;
          errorCode = ERROR_UNKNOWN_XML_TAG;
        }

        paramNode = paramNode.nextSibling();
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
void VpgFile::fillParameterFromXML(const QDomElement& paramTag, QSharedPointer<Parameter>& parameter)
{
    if(paramTag.hasAttribute("mode"))
    {
        QString modeName = paramTag.attribute("mode");
        if(modeName == "uniform")
        {
            parameter->setRandomMode(Parameter::RandomMode_Uniform);

            if(paramTag.hasAttribute("min"))
            {
                parameter->setUniformMinValue(paramTag.attribute("min").toFloat());
            }

            if(paramTag.hasAttribute("max"))
            {
                parameter->setUniformMaxValue(paramTag.attribute("max").toFloat());
            }
        }
        else if(modeName == "constant")
        {
            parameter->setRandomMode(Parameter::RandomMode_Constant);

            if(paramTag.hasAttribute("val"))
            {
                parameter->setConstantValue(paramTag.attribute("val").toFloat());
            }
        }
        else if(modeName == "normal")
        {
            parameter->setRandomMode(Parameter::RandomMode_Normal);

            if(paramTag.hasAttribute("mid"))
            {
                parameter->setNormalMiddleValue(paramTag.attribute("mid").toFloat());
            }

            if(paramTag.hasAttribute("dev"))
            {
                parameter->setNormalStandardDeviation(paramTag.attribute("dev").toFloat());
            }
        }
    }
}

//-----------------------------------------------------------------------------
VpgFile::ERROR_CODE VpgFile::WriteModelToFile(const QString& filename, const VSTListModel& vstListModel)
{
    ERROR_CODE errorCode = ERROR_NO_ERROR;

    //create the dom (virtual) file
    QDomDocument doc;
    QDomElement root = doc.createElement( "vpg_vst_list" );
    root.setAttribute("major_number", VpgVersion::getCurrentVersion().getMajorNumber());
    root.setAttribute("minor_number", VpgVersion::getCurrentVersion().getMinorNumber());
    root.setAttribute("revision_number", VpgVersion::getCurrentVersion().getRevisionNumber());
    doc.appendChild( root );

    createPluginTags(vstListModel, doc, root);

    QFile file( filename );
    if(!file.open( QIODevice::WriteOnly ))
    {
        errorCode = ERROR_COULD_NOT_OPEN_FILE;
    }
    else
    {
        QTextStream textStream( &file );
        textStream << doc.toString();
        file.close();
    }

    return errorCode;
}

//-----------------------------------------------------------------------------
void VpgFile::createPluginTags(const VSTListModel& vstListModel, QDomDocument& doc, QDomElement& root)
{
    QList<QSharedPointer<VSTListItem> > pluginItems = vstListModel.getPlugins();
    for(QList<QSharedPointer<VSTListItem> >::const_iterator itemIterator = pluginItems.begin();
        itemIterator != pluginItems.end();
        ++itemIterator)
    {
        const QSharedPointer<VSTPlugin>& vstPlugin = (*itemIterator)->getVSTPlugin();
        if(!vstPlugin.isNull())
        {
            QDomElement vstTag = doc.createElement( "vst" );
            vstTag.setAttribute( "name", vstPlugin->getName());
            vstTag.setAttribute( "fx_id", vstPlugin->getFxId());
            vstTag.setAttribute( "fx_version", vstPlugin->getFxVersion());
            vstTag.setAttribute( "fx_path", vstPlugin->getFilePath());

            createPresetTags(vstListModel, doc, vstPlugin, vstTag);

            root.appendChild(vstTag);
        }
    }
}

//-----------------------------------------------------------------------------
void VpgFile::createPresetTags(const VSTListModel& vstListModel, QDomDocument& doc, const QSharedPointer<VSTPlugin>& vstPlugin, QDomElement& vstTag)
{
    QList<QSharedPointer<VSTListItem> > presetItems = vstListModel.getPluginPresets(vstPlugin);
    for(QList<QSharedPointer<VSTListItem> >::const_iterator presetItemIterator = presetItems.begin();
        presetItemIterator != presetItems.end();
        ++presetItemIterator)
    {
        const QSharedPointer<VSTPreset>& vstPreset = (*presetItemIterator)->getVstPreset();
        if(!vstPreset.isNull())
        {
            QDomElement presetTag = doc.createElement("preset");
            presetTag.setAttribute("name", vstPreset->getName());

            createParameterTags(doc, presetTag, vstTag, vstPreset);
        }
    }
}

//-----------------------------------------------------------------------------
void VpgFile::createParameterTags(QDomDocument& doc, QDomElement& presetTag, QDomElement& vstTag, const QSharedPointer<VSTPreset>& vstPreset)
{
    const QVector<QSharedPointer<Parameter> >& parameters = vstPreset->getParameters();
    for(QVector<QSharedPointer<Parameter> >::const_iterator parameterIterator = parameters.begin();
        parameterIterator != parameters.end();
        ++parameterIterator)
    {
        QSharedPointer<Parameter> parameter = *parameterIterator;
        QDomElement paramTag = doc.createElement("param");
        switch(parameter->getRandomMode())
        {
        case Parameter::RandomMode_Uniform:
            paramTag.setAttribute("mode", "uniform");
            paramTag.setAttribute("min", parameter->getUniformMinValue());
            paramTag.setAttribute("max", parameter->getUniformMaxValue());
            break;
        case Parameter::RandomMode_Constant:
            paramTag.setAttribute("mode", "constant");
            paramTag.setAttribute("val", parameter->getConstantValue());
            break;
        case Parameter::RandomMode_Normal:
            paramTag.setAttribute("mode", "normal");
            paramTag.setAttribute("mid", parameter->getNormalMiddleValue());
            paramTag.setAttribute("dev", parameter->getNormalStandardDeviation());
            break;
        default:
            break;
        }

        presetTag.appendChild(paramTag);
    }


    vstTag.appendChild(presetTag);
}
