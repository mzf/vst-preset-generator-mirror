/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstlistmodel.cpp
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: class using Qt's XML functions in order to read and save the
               list of VST plugins (.vpg) with their header's parameters
-------------------------------------------------------------------------------
*/

#include "vstlistmodel.h"

#include "vstlistitem.h"
#include "vpgversion.h"
#include "vstpreset.h"
#include "vstplugin.h"

#include <QtWidgets>


//-----------------------------------------------------------------------------
VSTListModel::VSTListModel(QWidget * parent) : QAbstractItemModel(parent)
{
    this->areDataWritten = true;
}

//-----------------------------------------------------------------------------
VSTListModel::~VSTListModel()
{
    clearListWithoutRefresh();
}

//-----------------------------------------------------------------------------
QVariant VSTListModel::data(const QModelIndex & modelIndex, int role ) const
{
    QVariant returnedData;

    //must return only the name of the header
    if(modelIndex.isValid())
    {
        VSTListItem* listItem = static_cast<VSTListItem* >(modelIndex.internalPointer());

        if(role == Qt::DisplayRole)
        {
            if(listItem->getType() == VSTListItem::VST_TYPE)
            {
                returnedData = listItem->getVSTPlugin()->getName();
            }
            else if(listItem->getType() == VSTListItem::PRESET_TYPE)
            {
                returnedData = listItem->getVstPreset()->getName();
            }
        }
        else if(role == Qt::DecorationRole)
        {
            if(listItem->getType() == VSTListItem::VST_TYPE)
            {
                returnedData = QIcon(":/images/import.png");
            }
            else if(listItem->getType() == VSTListItem::PRESET_TYPE)
            {
                returnedData = QIcon(":/images/generate.png");
            }
        }
    }

    return returnedData;
}

//-----------------------------------------------------------------------------
int VSTListModel::rowCount(const QModelIndex& parent) const
{
    int columnCount = 0;

    if(!parent.isValid())
    {
        // This mean we are at the root.
        columnCount = this->vstPlugins.size();
    }
    else
    {
        VSTListItem* listItem = static_cast<VSTListItem* >(parent.internalPointer());
        if(listItem->getType() == VSTListItem::VST_TYPE)
        {
            columnCount = presetsByVstId[listItem->getVSTPlugin()->getFxId()].size();
        }
    }

    return columnCount;
}

//-----------------------------------------------------------------------------
int VSTListModel::columnCount(const QModelIndex& /*parent*/) const
{
    return 1;
}

//-----------------------------------------------------------------------------
QModelIndex VSTListModel::parent(const QModelIndex &modelIndex) const
{
    QModelIndex parentIndex;

    if (modelIndex.isValid())
    {

        VSTListItem* listItem = static_cast<VSTListItem* >(modelIndex.internalPointer());

        // parent only for presets.
        if(listItem->getType() == VSTListItem::PRESET_TYPE)
        {
            const QSharedPointer<VSTPreset>& vstPreset = listItem->getVstPreset();

            //create index with parent vst.
            int parentRow = 0;
            bool isParentFound = false;
            for(int pluginIndex = 0; pluginIndex < this->vstPlugins.size() && !isParentFound; ++pluginIndex)
            {
                if(this->vstPlugins[pluginIndex]->getVSTPlugin()->getFxId() == vstPreset->getVSTPlugin()->getFxId())
                {
                    parentRow = pluginIndex;
                    isParentFound = true;
                }
            }

            if(isParentFound)
            {
                parentIndex = createIndex(parentRow, 0, this->vstPlugins[parentRow].data());
            }
        }
    }

    return parentIndex;
}

//-----------------------------------------------------------------------------
QModelIndex VSTListModel::index(int row, int column, const QModelIndex &parent) const
{
    QModelIndex modelIndex;
    if (hasIndex(row, column, parent))
    {
        if (!parent.isValid())
        {
            // root item, vst plugins
            modelIndex = createIndex(row, column, this->vstPlugins[row].data());
        }
        else
        {
            // child item, presets.
            VSTListItem* parentVst = static_cast<VSTListItem* >(parent.internalPointer());
            QSharedPointer<VSTListItem> vstPreset = presetsByVstId[parentVst->getVSTPlugin()->getFxId()][row];
            modelIndex = createIndex(row, column, vstPreset.data());
        }
    }

    return modelIndex;
}

//-----------------------------------------------------------------------------
Qt::ItemFlags VSTListModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags itemFlags = QAbstractItemModel::flags(index);

    // Set preset name editable.
    if(index.isValid() && parent(index).isValid())
    {
        itemFlags |= Qt::ItemIsEditable;
    }

    return itemFlags;
}

//-----------------------------------------------------------------------------
bool VSTListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.isValid() && role == Qt::EditRole)
    {
        VSTListItem* presetItem = static_cast<VSTListItem* >(index.internalPointer());
        if(presetItem != NULL && presetItem->getType() == VSTListItem::PRESET_TYPE)
        {
            QSharedPointer<VSTPreset> vstPreset = presetItem->getVstPreset();
            if(!vstPreset.isNull())
            {
                vstPreset->setName(value.toString());
                dataChanged(index, index);
            }
        }
    }

    return true;
}

//-----------------------------------------------------------------------------
void VSTListModel::addPlugin(const QSharedPointer<VSTPlugin>& vstPlugin)
{
    this->beginResetModel();

    addPluginWithoutRefresh(vstPlugin);

    this->endResetModel();
}

//-----------------------------------------------------------------------------
void VSTListModel::addPreset(const QSharedPointer<VSTPreset>& vstPreset)
{
    if(vstPreset != NULL)
    {
        int32_t parentFxId = vstPreset->getVSTPlugin()->getFxId();

        bool isParentFound = false;
        for(QList<QSharedPointer<VSTListItem> >::const_iterator pluginItemIterator = vstPlugins.begin();
            pluginItemIterator != vstPlugins.end() && !isParentFound;
            ++pluginItemIterator)
        {
            if((*pluginItemIterator)->getVSTPlugin()->getFxId() == parentFxId)
            {
                isParentFound = true;
            }
        }

        if(isParentFound)
        {
            this->beginResetModel();
            presetsByVstId[parentFxId].append(QSharedPointer<VSTListItem>(new VSTListItem(vstPreset)));
            this->endResetModel();

            connect(vstPreset.data(), SIGNAL(dataChanged()), this, SLOT(presetDataChanged()));
            this->areDataWritten = false;
        }
    }
}

//-----------------------------------------------------------------------------
void VSTListModel::removePlugin(const QSharedPointer<VSTPlugin> &vstPlugin)
{
    if(!vstPlugin.isNull())
    {
        this->beginResetModel();

        // Find the plugin in the list and remove it.
        QList<QSharedPointer<VSTListItem> >::iterator finalPluginIterator = vstPlugins.end();
        for(QList<QSharedPointer<VSTListItem> >::iterator pluginIterator = vstPlugins.begin();
            pluginIterator != vstPlugins.end() && finalPluginIterator == vstPlugins.end();
            ++pluginIterator)
        {
            if((*pluginIterator)->getVSTPlugin()->getFxId() == vstPlugin->getFxId())
            {
                finalPluginIterator = pluginIterator;
            }
        }

        if(finalPluginIterator != vstPlugins.end())
        {
            vstPlugins.erase(finalPluginIterator);
        }

        // Remove presets.
        QMap<int32_t, QList<QSharedPointer<VSTListItem> > >::iterator presetsIterator = presetsByVstId.find(vstPlugin->getFxId());
        if(presetsIterator != presetsByVstId.end())
        {
            presetsIterator->clear();
            presetsByVstId.erase(presetsIterator);
        }

        this->endResetModel();
    }
}

//-----------------------------------------------------------------------------
void VSTListModel::removePreset(const QSharedPointer<VSTPreset> &vstPreset)
{
    if(vstPreset != NULL)
    {
        int32_t parentFxId = vstPreset->getVSTPlugin()->getFxId();

        bool isParentFound = false;
        for(QList<QSharedPointer<VSTListItem> >::const_iterator pluginItemIterator = vstPlugins.begin();
            pluginItemIterator != vstPlugins.end() && !isParentFound;
            ++pluginItemIterator)
        {
            if((*pluginItemIterator)->getVSTPlugin()->getFxId() == parentFxId)
            {
                isParentFound = true;
            }
        }

        if(isParentFound)
        {
            QList<QSharedPointer<VSTListItem> >& presetList = presetsByVstId[parentFxId];

            // Find the preset (should be unique).
            QList<QSharedPointer<VSTListItem> >::iterator presetToRemove = presetList.end();
            for(QList<QSharedPointer<VSTListItem> >::iterator presetIterator = presetList.begin();
                presetIterator != presetList.end() && presetToRemove == presetList.end();
                ++presetIterator)
            {
                if((*presetIterator)->getVstPreset() == vstPreset)
                {
                    presetToRemove = presetIterator;
                }
            }

            if(presetToRemove != presetList.end())
            {
                this->beginResetModel();
                presetList.erase(presetToRemove);
                this->endResetModel();

                this->areDataWritten = false;
            }
        }
    }
}

//-----------------------------------------------------------------------------
void VSTListModel::addPluginWithoutRefresh(const QSharedPointer<VSTPlugin>& vstPlugin)
{
    if(!vstPlugin.isNull())
    {
        // Check if the plugin is not already in the list.
        bool isPluginFound = false;
        for(QList<QSharedPointer<VSTListItem> >::const_iterator pluginIterator = vstPlugins.begin();
            pluginIterator != vstPlugins.end() && !isPluginFound;
            ++pluginIterator)
        {
            if((*pluginIterator)->getVSTPlugin()->getFxId() == vstPlugin->getFxId())
            {
                isPluginFound = true;
            }
        }

        if(!isPluginFound)
        {
            this->vstPlugins.append(QSharedPointer<VSTListItem>(new VSTListItem(vstPlugin)));
            this->areDataWritten = false;
        }
    }
}

//-----------------------------------------------------------------------------
void VSTListModel::clearList()
{
    this->beginResetModel();

    clearListWithoutRefresh();

    this->endResetModel();
}

//-----------------------------------------------------------------------------
void VSTListModel::clearListWithoutRefresh()
{
    this->vstPlugins.clear();
    presetsByVstId.clear();

    this->areDataWritten = false;
}

//-----------------------------------------------------------------------------
void VSTListModel::presetDataChanged()
{
    this->areDataWritten = false;
}

//-----------------------------------------------------------------------------
bool VSTListModel::areDataSaved() const
{
    return this->areDataWritten;
}

//-----------------------------------------------------------------------------
void VSTListModel::setNeedSaving(bool doesNeedSaving)
{
    this->areDataWritten = !doesNeedSaving;
}

//-----------------------------------------------------------------------------
bool VSTListModel::isListEmpty() const
{
    return this->vstPlugins.isEmpty();
}

//-----------------------------------------------------------------------------
QSharedPointer<VSTPlugin> VSTListModel::getPluginAtIndex(int pluginIndex) const
{
    QSharedPointer<VSTPlugin> vstPlugin;
    if(pluginIndex >= 0 && pluginIndex < this->vstPlugins.size())
    {
        vstPlugin = this->vstPlugins[pluginIndex]->getVSTPlugin();
    }
    return vstPlugin;
}

//-----------------------------------------------------------------------------
bool VSTListModel::doesExistPluginPresets(const QSharedPointer<VSTPlugin>& vstPlugin)
{
    bool doesExist = false;

    if(!vstPlugin.isNull())
    {
        QMap<int32_t, QList<QSharedPointer<VSTListItem> > >::const_iterator presetIterator = presetsByVstId.find(vstPlugin->getFxId());
        if(presetIterator != presetsByVstId.end())
        {
            doesExist = !presetIterator->isEmpty();
        }
    }

    return doesExist;
}

//-----------------------------------------------------------------------------
QSet<QString> VSTListModel::getUniquePluginPresetNames(const QSharedPointer<VSTPlugin>& vstPlugin)
{
    QSet<QString> presetNames;

    if(!vstPlugin.isNull())
    {
        QMap<int32_t, QList<QSharedPointer<VSTListItem> > >::const_iterator pluginPresets = presetsByVstId.find(vstPlugin->getFxId());
        if(pluginPresets != presetsByVstId.end())
        {
            for(QList<QSharedPointer<VSTListItem> >::const_iterator presetIterator = pluginPresets->begin();
                presetIterator != pluginPresets->end();
                ++presetIterator)
            {
                const QSharedPointer<VSTPreset>& preset = (*presetIterator)->getVstPreset();
                if(!preset.isNull())
                {
                    presetNames.insert(preset->getName());
                }
            }
        }
    }

    return presetNames;
}

//-----------------------------------------------------------------------------
QModelIndex VSTListModel::getPresetIndex(const QSharedPointer<VSTPreset>& vstPreset)
{
    QModelIndex presetModelIndex;

    if(!vstPreset.isNull())
    {
        // Get parent model index.
        int32_t parentFxId = vstPreset->getVSTPlugin()->getFxId();
        bool isParentFound = false;
        int parentRank = 0;
        for(int pluginIndex = 0; pluginIndex < vstPlugins.size() && !isParentFound; ++pluginIndex)
        {
            const QSharedPointer<VSTPlugin>& vstPlugin = vstPlugins[pluginIndex]->getVSTPlugin();
            if(!vstPlugin.isNull() && vstPlugin->getFxId() == parentFxId)
            {
                parentRank = pluginIndex;
                isParentFound = true;
            }
        }

        if(isParentFound)
        {
            QModelIndex parentModelIndex = index(parentRank, 0);
            if(parentModelIndex.isValid())
            {
                // Get children rank.
                QMap<int32_t, QList<QSharedPointer<VSTListItem> > >::const_iterator presetListIterator = presetsByVstId.find(parentFxId);
                if(presetListIterator != presetsByVstId.end())
                {
                    bool isPresetFound = false;
                    int presetRank = 0;
                    for(int presetIndex = 0; presetIndex < presetListIterator->size() && !isPresetFound; ++presetIndex)
                    {
                        const QSharedPointer<VSTPreset>& currentVstPreset = presetListIterator->at(presetIndex)->getVstPreset();
                        if(currentVstPreset.data() == vstPreset.data())
                        {
                            presetRank = presetIndex;
                            isPresetFound = true;
                        }
                    }

                    if(isPresetFound)
                    {
                        presetModelIndex = index(presetRank, 0, parentModelIndex);
                    }
                }
            }
        }

    }

    return presetModelIndex;
}

//-----------------------------------------------------------------------------
QList<QSharedPointer<VSTListItem> > VSTListModel::getPlugins() const
{
    return vstPlugins;
}

//-----------------------------------------------------------------------------
QList<QSharedPointer<VSTListItem> > VSTListModel::getPluginPresets(const QSharedPointer<VSTPlugin>& vstPlugin) const
{
    QList<QSharedPointer<VSTListItem> > presetList;

    if(!vstPlugin.isNull())
    {
        QMap<int32_t, QList<QSharedPointer<VSTListItem> > >::const_iterator presetListIterator = presetsByVstId.find(vstPlugin->getFxId());
        if(presetListIterator != presetsByVstId.end())
        {
            presetList = *presetListIterator;
        }
    }

    return presetList;
}

//-----------------------------------------------------------------------------
void VSTListModel::beginModelChangeWithoutRefresh()
{
    this->beginResetModel();
}

//-----------------------------------------------------------------------------
void VSTListModel::endModelChangeWithoutRefresh()
{
    this->endResetModel();
}

