/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vstlistmodel.h
  Author: Francois Mazen
  Date: 28/05/07 16:25
  Description: header file of the VSTListModel class
-------------------------------------------------------------------------------
*/


#ifndef VST_LIST_MODEL_H
#define VST_LIST_MODEL_H

#include <QAbstractItemModel>
#include <stdint.h>

class VSTPlugin;
class VSTPreset;
class VSTListItem;

class VSTListModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    VSTListModel(QWidget * parent);
    virtual ~VSTListModel();

    //QAbstractItemModel:
    QVariant data(const QModelIndex & modelIndex, int role ) const;
    int rowCount(const QModelIndex & parent = QModelIndex() ) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &modelIndex) const;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex & index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    // Data manipulation:
    void addPlugin(const QSharedPointer<VSTPlugin> &vstPlugin);
    void addPreset(const QSharedPointer<VSTPreset> &vstPreset);
    void removePlugin(const QSharedPointer<VSTPlugin> &vstPlugin);
    void removePreset(const QSharedPointer<VSTPreset> &vstPreset);
    void clearList();
    bool areDataSaved() const;
    void setNeedSaving(bool doesNeedSaving);
    bool isListEmpty() const;
    QSharedPointer<VSTPlugin> getPluginAtIndex(int pluginIndex) const;
    bool doesExistPluginPresets(const QSharedPointer<VSTPlugin> &vstPlugin);
    QSet<QString> getUniquePluginPresetNames(const QSharedPointer<VSTPlugin> &vstPlugin);
    QModelIndex getPresetIndex(const QSharedPointer<VSTPreset> &vstPreset);

    QList<QSharedPointer<VSTListItem> > getPlugins() const;
    QList<QSharedPointer<VSTListItem> > getPluginPresets(const QSharedPointer<VSTPlugin> &vstPlugin) const;
    void beginModelChangeWithoutRefresh();
    void endModelChangeWithoutRefresh();
    void addPluginWithoutRefresh(const QSharedPointer<VSTPlugin> &vstPlugin);
    void clearListWithoutRefresh();

private slots:
    void presetDataChanged();

private:
    QList<QSharedPointer<VSTListItem> > vstPlugins;
    QMap<int32_t, QList<QSharedPointer<VSTListItem> > > presetsByVstId;

    //are the data saved ?
    bool areDataWritten;
};



#endif //VST_LIST_MODEL_H
