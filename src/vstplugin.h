/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgfile.h
  Author: Francois Mazen
  Date: 20/02/17 19:56
  Description: VST Plugin abstraction.
-------------------------------------------------------------------------------
*/

#ifndef VSTPLUGIN_H
#define VSTPLUGIN_H

#include <stdint.h>
#include <QString>

class AEffect;
class VSTPreset;

class VSTPlugin
{
public:
    VSTPlugin(const QString& filePath, AEffect* audioEffect);
    void sendCloseEvent();

    int32_t getFxId() const;
    int32_t getFxVersion() const;
    int32_t getNumberParams() const;
    int32_t getNumberPrograms() const;
    QString getParameterName(int32_t index) const;
    QString getParameterDisplay(int32_t index) const;
    QString getName() const;
    bool useChunk() const;
    const QString& getFilePath() const;

    void setParameter(long index, float value) const;
    float getParameterValue(long index) const;
    void setProgram(long num) const;
    void setCurrentProgramName(const char* name) const;
    QString getCurrentProgramName() const;
    int32_t getProgramChunk(void** chunk) const;
    int32_t getBankChunk(void** chunk) const;
    void setProgramChunk(void* chunk, int32_t chunkSize) const;
    void setBankChunk(void* chunk, int32_t chunkSize) const;

private:
    QString filePath;
    AEffect* audioEffect;
};

#endif // VSTPLUGIN_H
