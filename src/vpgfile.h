/*
-------------------------------------------------------------------------------
    This file is a part of the VST Preset Generator
    Copyright (C) 2007  Francois Mazen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
-------------------------------------------------------------------------------
  Name: vpgfile.h
  Author: Francois Mazen
  Date: 28/06/16 19:22
  Description: VpgFile class that represent the list of VST plugins and presets
               loaded by the user.
-------------------------------------------------------------------------------
*/

#ifndef VPGFILE_H
#define VPGFILE_H

#include <QString>
#include <QList>

#include "parameter.h"

#include <stdint.h>

class QDomDocument;
class QDomElement;
class VpgVersion;
class VSTListModel;
class VSTPlugin;
class VSTPreset;

class VpgFile
{
public:
    enum ERROR_CODE
    {
        ERROR_NO_ERROR,
        ERROR_COULD_NOT_OPEN_FILE,
        ERROR_COULD_NOT_PARSE_FILE,
        ERROR_INVALID_VPG_FILE,
        ERROR_UNKNOWN_XML_TAG,
        ERROR_INVALID_XML_ELEMENT
    };

    // Preset from file representation.
    struct Preset
    {
        QString name;
        QList<QSharedPointer<Parameter> > parameters;
    };

    // Plugin from file representation.
    class Plugin
    {
    public:
        Plugin(): id(-1), version(-1)
        {
        }

        QString filePath;
        QString name;
        int32_t id;
        int32_t version;
        QList<Preset> presets;
    };

    static VpgFile::ERROR_CODE ExtractPluginsFromFile(const QString& filename, QList<Plugin> &pluginFromFile);
    static VpgFile::ERROR_CODE WriteModelToFile(const QString& filename, const VSTListModel& vstListModel);

private:
    static void createPluginTags(const VSTListModel& vstListModel, QDomDocument &doc, QDomElement &root);
    static void createPresetTags(const VSTListModel& vstListModel, QDomDocument& doc, const QSharedPointer<VSTPlugin> &vstPlugin, QDomElement &vstTag);
    static void createParameterTags(QDomDocument& doc, QDomElement &presetTag, QDomElement& vstTag, const QSharedPointer<VSTPreset> &vstPreset);
    static ERROR_CODE fillPluginFromXML(const VpgVersion& fileVersion, const QDomElement& vstTag, VpgFile::Plugin& newPlugin);
    static ERROR_CODE fillPresetFromXML(const QDomElement& presetTag, Preset& newPreset);
    static void fillParameterFromXML(const QDomElement& paramTag, QSharedPointer<Parameter> &parameter);
};

#endif // VPGFILE_H
